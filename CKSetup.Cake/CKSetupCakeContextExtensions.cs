using CK.Core;
using CKSetup;
using CodeCake;
using System;
using System.Collections.Generic;

namespace Cake.Core
{
    /// <summary>
    /// Extends <see cref="ICakeContext"/> with helper related to CKSetup.
    /// </summary>
    public static class CKSetupCakeContextExtensions
    {
        /// <summary>
        /// Name of the environment variable that can defines the Url and optionally the API Key
        /// of the store that will be used to push components.
        /// </summary>
        public const string EnvNameTargetStoreAPIKeyAndUrl = "CKSETUP_CAKE_TARGET_STORE_APIKEY_AND_URL";

        /// <summary>
        /// Creates a new <see cref="CKSetupStoreConfiguration"/> object (that can be not <see cref="CKSetupStoreConfiguration.IsValid"/>
        /// and may be mutated).
        /// <para>
        /// This configuration is based on the CKSETUP_CAKE_TARGET_STORE_APIKEY_AND_URL environment variable that, if it exists,
        /// can contain the API key and target url (or full local path) separated by a pipe (|) (or a target url alone without any pipe in it).
        /// </para>
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <returns>A configuration initalized by the environment variable.</returns>
        public static CKSetupStoreConfiguration CKSetupCreateDefaultConfiguration( this ICakeContext @this )
        {
            string targetStoreAPIKeyAndUrl = @this.Environment.GetEnvironmentVariable( EnvNameTargetStoreAPIKeyAndUrl );
            if( targetStoreAPIKeyAndUrl == null && @this.InteractiveMode() != InteractiveMode.NoInteraction )
            {
                targetStoreAPIKeyAndUrl = @this.InteractiveEnvironmentVariable( EnvNameTargetStoreAPIKeyAndUrl );
            }
            return CKSetupStoreConfiguration.CreateFromEnvironmentValues( targetStoreAPIKeyAndUrl );
        }

        /// <summary>
        /// Registers a set of bin folders (that must have been published if in .net core: their paths
        /// must end with "/publish") into a local store.
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <param name="folders">Sets of folder paths to register.</param>
        /// <param name="storePath">The store path to use. Defaults to <see cref="Facade.DefaultStorePath"/>.</param>
        /// <returns>True on success, false if an error occurred.</returns>
        public static bool CKSetupAddComponentFoldersToStore(
            this ICakeContext @this,
            IEnumerable<string> folders,
            string storePath = "CodeCakeBuilder/Releases/TempStore" )
        {
            Throw.CheckNotNullArgument( folders );
            var monitor = new ActivityMonitor { MinimalFilter = LogFilter.Monitor };
            monitor.Output.RegisterClient( new ActivityMonitorConsoleClient() );
            var binFolders = Facade.ReadBinFolders( monitor, folders );
            if( binFolders == null ) return false;

            using( LocalStore store = LocalStore.OpenOrCreate( monitor, storePath ) )
            using( IRemoteStore remote = store?.CreateStandardRemote( monitor ) )
            {
                return store != null
                        && store.CreateLocalImporter( remote )
                                .AddComponent( binFolders )
                                .Import();
            }
        }

        /// <summary>
        /// Registers a set of bin folders (that must have been published if in .net core: their paths
        /// must end with "/publish") into a local store.
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <param name="storeConf">Required configuration.</param>
        /// <param name="folders">Sets of folder paths to register.</param>
        /// <returns>True on success, false if an error occurred.</returns>
        public static bool CKSetupAddComponentFoldersToStore(
            this ICakeContext @this,
            CKSetupStoreConfiguration storeConf,
            IEnumerable<string> folders)
        {
            Throw.CheckNotNullArgument( storeConf );
            return CKSetupAddComponentFoldersToStore( @this, folders, storeConf.TemporaryStorePath );
        }


        /// <summary>
        /// Calls <see cref="CKSetupAddComponentFoldersToStore(ICakeContext, CKSetupStoreConfiguration, IEnumerable{string})"/>
        /// after having called Cake.DotNetCorePublish on paths with a last folder that starts with "netcoreapp" or "net5"..."net9".
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <param name="binFrameworkPaths">Sets of folder paths to register.</param>
        /// <param name="forcePublish">True to publish even if /publish folder exists.</param>
        /// <param name="storePath">The target store path.</param>
        /// <returns>True on success, false if an error occurred.</returns>
        public static bool CKSetupPublishAndAddComponentFoldersToStore(
            this ICakeContext @this,
            IEnumerable<string> binFrameworkPaths,
            bool forcePublish = false,
            string storePath = "CodeCakeBuilder/Releases/TempStore" )
        {
            var monitor = new ActivityMonitor { MinimalFilter = LogFilter.Monitor };
            monitor.Output.RegisterClient( new ActivityMonitorConsoleClient() );
            var paths = new List<string>();
            foreach( var p in binFrameworkPaths )
            {
                var r = Facade.EnsurePublished( monitor, p, forcePublish );
                if( !r.Success ) return false;
                paths.Add( r.FinalPath );
            }
            return CKSetupAddComponentFoldersToStore( @this, paths, storePath );
        }

        /// <summary>
        /// Calls <see cref="CKSetupAddComponentFoldersToStore(ICakeContext, CKSetupStoreConfiguration, IEnumerable{string})"/>
        /// after having called Cake.DotNetCorePublish on paths with a last folder that starts with "netcoreapp" or "net5"..."net9".
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <param name="storeConf">Required <see cref="CKSetupStoreConfiguration"/>.</param>
        /// <param name="binFrameworkPaths">Sets of folder paths to register.</param>
        /// <param name="forcePublish">True to publish even if /publish folder exists.</param>
        /// <returns>True on success, false if an error occurred.</returns>
        public static bool CKSetupPublishAndAddComponentFoldersToStore(
            this ICakeContext @this,
            CKSetupStoreConfiguration storeConf,
            IEnumerable<string> binFrameworkPaths,
            bool forcePublish = false)
        {
            Throw.CheckNotNullArgument( storeConf );
            return CKSetupPublishAndAddComponentFoldersToStore( @this, binFrameworkPaths, forcePublish, storeConf.TemporaryStorePath );
        }

        /// <summary>
        /// Pushes the content of a local store path to a remote store.
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <param name="targetStoreUrl">
        /// The target store url or full path to a local store. Must not be null or empty.
        /// Can be <see cref="Facade.DefaultStoreUrl"/> for public components (as long as the API key is knwon).
        /// </param>
        /// <param name="apiKey">The api key that may be required to be able to push components in the target store.</param>
        /// <param name="storePath">The path to the local store to push.</param>
        /// <returns>True on success, false on error.</returns>
        public static bool CKSetupPushLocalToRemoteStore(
            this ICakeContext @this,
            string targetStoreUrl,
            string apiKey,
            string storePath = "CodeCakeBuilder/Releases/TempStore" )
        {
            Throw.CheckNotNullOrWhiteSpaceArgument( targetStoreUrl );
            var monitor = new ActivityMonitor { MinimalFilter = LogFilter.Monitor };
            monitor.Output.RegisterClient( new ActivityMonitorConsoleClient() );
            using( LocalStore store = LocalStore.OpenOrCreate( monitor, storePath ) )
            {
                return store != null
                        && store.PushComponents( comp => true, new Uri( targetStoreUrl, UriKind.Absolute ), apiKey );
            }
        }

        /// <summary>
        /// Pushes the content of a local store path to a remote store.
        /// </summary>
        /// <param name="this">This CakeContext.</param>
        /// <param name="storeConf">Required <see cref="CKSetupStoreConfiguration"/>.</param>
        /// <returns>True on success, false on error.</returns>
        public static bool CKSetupPushLocalToRemoteStore(
            this ICakeContext @this,
            CKSetupStoreConfiguration storeConf )
        {
            Throw.CheckNotNullArgument( storeConf );
            return CKSetupPushLocalToRemoteStore( @this, storeConf.TargetStoreUrl, storeConf.TargetStoreAPIKey, storeConf.TemporaryStorePath );
        }

    }
}
