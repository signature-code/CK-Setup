using CKSetup;
using System;
using CK.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.Testing.CKSetup
{
    /// <summary>
    /// Event argument for <see cref="ICKSetupDriver.SetupRan"/> event.
    /// </summary>
    public class SetupRanEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new <see cref="SetupRanEventArgs"/>.
        /// </summary>
        /// <param name="config">The setup configuration.</param>
        /// <param name="storePath">The path of the store.</param>
        /// <param name="remoteStoreUrl">The uri of the remote store.</param>
        /// <param name="launchDebug">Whether the process launched the debugger.</param>
        /// <param name="result">Run setup result.</param>
        public SetupRanEventArgs( SetupConfiguration config, NormalizedPath storePath, Uri? remoteStoreUrl, bool launchDebug, CKSetupRunResult result )
        {
            Result = result;
            Configuration = config;
            StorePath = storePath;
            RemoteStoreUrl = remoteStoreUrl;
            LaunchDebug = launchDebug;
        }

        /// <summary>
        /// Gets whether the setup run result.
        /// </summary>
        public CKSetupRunResult Result { get; }

        /// <summary>
        /// Gets the Setup configuration.
        /// </summary>
        public SetupConfiguration Configuration { get; }

        /// <summary>
        /// Gets the path of the store used.
        /// </summary>
        public NormalizedPath StorePath { get; }

        /// <summary>
        /// Gets the url of the additional remote store.
        /// </summary>
        public Uri? RemoteStoreUrl { get; }

        /// <summary>
        /// Gets whether the debugger has been launched by the runner process.
        /// </summary>
        public bool LaunchDebug { get; }
    }
}
