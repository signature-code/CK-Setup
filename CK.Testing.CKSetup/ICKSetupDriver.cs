using CK.Core;
using CKSetup;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace CK.Testing.CKSetup
{
    /// <summary>
    /// Exposes an API to manipulate CKSetup.
    /// </summary>
    public interface ICKSetupDriver
    {
        /// <summary>
        /// Gets the semi colon (;) separated paths from "CKSetup/AutoRegisterLocalComponentPaths" configuration.
        /// This is to be used when the Solution implements setup dependencies and test projects must consume them.
        /// <para>
        /// When this is not empty the <see cref="DefaultStorePath"/> is automatically initialized to <c><see cref="LocalStoresFolder"/>/Default</c>
        /// if it is not configured and these components are published (see <see cref="PublishAndAddComponentFoldersToStore"/>)
        /// and added to the DefaultStorePath first time it is used.
        /// </para>
        /// <para>
        /// Note that before adding the components, any components with a <see cref="CSemVer.SVersion.ZeroVersion"/> version
        /// are removed from the <see cref="DefaultStorePath"/>.
        /// </para>
        /// </summary>
        IReadOnlyList<NormalizedPath> AutoRegisterLocalComponentPaths { get; }

        /// <summary>
        /// Gets the default store path from "CKSetup/DefaultStorePath" configuration.
        /// <para>
        /// When <see cref="AutoRegisterLocalComponentPaths"/> is not empty, this defaults to <c><see cref="LocalStoresFolder"/>/Default</c> directory,
        /// otherwise this is let to null (see <see cref="Facade.LocateStorePath"/> behavior).
        /// </para>
        /// <para>
        /// This configuration should be used to share a store across multiple tests projects. Note that this store is not
        /// automatically cleaned up whereas <see cref="LocalStoresFolder"/> (that is local to each test project) is cleared at its
        /// first use by each test project.
        /// </para>
        /// </summary>
        string? DefaultStorePath { get; }

        /// <summary>
        /// Gets or sets the working directory that the runner will use when <see cref="SetupConfiguration.WorkingDirectory"/>
        /// is null.
        /// <para>
        /// Defaults to "CKSetup/WorkingDirectory" configuration or an empty path (<see cref="NormalizedPath.IsEmptyPath"/>).
        /// If this path is empty, a temporary folder is created (<see cref="System.IO.Path.GetTempPath"/>). 
        /// A unique timed folder (see <see cref="CK.Core.FileUtil.CreateUniqueTimedFolder"/>) is created for
        /// each setup and let as-is on the file system.
        /// </para>
        /// </summary>
        NormalizedPath WorkingDirectory { get; set; }

        /// <summary>
        /// Gets the semi colon (;) separated paths from "CKSetup/DefaultBinPaths" configuration.
        /// When empty, the single BinPath: <see cref="IBasicTestHelper.ClosestSUTProjectFolder"/>/<see cref="IBasicTestHelper.PathToBin"/>.
        /// <para>
        /// This is used to fill the <see cref="SetupConfiguration.BinPaths"/> when <see cref="Run"/>
        /// is called if (and only if) the configuration parameter's BinPaths is empty.
        /// </para>
        /// <para>
        /// When not rooted (relative paths), the base directory is
        /// the <see cref="IBasicTestHelper.TestProjectFolder"/> (same behavior as <see cref="DefaultStorePath"/> and <see cref="WorkingDirectory"/>).
        /// BinPaths can use standard placeholders from CK.Testing like {BinFolder}, {TestProjectFolder}, {ClosestSUTProjectFolder},
        /// {BuildConfiguration}, {PathToBin} etc.
        /// </para>
        /// <para>
        /// Bin paths are automatically published (see <see cref="EnsurePublished(string, bool)"/>).
        /// </para>
        /// </summary>
        IReadOnlyList<NormalizedPath> DefaultBinPaths { get; }

        /// <summary>
        /// Gets the semi colon (;) separated <see cref="SetupDependency"/> from "CKSetup/DefaultDependencies" configuration.
        /// A SetupDependency is the name and an optional minimal version like "CKSetup.Runner/6.0.0-a00-00-develop-0032".
        /// <para>
        /// This is used to fill the <see cref="SetupConfiguration.Dependencies"/> when <see cref="Run"/>
        /// is called if (and only if) the Dependencies is empty.
        /// </para>
        /// </summary>
        IReadOnlyList<SetupDependency> DefaultDependencies { get; }

        /// <summary>
        /// Gets or sets whether the debugger should be launched by the runner process.
        /// Defaults to "CKSetup/LaunchDebug" configurations or false if no configuration exist.
        /// </summary>
        bool LaunchDebug { get; set; }

        /// <summary>
        /// Gets or sets whether the setup should be ran, regardless of the signature files that may
        /// exist in the <see cref="SetupConfiguration.BinPaths"/> folders.
        /// Defaults to "CKSetup/ForceSetup" configurations or <see cref="ForceSetupLevel.None"/>
        /// if no configuration exist.
        /// </summary>
        ForceSetupLevel ForceSetup { get; set; }

        /// <summary>
        /// Gets the semi colon (;) separated preferred target runtimes from "CKSetup/DefaultPreferredTargetRuntimes"
        /// configuration.
        /// <para>
        /// This is used to fill the <see cref="SetupConfiguration.PreferredTargetRuntimes"/> when <see cref="Run"/>
        /// is called if (and only if) the configuration parameter's PreferredTargetRuntimes is empty.
        /// </para>
        /// </summary>
        IReadOnlyList<TargetRuntime> DefaultPreferredTargetRuntimes { get; }

        /// <summary>
        /// This event fires once and only once before a store is used by <see cref="Run"/>.
        /// This can be used to populate the store with components as needed.
        /// Note that this event is not raised when the <see cref="SetupRunningEventArgs.StorePath"/>
        /// is the shared <see cref="Facade.DefaultStorePath"/>.
        /// </summary>
        event EventHandler<StorePathInitializationEventArgs>? InitializeStorePath;

        /// <summary>
        /// Fired before <see cref="Run"/>. This can be used to cancel the setup 
        /// This can be used to populate the store with components as needed.
        /// </summary>
        event EventHandler<SetupRunningEventArgs>? SetupRunning;

        /// <summary>
        /// Fired after <see cref="Run"/>.
        /// </summary>
        event EventHandler<SetupRanEventArgs>? SetupRan;

        /// <summary>
        /// Runs a configuration.
        /// </summary>
        /// <param name="configuration">The configuration to run. Must not be null.</param>
        /// <param name="storePath">Overridden store path to use instead of <see cref="DefaultStorePath"/>.</param>
        /// <param name="remoteStoreUrl">
        /// Optional remote store url to use (in addition to the <see cref="LocalStore.PrototypeStoreUrl"/>) to resolve locally missing components.
        /// When let to null, an attempt to locate an ambient store is made (see <see cref="Facade.LocateStorePath(string?, string?, bool, Core.IActivityMonitor?)"/>).
        /// To explicitly not use any remote store, "no" or "none" can be specified (see <see cref="ComponentDB.CreateRemoteUri(string?)"/>).
        /// </param>
        /// <param name="launchDebug">
        /// True to call <see cref="System.Diagnostics.Debugger.Launch"/> at the very start of the runner process.
        /// Defaults to <see cref="LaunchDebug"/>.
        /// </param>
        /// <param name="forceSetup">
        /// <see cref="ForceSetupLevel.Engine"/> to run the engine even if the signature files that may exist in the <see cref="SetupConfiguration.BinPaths"/> folders
        /// have not changed and <see cref="ForceSetupLevel.Full"/> to also ask the engine to ignore any cache.
        /// Defaults to <see cref="ForceSetup"/> value.
        /// </param>
        /// <returns>Run result.</returns>
        CKSetupRunResult Run( SetupConfiguration configuration,
                              string? storePath = null,
                              string? remoteStoreUrl = null,
                              bool? launchDebug = null,
                              ForceSetupLevel? forceSetup = null );

        /// <summary>
        /// Path to a folder for local stores: "<see cref="IBasicTestHelper.TestProjectFolder"/>/LocalStores".
        /// This folder is globally cleared once (the first time this helper is used).
        /// </summary>
        NormalizedPath LocalStoresFolder { get; }

        /// <summary>
        /// Gets a path to a local store in <see cref="LocalStoresFolder"/>.
        /// Use <see cref="GetCleanTestStorePath(string, string)"/> to clear it before returning it.
        /// </summary>
        /// <param name="suffix">Optional suffix (will appear at the end of the folder).</param>
        /// <param name="name">Name (automatically sets from the caller method name).</param>
        /// <returns>Path to a directory that may already exists.</returns>
        NormalizedPath GetTestStorePath( string? suffix = null, [CallerMemberName]string? name = null );

        /// <summary>
        /// Gets a path to a local store in <see cref="LocalStoresFolder" />. If the store exists it is deleted.
        /// </summary>
        /// <param name="suffix">Optional suffix (will appear at the end of the folder).</param>
        /// <param name="name">Name (automatically sets from the caller method name).</param>
        /// <returns>Path to directory that does not exist.</returns>
        NormalizedPath GetCleanTestStorePath( string? suffix = null, [CallerMemberName]string? name = null );

        /// <summary>
        /// Runs "dotnet publish" on an output bin folder if it needs to be published.
        /// The path can end with "/publish" or be the path above (typically "bin/Debug/netcoreapp2.1").
        /// <para>
        /// </para>
        /// The configuration (Debug/Release) and target framework (netcoreapp2.1 for instance) are extracted
        /// from the path. When called on .Net framework (like 'net461' folders) or on net standard
        /// targets (like 'netstandard2.0'), nothing is done, a warning is emitted and an empty path is returned
        /// without any error.
        /// <para>
        /// If /publish folder exists and contains the project file name (.exe or .dll) with a LastWriteTime equal
        /// or greater than the one of the file in <paramref name="pathToFramework"/>, publish is skipped
        /// unless <paramref name="forcePublish"/> is true.
        /// </para>
        /// </summary>
        /// <param name="pathToFramework">Path to the framework (typically ends with "bin/Debug/netcoreapp2.1") or to the "/publish".</param>
        /// <param name="forcePublish">
        /// True to force the publication in .net core even if the "publish/" folder is up to date.
        /// </param>
        /// <returns>The result value type that contains success and paths.</returns>
        Facade.EnsurePublishResult EnsurePublished( string pathToFramework, bool forcePublish = false );

        /// <summary>
        /// Registers a set of bin folders (that must have been published if in .net core: their paths
        /// must end with "/publish") into a local store.
        /// The <paramref name="storePath"/> can not be the shared <see cref="Facade.DefaultStorePath"/> otherwise
        /// an <see cref="ArgumentException"/> is thrown.
        /// </summary>
        /// <param name="publishedFolders">Sets of folder paths to register.</param>
        /// <param name="storePath">Overridden store path to use instead of <see cref="DefaultStorePath"/>.</param>
        /// <param name="remoteStoreUrl">
        /// Optional remote store url that will be used in addition to the <see cref="LocalStore.PrototypeStoreUrl"/> to resolve
        /// any embedded components that may be found in <paramref name="publishedFolders"/> and that are not already in
        /// the local store.
        /// When let to null, an attempt to locate an ambient store is made (see <see cref="Facade.LocateStorePath(string?, string?, bool, Core.IActivityMonitor?)"/>).
        /// To explicitly not use any remote store, "no" or "none" can be specified (see <see cref="ComponentDB.CreateRemoteUri(string?)"/>).
        /// </param>
        /// <returns>True on success, false if an error occurred.</returns>
        bool AddComponentFoldersToStore( IEnumerable<string> publishedFolders, string? storePath = null, string? remoteStoreUrl = null );

        /// <summary>
        /// Removes components that match a predicate.
        /// This does not remove any potential dependencies (a <see cref="ComponentDependency"/> only defines the name of its target and a minimal version).
        /// This is typically used from unit tests to remove the <see cref="CSemVer.SVersion.ZeroVersion"/> version of components.
        /// The final local store cannot be the shared <see cref="Facade.DefaultStorePath"/> otherwise
        /// an <see cref="ArgumentException"/> is thrown.
        /// </summary>
        /// <param name="exclude">A predicate that must return true to remove the component.</param>
        /// <param name="garbageCollectFiles">True to delete all stored files that are no more referenced by any component.</param>
        /// <param name="storePath">Overridden target store path to use instead of <see cref="DefaultStorePath"/>.</param>
        /// <returns>The number of removed components.</returns>
        int RemoveComponentsFromStore( Func<Component, bool> exclude, bool garbageCollectFiles = false, string? storePath = null );

        /// <summary>
        /// Calls <see cref="AddComponentFoldersToStore"/> after having called <see cref="EnsurePublished"/>.
        /// The final local store cannot be the shared <see cref="Facade.DefaultStorePath"/> otherwise
        /// an <see cref="ArgumentException"/> is thrown.
        /// </summary>
        /// <param name="binFrameworkPaths">Sets of folder paths to register.</param>
        /// <param name="forcePublish">
        /// True to force the publication in .net core even if the "publish/" folder is up to date.
        /// See <see cref="EnsurePublished(string, bool)"/>.
        /// </param>
        /// <param name="storePath">Overridden store path to use instead of <see cref="DefaultStorePath"/>.</param>
        /// <param name="remoteStoreUrl">
        /// Optional remote store url that will be used in addition to the <see cref="LocalStore.PrototypeStoreUrl"/> to resolve
        /// any embedded components that may be found in <paramref name="binFrameworkPaths"/> and that are not already in
        /// the local store.
        /// When let to null, an attempt to locate an ambient store is made (see <see cref="Facade.LocateStorePath(string?, string?, bool, Core.IActivityMonitor?)"/>).
        /// To explicitly not use any remote store, "no" or "none" can be specified (see <see cref="ComponentDB.CreateRemoteUri(string?)"/>).
        /// </param>
        /// <returns>True on success, false if an error occurred.</returns>
        bool PublishAndAddComponentFoldersToStore( IEnumerable<string> binFrameworkPaths, bool forcePublish = false, string? storePath = null, string? remoteStoreUrl = null );

        /// <summary>
        /// Pushes the content of a local store path to a remote store.
        /// </summary>
        /// <param name="storePath">The path to the local store to push. Must not be null.</param>
        /// <param name="remoteStoreUrl">The remote store url. Must not be null nor <see cref="ComponentDB.NoRemote"/>.</param>
        /// <param name="apiKey">The api key that may be required to be able to push components in the remote store.</param>
        /// <returns>True on success, false on error.</returns>
        bool PushLocalStoreToRemote( string storePath, string remoteStoreUrl, string apiKey );
    }
}
