using CK.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.Testing.CKSetup
{
    /// <summary>
    /// Event argument for <see cref="ICKSetupDriver.InitializeStorePath"/>.
    /// </summary>
    public class StorePathInitializationEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new <see cref="StorePathInitializationEventArgs"/>.
        /// </summary>
        /// <param name="storePath">The path of the store that will be used for the first time.</param>
        public StorePathInitializationEventArgs( NormalizedPath storePath )
        {
            StorePath = storePath;
        }

        /// <summary>
        /// Gets the path of the store that will be used by <see cref="ICKSetupDriver.Run"/>.
        /// </summary>
        public NormalizedPath StorePath { get; }
    }
}
