using CK.Core;
using CK.Testing.CKSetup;
using CKSetup;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace CK.Testing
{
    /// <summary>
    /// Exposes the <see cref="ICKSetupTestHelper"/> mixin test helper.
    /// </summary>
    public class CKSetupTestHelper : CKSetup.ICKSetupTestHelperCore
    {
        readonly ICKSetupDriver _driver;

        class Driver : ICKSetupDriver
        {
            readonly IMonitorTestHelper _monitor;
            readonly NormalizedPath _localStoresFolder;
            readonly string? _defaultStorePath;
            bool _launchDebug;
            ForceSetupLevel _forceSetup;

            public Driver( TestHelperConfiguration config, IMonitorTestHelper monitor )
            {
                // Solicits HttpClient to force assembly load here instead of inside Run:
                // when TestHelpers are instantiated we are in CK.WeakAssemblyNameResolver.
                new System.Net.Http.HttpRequestMessage();

                _monitor = monitor;
                _localStoresFolder = monitor.TestProjectFolder.AppendPart( "LocalStores" );

                AutoRegisterLocalComponentPaths = config.DeclareMultiPaths( "CKSetup/AutoRegisterLocalComponentPaths",
                                                                            "When the Solution implements models and setup dependencies and test projects must consume them, this lists the local components and registration in a local store is automatically handled.",
                                                                            editableValue: null ).Value.ToList();
                // CKSetup/DefaultStorePath defaults to null except if there is at
                // least one AutoRegisterLocalComponentPaths
                {
                    var (c, storePath) = config.DeclarePath( "CKSetup/DefaultStorePath",
                                                             "Advanced configuration to use when CKSetup/AutoRegisterLocalComponentPaths is not empty that enables multiple tests projects to share the same be used to share a local store.",
                                                             editableValue: null );
                    if( storePath.HasValue )
                    {
                        _defaultStorePath = storePath.Value;
                    }
                    else if( AutoRegisterLocalComponentPaths.Count > 0 )
                    {
                        _defaultStorePath = _localStoresFolder.AppendPart( "Default" );
                        c.SetDefaultValue( _defaultStorePath );
                    }
                }

                // WorkingDirectory defaults to "" (empty path) and is editable.
                {
                    var (c,wd) = config.DeclarePath( "CKSetup/WorkingDirectory",
                                                     "The path of the working directory to use (typically \"{TestProjectFolder}/WorkingDirectory\"). If this is not configured or empty, a temporary folder is created in the user temp directory.",
                                                     editableValue: () => WorkingDirectory.ToString() );
                    if( wd.HasValue )
                    {
                        WorkingDirectory = wd.Value;
                    }
                }

                // DefaultBnPaths is an empty list by default.
                DefaultBinPaths = config.DeclareMultiPaths( "CKSetup/DefaultBinPaths",
                                                            "Advanced configuration for multiple BinPaths. When none are provided, \"{ClosestSUTProjectFolder}/{PathToBin}\" is used (and this works for almost any test projects).",
                                                            editableValue: null ).Value.ToList();

                // DefaultPreferredTargetRuntimes defaults to an empty enum list.
                {
                    var (c,r) = config.DeclareMultiStrings( "CKSetup/DefaultPreferredTargetRuntimes",
                                                            "Advanced configuration: with NET6+ this doesn't make a lot of sense.",
                                                            editableValue: null );
                    if( r.Any() )
                    {
                        DefaultPreferredTargetRuntimes = r.Select( s => (TargetRuntime)Enum.Parse( typeof( TargetRuntime ), s ) )
                                                          .ToArray();
                        c.SetNormalizedConfiguredValue( String.Join( ';', DefaultPreferredTargetRuntimes.Select( v => v.ToString() ) ) );
                    }
                    else
                    {
                        DefaultPreferredTargetRuntimes = Array.Empty<TargetRuntime>();
                        c.SetDefaultValue( "" );
                    }
                }

                // DefaultDependencies defaults to an empty list of SetupDependency ('UseName/UseVersion').
                {
                    var (c,p) = config.DeclareMultiStrings( "CKSetup/DefaultDependencies",
                                                            "Enables to inject one or more SetupDependency (separated by semicolons ';'). A SetupDependency is the name and an optional minimal version like \"CKSetup.Runner/6.0.0-a00-02\".",
                                                            editableValue: null );
                    if( p.Any() )
                    {
                        DefaultDependencies = p.Select( x => SetupDependency.Parse( x ) ).ToList();
                        c.SetNormalizedConfiguredValue( String.Join( ';', DefaultDependencies.Select( v => v.ToString() ) ) );
                    }
                    else
                    {
                        DefaultDependencies = Array.Empty<SetupDependency>();
                        c.SetDefaultValue( "" );
                    }
                }
                // CKSetup/LaunchDebug is editable and is false by default.
                {
                    _launchDebug = config.DeclareBoolean( "CKSetup/LaunchDebug",
                                                          false,
                                                          "Advanced configuration: when True, the debugger is launched by the runner process at its start. By attaching to the process, the setup process can be debugged.",
                                                          editableValue: () => LaunchDebug.ToString(),
                                                          "CKSetup/DefaultLaunchDebug" ).Value;
                }
                // CKSetup/ForceSetup is editable and is None by default.
                {
                    var c = config.Declare( "CKSetup/ForceSetup",
                                            "Advanced configuration: \"Engine\" to run the engine even if the signature files that may exist in the BinPaths folders have not changed and \"Full\" to also ask the engine to ignore any cache. \"None\" is most often fine.",
                                            editableValue: () => _forceSetup.ToString(),
                                            "CKSetup/DefaultForceSetup" );
                    if( c.ConfiguredValue != null && !Enum.TryParse( c.ConfiguredValue, true, out _forceSetup ) )
                    {
                        Throw.InvalidDataException( $"Invalid ForceSetup: '{c.ConfiguredValue}'. Must be {ForceSetupLevel.None}, {ForceSetupLevel.Engine} or {ForceSetupLevel.Full}." );
                    }
                }
            }

            public IReadOnlyList<NormalizedPath> AutoRegisterLocalComponentPaths { get; }

            public NormalizedPath LocalStoresFolder
            {
                get
                {
                    _monitor.OnlyOnce( () => _monitor.CleanupFolder( _localStoresFolder ) );
                    return _localStoresFolder;
                }
            }

            public string? DefaultStorePath => _defaultStorePath;

            public NormalizedPath WorkingDirectory { get; set; }

            public IReadOnlyList<NormalizedPath> DefaultBinPaths { get; }

            public IReadOnlyList<SetupDependency> DefaultDependencies { get; }

            public bool LaunchDebug { get => _launchDebug; set => _launchDebug = value; }

            public ForceSetupLevel ForceSetup { get => _forceSetup; set => _forceSetup = value; }

            public IReadOnlyList<TargetRuntime> DefaultPreferredTargetRuntimes { get; }

            public event EventHandler<SetupRunningEventArgs>? SetupRunning;

            public event EventHandler<SetupRanEventArgs>? SetupRan;

            public event EventHandler<StorePathInitializationEventArgs>? InitializeStorePath;

            public CKSetupRunResult Run( SetupConfiguration configuration,
                                         string? storePath = null,
                                         string? remoteStoreUrl = null,
                                         bool? launchDebug = false,
                                         ForceSetupLevel? forceSetup = null )
            {
                Throw.CheckNotNullArgument( configuration );
                if( !forceSetup.HasValue ) forceSetup = _forceSetup;

                // We have potentially 3 stores here:
                //  - running store: The specific one asked here by the storePath parameter.
                //  - default test store: The one of the test that depends on the CKSetup/DefaultStorePath configuration (that can be LocalStoresFolder/Default
                //    if GenerateComponentsMode is true).
                //  - and the ambient one (often the shared Facade.DefaultStorePath) that may be redirected by a CKSetupStore.txt file above, to a CKSetupStore folder above, or
                //    to the CKSETUP_STORE environment variable.
                //    The ambient one is typically one of the /LocalFeed/ store folders above.
                //
                // A fourth one must always be considered: the shared default one in Facade.DefaultStorePath.

                NormalizedPath runningLocalStore = Facade.LocateStorePath( storePath ?? _defaultStorePath, monitor: _monitor.Monitor );
                NormalizedPath defaultTestStore = storePath == null
                                                    ? runningLocalStore
                                                    : new NormalizedPath( Facade.LocateStorePath( _defaultStorePath ) );

                // When an explicit remoteStoreUrl is not provided...
                if( remoteStoreUrl == null )
                {
                    NormalizedPath ambientStore;
                    if( storePath == null && _defaultStorePath == null )
                    {
                        ambientStore = defaultTestStore;
                    }
                    else
                    {
                        using( _monitor.Monitor.OpenInfo( $"Looking for an Ambient store." ) )
                        {
                            ambientStore = new NormalizedPath( Facade.LocateStorePath( startPath: runningLocalStore, monitor: _monitor.Monitor ) );
                            if( ambientStore.Path == Facade.DefaultStorePath ) _monitor.Monitor.CloseGroup( "Not found." );
                        }
                    }

                    // If the running store is the shared Facade.DefaultStorePath, we don't do anything:
                    //  - its PrototypeStoreUrl is on the Facade.DefaultStoreUrl public remote store and this is fine.
                    // If the running store is the ambient one (typically in a /LocalFeed/ folder above), we don't do anything either:
                    //  - its PrototypeStoreUrl should be on the shared default Facade.DefaultStorePath (whose ProtoTypeUrl is on the public remote)
                    //    or to any other store but this is outside of our scope/responsibility.
                    //
                    // Otherwise, when the running store is not one of the 2 above, we use the remote store url to "inject" the ambient
                    // store so that missing components can be resolved (whatever their PrototypeStoreUrl are).
                    if( runningLocalStore != ambientStore && runningLocalStore.Path != Facade.DefaultStorePath )
                    {
                        remoteStoreUrl = new NormalizedPath( ambientStore );
                    }
                }
                Uri? remoteUri = remoteStoreUrl != null && remoteStoreUrl != "none" ? new Uri( remoteStoreUrl, UriKind.Absolute ) : null;

                if( configuration.WorkingDirectory.IsEmptyPath )
                {
                    configuration.WorkingDirectory = WorkingDirectory;
                }
                if( configuration.BinPaths.Count == 0 )
                {
                    if( DefaultBinPaths.Count > 0 )
                    {
                        _monitor.Monitor.Info( $"Using 'CKSetup/DefaultBinPaths' = {DefaultBinPaths.Select( s => s.ToString() ).Concatenate()}" );
                        configuration.BinPaths.AddRange( DefaultBinPaths );
                    }
                    else
                    {
                        _monitor.Monitor.Info( $"No 'CKSetup/DefaultBinPaths' configuration. Using ClosestSUTProjectFolder/PathToBin." );
                        configuration.BinPaths.Add( _monitor.ClosestSUTProjectFolder.Combine( _monitor.PathToBin ) );
                    }
                }
                if( configuration.PreferredTargetRuntimes.Count == 0 )
                {
                    if( DefaultPreferredTargetRuntimes.Count > 0 )
                    {
                        _monitor.Monitor.Info( $"Using 'CKSetup/DefaultPreferredTargetRuntimes' configuration." );
                        configuration.PreferredTargetRuntimes.AddRange( DefaultPreferredTargetRuntimes );
                    }
                    else
                    {
                        _monitor.Monitor.Info( $"No 'CKSetup/DefaultPreferredTargetRuntimes' configuration. PreferredTargetRuntimes will be empty." );
                    }
                }

                if( configuration.Dependencies.Count == 0 && DefaultDependencies.Count > 0 )
                {
                    _monitor.Monitor.Info( $"Adding 'CKSetup/Dependencies' configuration: {DefaultDependencies.Select( s => s.ToString() ).Concatenate()}" );
                    configuration.Dependencies.AddRange( DefaultDependencies );
                }
                SetupRunningEventArgs running = new SetupRunningEventArgs( configuration, runningLocalStore, remoteUri, launchDebug ?? _launchDebug, forceSetup.Value );
                SetupRunning?.Invoke( this, running );
                if( running.Cancel ) return CKSetupRunResult.None;

                // The configuration instance may have changed (pure defensive programming here: the parameter is not used below).
                configuration = running.Configuration;

                // If the store is the shared default one, we have nothing special to do (no components produced by tests should ever be registered in it).
                if( running.StorePath != Facade.DefaultStorePath )
                {
                    // If the running store is not the shared one, we raise the InitializeStorePath event (at most once).
                    _monitor.OnlyOnce( () =>
                    {
                        using( _monitor.Monitor.OpenInfo( $"Running Setup on '{running.StorePath}' for the first time." ) )
                        {
                            // If the running store is the default store of the Test project, we automatically handle AutoRegisterLocalComponentPaths
                            // before raising the InitializeStorePath event.
                            bool isDefaultStore = running.StorePath == defaultTestStore;
                            if( isDefaultStore )
                            {
                                _monitor.Monitor.Trace( "This store is the default local store for this test project: if 'CKSetup/AutoRegisterLocalComponentPaths' exist, they will be automatically registered." );
                                if( AutoRegisterLocalComponentPaths.Count > 0 )
                                {
                                    using( _monitor.Monitor.OpenInfo( $"Registering {AutoRegisterLocalComponentPaths.Count} components from 'CKSetup/AutoRegisterLocalComponentPaths'." ) )
                                    {
                                        RemoveComponentsFromStore( c => c.Version == CSemVer.SVersion.ZeroVersion, garbageCollectFiles: false );
                                        PublishAndAddComponentFoldersToStore( AutoRegisterLocalComponentPaths.Select( x => x.ToString() ) );
                                    }
                                }
                                else _monitor.Monitor.Info( "No 'CKSetup/AutoRegisterLocalComponentPaths' components specified." );
                            }
                            InitializeStorePath?.Invoke( this, new StorePathInitializationEventArgs( running.StorePath ) );
                        }
                    }, "Init:" + running.StorePath );
                }
                _monitor.Monitor.Info( $"Using store '{running.StorePath}'." );
                if( running.RemoteStoreUrl != null )
                {
                    _monitor.Monitor.Info( $"Using remote '{running.RemoteStoreUrl}'." );
                }
                else _monitor.Monitor.Info( $"Not using any additional remote." );

                using( LocalStore store = LocalStore.OpenOrCreate( _monitor.Monitor, running.StorePath, throwOnError: true )! )
                {
                    using( IRemoteStore? remote = running.RemoteStoreUrl != null && running.RemoteStoreUrl != store.PrototypeStoreUrl
                                               ? ClientRemoteStore.Create( _monitor.Monitor, running.RemoteStoreUrl, null )
                                               : null )
                    {
                        var result = CKSetupRunResult.Failed;
                        result = Facade.DoRun( _monitor.Monitor,
                                                store,
                                                running.Configuration,
                                                remote,
                                                running.LaunchDebug,
                                                running.ForceSetup );
                        SetupRan?.Invoke( this, new SetupRanEventArgs( running.Configuration, running.StorePath, running.RemoteStoreUrl, running.LaunchDebug, result ) );
                        return result;
                    }
                }
            }

            public Facade.EnsurePublishResult EnsurePublished( string pathToFramework, bool forcePublish = false )
            {
                return Facade.EnsurePublished( _monitor.Monitor, pathToFramework, forcePublish );
            }

            public int RemoveComponentsFromStore( Func<Component, bool> exclude, bool garbageCollectFiles, string? storePath = null )
            {
                using( LocalStore store = LocalStore.OpenOrCreate( _monitor.Monitor, CheckWritableStorePath( storePath ), throwOnError: true )! )
                {
                    int result = store.RemoveComponents( exclude );
                    if( result > 0 && garbageCollectFiles ) store.GarbageCollectFiles();
                    return result;
                }
            }

            public bool AddComponentFoldersToStore( IEnumerable<string> publishedFolders, string? storePath = null, string? remoteStoreUrl = null )
            {
                var monitor = _monitor.Monitor;
                var binFolders = Facade.ReadBinFolders( monitor, publishedFolders );
                if( binFolders == null ) return false;

                // Here CheckWritableStorePath makes sure that this local store is not the shared one.                    
                using( LocalStore store = LocalStore.OpenOrCreate( monitor, CheckWritableStorePath( storePath ), throwOnError: true )! )
                using( IRemoteStore? remote = store.CreateStandardRemote( monitor, ComponentDB.CreateRemoteUri( remoteStoreUrl ), apiKey: null ) )
                {
                    return store.CreateLocalImporter( remote ).AddComponent( binFolders ).Import();
                }
            }

            public bool PublishAndAddComponentFoldersToStore( IEnumerable<string> binFrameworkPaths, bool forcePublish = false, string? storePath = null, string? remoteStoreUrl = null )
            {
                var safeStorePath = CheckWritableStorePath( storePath );
                var paths = new List<string>();
                foreach( var p in binFrameworkPaths )
                {
                    var r = EnsurePublished( p, forcePublish );
                    if( !r.Success ) return false;
                    paths.Add( r.FinalPath );
                }
                return AddComponentFoldersToStore( paths, safeStorePath, remoteStoreUrl );
            }

            /// <summary>
            /// Throws an exception if the storePath is eventually the shared Facade.DefaultStorePath.
            /// The shared store MUST NOT be used to register local components.
            /// </summary>
            /// <param name="storePath">The store path to challenge. When null, DefaultStorePath is used.</param>
            /// <returns>The storePath as-is or the configured DefaultStorePath if storePath is null.</returns>
            NormalizedPath CheckWritableStorePath( string? storePath )
            {
                var p = Facade.LocateStorePath( storePath ?? _defaultStorePath, monitor: _monitor.Monitor );
                if( p == Facade.DefaultStorePath ) Throw.ArgumentException( $"Store path can not be the shared one ({Facade.DefaultStorePath}).", nameof( storePath ) );
                return p;
            }

            public bool PushLocalStoreToRemote( string storePath, string remoteStoreUrl, string apiKey )
            {
                Throw.CheckNotNullArgument( storePath );
                Throw.CheckNotNullArgument( remoteStoreUrl );
                // The 'NoRemote' url is checked inside PushComponents.
                using( LocalStore? store = LocalStore.OpenOrCreate( _monitor.Monitor, storePath ) )
                {
                    return store != null
                            && store.PushComponents( comp => true, new Uri( remoteStoreUrl, UriKind.Absolute ), apiKey );
                }
            }

            public NormalizedPath GetTestStorePath( string? suffix = null, [CallerMemberName]string? name = null )
            {
                return _localStoresFolder.AppendPart( name + suffix );
            }

            public NormalizedPath GetCleanTestStorePath( string? suffix = null, [CallerMemberName]string? name = null )
            {
                var p = GetTestStorePath( suffix, name );
                if( Directory.Exists( p ) ) _monitor.CleanupFolder( p );
                return p;
            }
        }

        internal CKSetupTestHelper( TestHelperConfiguration config, IMonitorTestHelper monitor )
        {
            _driver = new Driver( config, monitor );
        }

        ICKSetupDriver ICKSetupTestHelperCore.CKSetup => _driver;

        /// <summary>
        /// Gets the <see cref="ICKSetupTestHelper"/> mixin interface.
        /// </summary>
        public static ICKSetupTestHelper TestHelper => TestHelperResolver.Default.Resolve<ICKSetupTestHelper>();

    }
}
