using CK.Core;
using CK.Monitoring.InterProcess;
using CSemVer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// This facade encapsulates the CKSetup run operations.
    /// </summary>
    static public class Facade
    {
        /// <summary>
        /// The default store is the directory Environment.SpecialFolder.LocalApplicationData/CKSetupStore
        /// (ie. '%LocalAppData%/CKSetupStore' folder).
        /// This is the default shared store that is user and machine specific.
        /// <para>
        /// Note that it may not exist when no user session exists (the resulting path on windows is 'C:\Windows\system32\config\systemprofile' and
        /// this is not writable by "normal" users). No attempt is made to create the directory here: it is the <see cref="StreamStore.DirectoryStreamStore.DirectoryStreamStore(string)"/>
        /// constructor that ensures that the directory exists. 
        /// </para>
        /// </summary>
        public static readonly string DefaultStorePath;

        /// <summary>
        /// The default remote store is https://cksetup.invenietis.net.
        /// </summary>
        public static readonly Uri DefaultStoreUrl;

        static Facade()
        {
            var p = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData, Environment.SpecialFolderOption.DoNotVerify ), "CKSetupStore" );
            DefaultStorePath = p;
            DefaultStoreUrl = new Uri( "https://cksetup.invenietis.net" );
        }

        /// <summary>
        /// Locates a store that must be used (if the directory does not already exist, it is created).
        /// This method ultimately defaults to <see cref="DefaultStorePath"/>.
        /// <list type="bullet">
        /// <item>
        /// If a <paramref name="storePath"/> is provided, it will be used. The environment variable "CKSETUP_STORE" like any other environment variables
        /// is expanded.
        /// If the <see cref="Environment.ExpandEnvironmentVariables(string)"/> of this path leads to a path with <see cref="Path.GetInvalidPathChars"/> in it,
        /// an <see cref="InvalidOperationException"/> is thrown, but if the resulting path still contains a '%', this is just a warning (a referenced environment
        /// variable is missing).
        /// </item>
        /// <item>
        /// When the step above failed to resolve a path, a "CKSetupStore.txt" file (that must contain the final store path)
        /// is searched from <paramref name="startPath"/> up to the root (if startPath is not null).
        /// </item>
        /// <item>
        /// When the step above failed to resolve a path, then the same lookup of a "CKSetupStore.txt" file is done from
        /// current <see cref="AppContext.BaseDirectory"/> up to the root.
        /// </item>
        /// <item>
        /// Ultimately, <see cref="DefaultStorePath"/> is used.
        /// </item>
        /// </list>
        /// </summary>
        /// <param name="storePath">A path to an existing directory store or to a store that will be created.</param>
        /// <param name="startPath">An optional path to look for a store up to the root.</param>
        /// <param name="startFromBaseDirectory">False to disable lookup from the current <see cref="AppContext.BaseDirectory"/>.</param>
        /// <param name="monitor">Optional monitor to trace the decisions.</param>
        /// <returns>The final local store path to use.</returns>
        public static string LocateStorePath( string? storePath = "%CKSETUP_STORE%",
                                              string? startPath = null,
                                              bool startFromBaseDirectory = true,
                                              IActivityMonitor? monitor = null )
        {
            using( monitor?.OpenInfo( "Locating CKSetup Store." ) )
            {
                string? result;
                if( !string.IsNullOrEmpty( storePath ) )
                {
                    result = Environment.ExpandEnvironmentVariables( storePath );
                    bool isValidPath = true;
                    if( !string.IsNullOrEmpty( result )
                        && !result.Contains( '%' )
                        && (isValidPath = FileUtil.IndexOfInvalidPathChars( result ) < 0) )
                    {
                        return Found( monitor, result, $"From provided '{storePath}' path." );
                    }
                    if( result.Contains( "%CKSETUP_STORE%" ) )
                    {
                        // This is the most common case. This is not an error nor a warning, just a trace.
                        monitor?.Trace( $"Environment variable CKSETUP_STORE is not defined." );
                    }
                    else
                    {
                        if( !isValidPath )
                        {
                            // This is a serious error.
                            Throw.InvalidOperationException( $"Expanding environment variables from '{storePath}' resulted in an invalid path: '{result}'." );
                        }
                        if( string.IsNullOrEmpty( result ) )
                        {
                            monitor?.Warn( $"Expanding environment variables from '{storePath}' leads to an empty path. It is ignored." );
                        }
                        else
                        {
                            monitor?.Warn( $"Expanding environment variables from '{storePath}': the resulting path '{result}' must not contain '%' anymore (used environment variables must exist). It is ignored." );
                        }
                    }
                }
                if( !string.IsNullOrEmpty( startPath ) )
                {
                    result = FindStorePathFrom( monitor, startPath );
                    if( result != null ) return Found( monitor, result, $"From '{startPath}' up to the root." );
                    monitor?.Trace( $"Unable to find any 'CKSetupStore.txt' file from '{startPath}' up to the root." );
                }
                if( startFromBaseDirectory )
                {
                    result = FindStorePathFrom( monitor, AppContext.BaseDirectory );
                    if( result != null ) return Found( monitor, result, $"From AppContext.BaseDirectory = '{AppContext.BaseDirectory}' up to the root." );
                    monitor?.Trace( $"Unable to find any 'CKSetupStore.txt' file from AppContext.BaseDirectory = '{AppContext.BaseDirectory}' up to the root." );
                }
                return Found( monitor, DefaultStorePath, $"Using DefaultStorePath: {DefaultStorePath}." );
            }

            static string Found( IActivityMonitor? m, string result, string message )
            {
                result = Path.GetFullPath( result );
                m?.Info( message );
                if( !Directory.Exists( result ) )
                {
                    m?.Warn( $"CKSetupStore directory '{result}' does not exist. It is created." );
                    Directory.CreateDirectory( result );
                }
                m?.CloseGroup( $"Found: {result}." );
                return result;
            }

            static string? FindStorePathFrom( IActivityMonitor? m, string? dir )
            {
                while( !string.IsNullOrEmpty( dir ) )
                {
                    string test = Path.Combine( dir, "CKSetupStore.txt" );
                    if( File.Exists( test ) )
                    {
                        m?.Info( $"Found 'CKSetupStore.txt' redirection file: {test}." );
                        return File.ReadAllText( test );
                    }
                    dir = Path.GetDirectoryName( dir );
                }
                return null;
            }
        }

        /// <summary>
        /// Read multiple <see cref="BinFolder"/>.
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="paths">Folder paths to read.</param>
        /// <returns>The list of bin folders or null if an error occurred.</returns>
        public static List<BinFolder>? ReadBinFolders( IActivityMonitor monitor, IEnumerable<string> paths )
        {
            var result = new List<BinFolder>();
            using( monitor.OpenDebug( "Discovering files." ) )
            {
                try
                {
                    foreach( var d in paths )
                    {
                        var f = BinFolder.ReadBinFolder( monitor, d );
                        if( f == null ) return null;
                        result.Add( f );
                    }
                }
                catch( Exception ex )
                {
                    monitor.Fatal( ex );
                    return null;
                }
            }
            if( result.Count == 0 )
            {
                monitor.Error( "No valid runtime files found." );
                return null;
            }
            return result;
        }

        /// <summary>
        /// Captures <see cref="EnsurePublished(IActivityMonitor, string, bool)"/> result.
        /// </summary>
        public readonly struct EnsurePublishResult
        {
            /// <summary>
            /// Gets whether the publish succeeds.
            /// </summary>
            public bool Success { get; }

            /// <summary>
            /// Gets the published path if it is a netcore folder. Empty otherwise.
            /// </summary>
            public NormalizedPath PublishedPath { get; }

            /// <summary>
            /// Gets the final path: <see cref="PublishedPath"/> for netcore folder or the original folder
            /// for .Net framework or netstandard folder.
            /// </summary>
            public NormalizedPath FinalPath { get; }

            internal EnsurePublishResult( bool success, NormalizedPath publishedPath, NormalizedPath path )
            {
                Success = success;
                PublishedPath = publishedPath;
                FinalPath = publishedPath.IsEmptyPath ? path : publishedPath;
            }

        }

        /// <summary>
        /// Runs "dotnet publish" on an output bin folder if it needs to be published.
        /// The path can end with "/publish" or be the path above (typically "bin/Debug/netcoreapp3.1").
        /// <para>
        /// </para>
        /// The configuration (Debug/Release) and target framework (net5.0 for instance) are extracted
        /// from the path. When called on .Net framework (like 'net461' folders) or on net standard
        /// targets (like 'netstandard2.0'), nothing is done, a warning is emitted and an empty path is returned
        /// without any error.
        /// <para>
        /// If /publish folder exists and contains the project file name (.exe or .dll) with a LastWriteTime equal
        /// or greater than the one of the file in <paramref name="pathToFramework"/>, publish is skipped
        /// unless <paramref name="forcePublish"/> is true.
        /// </para>
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="pathToFramework">Path to the framework (typically ends with "bin/Debug/netcoreapp3.1") or to the "/publish".</param>
        /// <param name="forcePublish">
        /// True to force the publication in .net core even if the "publish/" folder is up to date.
        /// </param>
        /// <returns>A result object.</returns>
        static public EnsurePublishResult EnsurePublished( IActivityMonitor monitor, string pathToFramework, bool forcePublish = false )
        {
            Throw.CheckNotNullArgument( pathToFramework ); 
            return DoEnsurePublished( monitor, Path.GetFullPath( pathToFramework ), forcePublish );
        }

        internal static EnsurePublishResult DoEnsurePublished( IActivityMonitor monitor, NormalizedPath path, bool forcePublish )
        {
            using( monitor.OpenInfo( $"Ensure '{path}' is published (forcePublish={forcePublish})." ) )
            {
                NormalizedPath publishPath = new NormalizedPath();
                try
                {
                    if( IsPublishedRequired( monitor, ref path, forcePublish, out publishPath ) )
                    {
                        Debug.Assert( publishPath.LastPart == "publish" );
                        // publishPath is: project/bin/Debug/netcoreapp2.1/publish

                        // If the path (not the publishPath one) doesn't exist, it's over.
                        if( !Directory.Exists( path ) )
                        {
                            monitor.Error( $"Folder '{path}' doesn't exist." );
                        }
                        else
                        {
                            var framework = publishPath.Parts[publishPath.Parts.Count - 2];
                            var configuration = publishPath.Parts[publishPath.Parts.Count - 3];
                            var projectPath = publishPath.RemoveLastPart( 4 );
                            var projectName = projectPath.LastPart;
                            var pI = new ProcessStartInfo()
                            {
                                WorkingDirectory = projectPath,
                                FileName = "dotnet",
                                // The NoBuild option is available since 2.1: https://github.com/dotnet/cli/issues/5331
                                Arguments = $"publish -c {configuration} -f {framework} --no-build",
                                RedirectStandardOutput = true,
                                RedirectStandardError = true,
                                RedirectStandardInput = true,
                                UseShellExecute = false,
                                CreateNoWindow = true
                            };
                            // PERF: 20% of the time spent there on cold setup. 1.5 secs per call.
                            // But we need to work on a published, fully resolved, folder.
                            // IsPublishedRequired does its job to avoid any useless publish.
                            using( monitor.OpenInfo( $"Publishing (x{(Environment.Is64BitProcess ? "64" : "32")}) '{projectName}': {projectPath} > dotnet {pI.Arguments}" ) )
                            using( Process cmdProcess = new Process() )
                            {
                                //using( monitor.OpenDebug( ActivityMonitor.Tags.SecurityCritical, "Environment variables:" ) )
                                //{
                                //    foreach( System.Collections.DictionaryEntry env in Environment.GetEnvironmentVariables() )
                                //    {
                                //        monitor.Debug( $"{env.Key} => {env.Value}" );
                                //    }
                                //}
                                StringBuilder bErr = new StringBuilder();
                                cmdProcess.StartInfo = pI;
                                cmdProcess.ErrorDataReceived += ( o, e ) => { if( !string.IsNullOrEmpty( e.Data ) ) bErr.Append( "<StdErr> " ).AppendLine( e.Data ); };
                                cmdProcess.OutputDataReceived += ( o, e ) => { if( e.Data != null ) monitor.Info( e.Data ); };
                                cmdProcess.Start();
                                cmdProcess.BeginErrorReadLine();
                                cmdProcess.BeginOutputReadLine();
                                cmdProcess.WaitForExit( int.MaxValue ); // First we need for the program to exit.
                                // Here the program exited, but background may still pump messages
                                ReflectionHack( cmdProcess ); // This will cancel the reads in the pipes background loop.

                                // This method shut down things in <see cref="Process"/>. Call it when you know that the process has exited.
                                // Sometimes the <see cref="Process"/> class deadlock itself.
                                // See this for more info: https://github.com/dotnet/runtime/issues/51277
                                static void ReflectionHack( Process process )
                                {
                                    const BindingFlags bindingFlags = BindingFlags.NonPublic | BindingFlags.Instance;
                                    FieldInfo? outputField = typeof( Process ).GetField( "_output", bindingFlags );
                                    FieldInfo? errorField = typeof( Process ).GetField( "_error", bindingFlags );
                                    ((IDisposable)outputField!.GetValue( process )!).Dispose();
                                    ((IDisposable)errorField!.GetValue( process )!).Dispose();
                                }

                                cmdProcess.WaitForExit();     // This allow to wait for the 2 pipes async to finish looping and flushing the last messages.
                                                              // Here you shouldn't receive any message.

                                if( bErr.Length > 0 )
                                {
                                    monitor.Error( bErr.ToString() );
                                }
                                if( cmdProcess.ExitCode != 0 )
                                {
                                    monitor.Error( $"Process returned ExitCode {cmdProcess.ExitCode}." );
                                    return new EnsurePublishResult( false, publishPath, path );
                                }
                            }
                        }
                    }
                    else monitor.CloseGroup( "Skipped." );
                    return new EnsurePublishResult( true, publishPath, path );
                }
                catch( Exception ex )
                {
                    monitor.Error( ex );
                }
                return new EnsurePublishResult( false, publishPath, new NormalizedPath() );
            }
        }

        static bool IsPublishedRequired( IActivityMonitor monitor, ref NormalizedPath path, bool forcePublish, out NormalizedPath publishPath )
        {
            if( path.LastPart == "publish" )
            {
                publishPath = path;
                path = publishPath.RemoveLastPart();
            }
            else publishPath = path.AppendPart( "publish" );

            var framework = path.LastPart;
            if( !framework.StartsWith( "netcoreapp", StringComparison.OrdinalIgnoreCase )
                && !framework.StartsWith( "net5", StringComparison.OrdinalIgnoreCase )
                && !framework.StartsWith( "net6", StringComparison.OrdinalIgnoreCase )
                && !framework.StartsWith( "net7", StringComparison.OrdinalIgnoreCase )
                && !framework.StartsWith( "net8", StringComparison.OrdinalIgnoreCase )
                && !framework.StartsWith( "net9", StringComparison.OrdinalIgnoreCase ) )
            {
                publishPath = new NormalizedPath();
                monitor.Warn( "Not a /netcoreapp nor /net5+ path." );
                return false;
            }
            monitor.Info( $"Parent folder is '{framework}': it can be published." );
            if( forcePublish )
            {
                monitor.Trace( "Forcing publication." );
                return true;
            }
            // If there is no publish folder, we need to publish.
            if( !Directory.Exists( publishPath ) )
            {
                monitor.Trace( "No /publish directory." );
                return true;
            }
            var projectName = path.Parts[path.Parts.Count - 4];

            // If we don't find a .dll or .exe with the projectName, in doubt, we need to publish.
            var dllOrExeName = path.AppendPart( projectName );
            string source = dllOrExeName + ".dll";
            if( !File.Exists( source ) )
            {
                source = dllOrExeName + ".exe";
                if( !File.Exists( source ) )
                {
                    monitor.Warn( $"No {projectName} dll or exe found in base path '{path}'." );
                    return true;
                }
            }
            var pubTargetName = Path.GetFileName( source );
            var pubName = publishPath.AppendPart( pubTargetName );
            if( !File.Exists( pubName ) )
            {
                monitor.Trace( $"Target {pubTargetName} not found in /publish." );
                return true;
            }
            DateTime sourceTime = File.GetLastWriteTimeUtc( source );
            DateTime pubTime = File.GetLastWriteTimeUtc( pubName );
            bool older = pubTime < sourceTime;
            if( older )
            {
                monitor.Trace( $"{pubTargetName} is newer than publish/{pubTargetName}." );
                return true;
            }
            return false;
        }

        /// <summary>
        /// Main entry point to CKSetup run that takes an optional store to resolve
        /// missing components.
        /// The <paramref name="config"/> object is internally cloned before the run: it is not altered by the run.
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="store">The opened, valid, local store.</param>
        /// <param name="config">The setup configuration.</param>
        /// <param name="launchDebug">True to call <see cref="Debugger.Launch"/> at the very start of the runner process.</param>
        /// <param name="forceSetup">Combined with <see cref="SetupConfiguration.ForceSetup"/>: the stronger wins.</param>
        /// <param name="remoteUri">Explicit remote store Url to use. When let to null, an attempt to locate an ambient store is made.</param>
        /// <param name="apiKey">Optional api key for the <paramref name="remoteUri"/>.</param>
        /// <returns>The <see cref="CKSetupRunResult"/> enum.</returns>
        public static CKSetupRunResult DoRun( IActivityMonitor monitor,
                                              LocalStore store,
                                              SetupConfiguration config,
                                              bool launchDebug,
                                              ForceSetupLevel forceSetup = ForceSetupLevel.None,
                                              Uri? remoteUri = null,
                                              string? apiKey = null )
        {
            Throw.CheckNotNullArgument( monitor );
            Throw.CheckNotNullArgument( store );
            using( IRemoteStore? remote = store.CreateStandardRemote( monitor, remoteUri, apiKey ) )
            {
                return DoRun( monitor, store, config, remote, launchDebug, forceSetup );
            }
        }

        /// <summary>
        /// Main entry point to CKSetup run that takes an explicit <see cref="IComponentImporter"/> additional store to resolve
        /// missing components.
        /// The <paramref name="config"/> object is internally cloned before the run: it is not altered by the run.
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="store">The opened, valid, local store.</param>
        /// <param name="config">The setup configuration.</param>
        /// <param name="missingImporter">Optional component importer. Can be null: only the <see cref="LocalStore.PrototypeStoreUrl"/> is used to resolve missing components.</param>
        /// <param name="launchDebug">True to call <see cref="Debugger.Launch"/> at the very start of the runner process.</param>
        /// <param name="forceSetup">Combined with <see cref="SetupConfiguration.ForceSetup"/>: the stronger wins.</param>
        /// <returns>The <see cref="CKSetupRunResult"/> enum.</returns>
        public static CKSetupRunResult DoRun( IActivityMonitor monitor,
                                              LocalStore store,
                                              SetupConfiguration config,
                                              IComponentImporter? missingImporter,
                                              bool launchDebug,
                                              ForceSetupLevel forceSetup )
        {
            Throw.CheckNotNullArgument( monitor );
            Throw.CheckNotNullArgument( store );
            Throw.CheckNotNullArgument( config );

            var previousGates = StaticGateConfigurator.GetConfiguration();
            StaticGateConfigurator.ApplyConfiguration( monitor, config.StaticGates ?? String.Empty );

            config = config.Clone();
            using( monitor.OpenInfo( $"Running Setup." ) )
            {
                SetupContext ctx;
                BufferedFileMonitorClient? fileClient;
                // Ensures that the level is at least Monitor while creating the SetupContext.
                // The Warn when the signature match is necessarily emitted.
                using( monitor.TemporarilySetMinimalFilter( monitor.ActualFilter.Combine( ActivityMonitor.DefaultFilter ).Combine( LogFilter.Monitor ) ) )
                {
                    fileClient = config.WorkingDirectoryHasLogs ? BufferedFileMonitorClient.RegisterClient( monitor ) : null;
                    monitor.Info( GetCKSetupCoreVersion() );
                    ctx = SetupContext.Create( monitor, config, forceSetup, fileClient );
                    if( !ctx.Success ) return CKSetupRunResult.Failed;
                    if( ctx.UpToDate )
                    {
                        return CKSetupRunResult.UpToDate;
                    }
                }
                // Applies the LogLevel from the configuration to the monitor itself. The change is scoped to this opened group.
                var level = monitor.ActualFilter.Combine( config.LogLevel );
                if( level != monitor.ActualFilter )
                {
                    monitor.UnfilteredLog( LogLevel.Info, null, $"LogLevel changed from {monitor.ActualFilter} to {level}.", null );
                    monitor.MinimalFilter = level;
                }
                var r = ActualRun( ctx );
                if( fileClient != null )
                {
                    fileClient.Dispose();
                    monitor.Output.UnregisterClient( fileClient );
                }
                monitor.CloseGroup( r );
                StaticGateConfigurator.ApplyConfiguration( monitor, previousGates );
                return r;
            }

            CKSetupRunResult ActualRun( SetupContext ctx )
            {
                try
                {
                    Debug.Assert( ctx.BinFolders != null && ctx.DedupFiles != null );
                    Debug.Assert( Directory.Exists( ctx.FinalWorkingDirectory ) );

                    if( !ctx.WorkingDirectoryIsReady )
                    {
                        var dependencies = config.Dependencies;

                        // If no explicit dependencies have been set on CKSetup.Runner...
                        if( !dependencies.Any( d => d.UseName == "CKSetup.Runner" ) )
                        {
                            var thisAssembly = (AssemblyInformationalVersionAttribute?)Attribute.GetCustomAttribute( Assembly.GetExecutingAssembly(), typeof( AssemblyInformationalVersionAttribute ) );
                            var thisVersion = new InformationalVersion( thisAssembly?.InformationalVersion );
                            // ...use the ZeroVersion when compiling uncommitted work
                            // ...or this CKSetup.Core version.
                            SVersion runnerVersion = thisVersion.Version ?? SVersion.ZeroVersion;
                            dependencies.Add( new SetupDependency( "CKSetup.Runner", runnerVersion ) );
                        }

                        using( monitor.OpenInfo( $"Copying {ctx.DedupFiles.Count} files from bin folders." ) )
                        {
                            var dedupFolders = new HashSet<NormalizedPath>( ctx.DedupFiles.Keys.Select( p => new NormalizedPath( p ).RemoveLastPart() ) );
                            foreach( var requiredFolder in dedupFolders )
                            {
                                var targetPath = ctx.FinalWorkingDirectory.Combine( requiredFolder );
                                if( !Directory.Exists( targetPath ) )
                                {
                                    monitor.Debug( $"Creating '{targetPath}' directory." );
                                    Directory.CreateDirectory( targetPath );
                                }
                            }
                            foreach( var f in ctx.DedupFiles.Values )
                            {
                                string targetPath = ctx.FinalWorkingDirectory.Combine( f.LocalFileName );
                                File.Copy( f.FullPath, targetPath );
                            }
                        }

                        bool RuntimeFileSkipper( IActivityMonitor m, ComponentFile c, NormalizedPath targetPath )
                        {
                            bool alreadyHere = File.Exists( targetPath );
                            if( alreadyHere )
                            {
                                m.Debug( $"File {c.Name} already exists." );
                            }
                            return !alreadyHere;
                        }

                        var extractResult = store.ExtractRuntimeDependencies( ctx.FinalWorkingDirectory,
                                                                              ctx.BinFolders,
                                                                              missingImporter,
                                                                              dependencies,
                                                                              config.PreferredTargetRuntimes,
                                                                              RuntimeFileSkipper );
                        if( !extractResult.Success )
                        {
                            return CKSetupRunResult.Failed;
                        }
                        using( monitor.OpenInfo( $"Generating CKSetup.Runner.Config.xml file." ) )
                        {
                            XElement AddCategorizedAssemblies( XElement parent, IEnumerable<BinFileAssemblyInfo> assemblies )
                            {
                                XElement ToXml( XName name, BinFileAssemblyInfo info )
                                {
                                    var e = new XElement( name );
                                    e.Value = info.Name.Name;
                                    var v = info.InfoVersion?.Version;
                                    if( v != null && v.IsValid ) e.Add( new XAttribute( SetupConfiguration.xSVersion, v.ToNormalizedString() ) );
                                    return e;
                                }
                                var models = assemblies.Where( a => a.ComponentKind == ComponentKind.Model );
                                var setupDependencies = assemblies.Where( a => a.ComponentKind == ComponentKind.SetupDependency );
                                var modelDependents = assemblies.Where( a => a.ComponentKind == ComponentKind.None && (a.IsModelDependent || a.LocalDependencies.Any( dep => dep.IsModelDependentSource )) );
                                parent.Add( models.Select( a => ToXml( SetupConfiguration.xModel, a ) ) );
                                parent.Add( setupDependencies.Select( a => ToXml( SetupConfiguration.xDependency, a ) ) );
                                parent.Add( modelDependents.Select( a => ToXml( SetupConfiguration.xModelDependent, a ) ) );
                                return parent;
                            }

                            // Configuration has been cloned.
                            var binPathsComment = new XComment( "BinPaths elements: 'Path' attribute is the configured path, 'BinPath' is the normalized path that may be combined with 'BasePath' and changed to '/publish' sub folder if needed." );
                            if( config.LogLevel == LogFilter.Undefined )
                            {
                                config.LogLevel = monitor.ActualFilter;
                                // This is required to take into account (to transfer) the static ActivityMonitor.DefaultFilter
                                // value if it differs from the default.
                                if( config.LogLevel == LogFilter.Undefined ) config.LogLevel = ActivityMonitor.DefaultFilter;
                            }
                            XElement root = config.Configuration;
                            root.Add( new XElement( SetupConfiguration.xRunSignature, ctx.RunSignature.ToString() ) );
                            if( ctx.Configuration.ForceSetup == ForceSetupLevel.Full )
                            {
                                root.Add( new XElement( SetupConfiguration.xForceRun, true ) );
                            }
                            if( config.SharedConfigurationMode )
                            {
                                root.Element( SetupConfiguration.xBinPaths )?.AddBeforeSelf( binPathsComment );
                                foreach( BinFolder f in ctx.BinFolders )
                                {
                                    // Finds all the BinPath element for this Path and provide them the same set of assemblies.
                                    // The fact that more than one BinPath share the same Path is not our concern here: we want an engine
                                    // to be able to work on the same path but with different configurations.
                                    // TODO: THIS IS CURRENTLY NOT SUPPORTED. There is a 1-1 association between <BinPath Path="..."> and
                                    //       the BinPaths collection configuration.
                                    //       We should not support any more the Independent mode. Now the SharedMode is the default and it
                                    //       should be the only one.
                                    //       BinPaths XElement should be simply exposed and only during SetupContext.Create should we care
                                    //       about their correctness (checking the Path attributes to be a NormalizedPath).
                                    //       We should simply do our job on the distinct Path and then do this (below): populates the BinPaths
                                    //       with the Model and ModelDependent assemblies.
                                    //
                                    foreach( var e in config.BinPathElements.Where( e => e.Attribute( SetupConfiguration.xBinPath )?.Value == f.BinPath ) )
                                    {
                                        var assemblies = e.Element( SetupConfiguration.xAssemblies );
                                        if( assemblies == null ) e.Add( assemblies = new XElement( SetupConfiguration.xAssemblies ) );
                                        AddCategorizedAssemblies( assemblies, f.Assemblies );
                                    }
                                }
                            }
                            else
                            {
                                root.Add( new XAttribute( SetupConfiguration.xEngine, config.EngineAssemblyQualifiedName ),
                                          new XElement( SetupConfiguration.xCKSetupName, config.CKSetupName ),
                                          new XElement( SetupConfiguration.xBasePath, ctx.BasePath ),
                                          new XElement( SetupConfiguration.xLogLevel, config.LogLevel.ToString() ),
                                          new XElement( SetupConfiguration.xStaticGates, config.StaticGates ),
                                          binPathsComment,
                                          new XElement( SetupConfiguration.xBinPaths,
                                                  ctx.BinFolders.Select( f => AddCategorizedAssemblies(
                                                                              // We clone the BinPath XElement to get the non-normalized 'Path' attribute. 
                                                                              new XElement( config.BinPathElements.Single( e => e.Attribute( SetupConfiguration.xBinPath )?.Value == f.BinPath ) ),
                                                                              f.Assemblies ) ) ) );
                                // Supports runners < v10 (tested with v9.3.0 released on the 27ths of July, 2019).
                                root.Add( new XElement( SetupConfiguration.xCKSetup, new XElement( SetupConfiguration.xEngineAssemblyQualifiedName, config.EngineAssemblyQualifiedName ) ) );
                            }
                            monitor.Info( root.ToString() );
                            new XDocument( root ).Save( Path.Combine( ctx.FinalWorkingDirectory, "CKSetup.Runner.Config.xml" ) );
                        }
                    }
                    if( RunSetupRunner( monitor, ctx.FinalWorkingDirectory, ctx.WorkingDirectoryIsReady, launchDebug ) )
                    {
                        // If the folder has been reused, the signature files are okay.
                        if( !ctx.WorkingDirectoryIsReady )
                        {
                            UpdateFileSignatures( monitor, ctx.FinalWorkingDirectory, ctx.BinFolders, ctx.RunSignature );
                        }
                        return CKSetupRunResult.Succeed;
                    }
                    // On failure, we set the SHA1 to the Zero.
                    UpdateFileSignatures( monitor, ctx.FinalWorkingDirectory, ctx.BinFolders, SHA1Value.Zero );
                }
                catch( Exception ex )
                {
                    monitor.Fatal( ex );
                }
                finally
                {
                    if( ctx.FinalWorkingDirectory.StartsWith( Path.GetTempPath() ) )
                    {
                        SafeDeleteFolder( monitor, ctx.FinalWorkingDirectory );
                    }
                    else monitor.Trace( $"Leaving Working Directory '{ctx.FinalWorkingDirectory}' as-is." );
                }
                return CKSetupRunResult.Failed;
            }
        }

        static string GetCKSetupCoreVersion()
        {
            var thisFramework = Assembly.GetExecutingAssembly().CustomAttributes
                                        .Where( x => x.AttributeType.FullName == "System.Runtime.Versioning.TargetFrameworkAttribute" )
                                        .Select( x => x.ConstructorArguments )
                                        .Where( p => p.Count > 0 )
                                        .Select( p => p[0].Value as string )
                                        .FirstOrDefault();
            var a = (AssemblyInformationalVersionAttribute?)Attribute.GetCustomAttribute( typeof( Facade ).Assembly, typeof( AssemblyInformationalVersionAttribute ) );
            return "CKSetup.Core " + (thisFramework ?? "<no TargetFrameworkAttribute>") + " - " + (a != null ? a.InformationalVersion : "<no version>");
        }

        static void UpdateFileSignatures( IActivityMonitor monitor,
                                          string workingFolder,
                                          IReadOnlyList<BinFolder> binFolders,
                                          SHA1Value currentSignature )
        {
            bool delete = currentSignature == SHA1Value.Zero;
            var msg = delete ? "Deleting signature files." : "Updating signature files.";
            using( monitor.OpenTrace( msg ) )
            {
                var fSign = Path.Combine( workingFolder, SetupContext.SignatureFileName );
                if( delete )
                {
                    if( File.Exists( fSign ) ) File.Delete( fSign );
                }
                else
                {
                    File.WriteAllText( fSign, currentSignature.ToString() );
                }
                foreach( var f in binFolders )
                {
                    try
                    {
                        fSign = Path.Combine( f.BinPath, SetupContext.SignatureFileName );
                        if( delete )
                        {
                            if( File.Exists( fSign ) ) File.Delete( fSign );
                        }
                        else
                        {
                            File.WriteAllText( fSign, currentSignature.ToString() );
                        }
                    }
                    catch( Exception ex )
                    {
                        monitor.Warn( msg, ex );
                    }
                }
            }
        }

        internal static void SafeDeleteFolder( IActivityMonitor monitor, string dir )
        {
            using( monitor.OpenInfo( $"Deleting folder: {dir}" ) )
            {
                int retryCount = 0;
                for(; ; )
                {
                    try
                    {
                        Directory.Delete( dir, true );
                        break;
                    }
                    catch( Exception ex )
                    {
                        if( ++retryCount < 4 )
                        {
                            monitor.Warn( $"Error while deleting folder. Retrying (count = {retryCount}).", ex );
                            System.Threading.Thread.Sleep( 200 * retryCount );
                        }
                        else
                        {
                            monitor.Error( $"Unable to delete folder '{dir}'.", ex );
                            break;
                        }
                    }
                }
            }
        }

        static bool RunSetupRunner( IActivityMonitor m,
                                    string workingDir,
                                    bool workingDirIsReady,
                                    bool launchDebug )
        {
            using( m.OpenInfo( "Launching CKSetup.Runner process." ) )
            {
                string deps = Path.Combine( workingDir, "CKSetup.Runner.deps.json" );
                string fileName, arguments;

                if( File.Exists( deps ) )
                {
                    // .NET
                    string dll = Path.Combine( workingDir, "CKSetup.Runner.dll" );
                    if( !File.Exists( dll ) )
                    {
                        m.Error( "Unable to find CKSetup.Runner in folder." );
                        return false;
                    }
                    fileName = "dotnet";

                    // Merging all deps.json into CKSetup.Runner.deps.json
                    if( !workingDirIsReady )
                    {
                        arguments = "CKSetup.Runner.dll merge-deps";
                        if( launchDebug ) arguments += " /launchDebug";
                        if( !RunCKSetupRunnerProcess( m, workingDir, fileName, arguments ) )
                        {
                            return false;
                        }

                        string depsFile = Path.Combine( workingDir, "CKSetup.Runner.deps.json" );
                        string depsBackup = depsFile + ".original";
                        File.Replace( depsFile + ".merged", depsFile, depsBackup );

                        string rcFile = Path.Combine( workingDir, "CKSetup.Runner.runtimeconfig.json" );
                        string rcMerged = rcFile + ".merged";
                        // RuntimeConfig swap is not supported by older CKSetup.Runners below 10.1.1--0011-develop
                        if( File.Exists( rcMerged ) )
                        {
                            string rcBackup = rcFile + ".original";
                            File.Replace( rcMerged, rcFile, rcBackup );
                        }
                    }
                    arguments = "CKSetup.Runner.dll ";
                }
                else
                {
                    // .Net frameworks
                    fileName = Path.Combine( workingDir, "CKSetup.Runner.exe" );
                    arguments = String.Empty;
                }
                if( launchDebug ) arguments += "/launchDebug ";
                return RunCKSetupRunnerProcess( m, workingDir, fileName, arguments );
            }
        }

        static bool RunCKSetupRunnerProcess( IActivityMonitor m,
                                             string workingDir,
                                             string fileName,
                                             string arguments )
        {
            ProcessStartInfo cmdStartInfo = new ProcessStartInfo
            {
                WorkingDirectory = workingDir,
                UseShellExecute = false,
                CreateNoWindow = true,
                FileName = fileName,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
            };
            using( var logReceiver = SimpleLogPipeReceiver.Start( m, true ) )
            {
                cmdStartInfo.Arguments = arguments;
                cmdStartInfo.Arguments += " /silent /logPipe:" + logReceiver.PipeName;
                using( m.OpenTrace( $"{fileName} {cmdStartInfo.Arguments}" ) )
                using( Process cmdProcess = new Process() )
                {
                    StringBuilder conOut = new StringBuilder();
                    cmdProcess.StartInfo = cmdStartInfo;
                    cmdProcess.ErrorDataReceived += ( o, e ) => { if( !string.IsNullOrEmpty( e.Data ) ) conOut.Append( "<StdErr> " ).AppendLine( e.Data ); };
                    cmdProcess.OutputDataReceived += ( o, e ) => { if( e.Data != null ) conOut.Append( "<StdOut> " ).AppendLine( e.Data ); };
                    cmdProcess.Start();
                    cmdProcess.BeginErrorReadLine();
                    cmdProcess.BeginOutputReadLine();
                    cmdProcess.WaitForExit();

                    if( conOut.Length > 0 ) m.Info( conOut.ToString() );
                    var endLogStatus = logReceiver.WaitEnd( cmdProcess.ExitCode != 0 );
                    if( endLogStatus != LogReceiverEndStatus.Normal )
                    {
                        m.Warn( $"Pipe log channel abnormal end status: {endLogStatus}." );
                    }
                    if( cmdProcess.ExitCode != 0 )
                    {
                        const string rawLogFileName = "CKSetup.Runner.RawLogs.txt";
                        using( m.OpenError( $"Process returned ExitCode {cmdProcess.ExitCode}. {rawLogFileName} file content:" ) )
                        {
                            var rawLog = Path.Combine( workingDir, rawLogFileName );
                            if( File.Exists( rawLog ) )
                            {
                                m.Debug( File.ReadAllText( rawLog ) );
                            }
                            else m.CloseGroup( $"No {rawLogFileName} file." );
                        }
                        return false;
                    }
                    return true;
                }
            }
        }

    }
}
