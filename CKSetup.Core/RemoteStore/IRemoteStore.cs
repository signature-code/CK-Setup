using System;
using System.Collections.Generic;
using System.Text;

namespace CKSetup
{
    /// <summary>
    /// Unifies <see cref="IComponentPushTarget"/> and <see cref="IComponentImporter"/>.
    /// This must be disposed as soon as possible.
    /// </summary>
    public interface IRemoteStore : IComponentImporter, IComponentPushTarget, IDisposable
    {
    }
}
