using CK.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// Describes basic configuration object.
    /// This configuration is expressed as Xml in two forms: the configuration of the Engine that will
    /// be run (defined by <see cref="EngineAssemblyQualifiedName"/>) can be independent of the one of CKSetup itself
    /// or mixed with it. See <see cref="SharedConfigurationMode"/> for more information.
    /// </summary>
    public class SetupConfiguration
    {
        /// <summary>
        /// The default number of CKSetup folders that are kept is 5.
        /// </summary>
        public const int DefaultWorkingDirectoryKeepOnlyCount = 5;

        /// <summary>
        /// The maximum number of CKSetup folders that are kept is 50.
        /// </summary>
        public const int MaxWorkingDirectoryKeepOnlyCount = 50;

        #region Xml element names
        /// <summary>
        /// Name of the document root element when not in shared mode: this name is not relevant, we use just "Root"
        /// </summary>
        public static readonly XName xDocumentRootName = XNamespace.None + "Root";

        /// <summary>
        /// Name of the root CKSetup element. This must be the first element in the xml document,
        /// the second one being the engine configuration that becomes the root of runner configuration file
        /// (its name doesn't matter).
        /// A new CKSetup element is injected at the end of this root of runner configuration file that
        /// contains the <see cref="xEngineAssemblyQualifiedName"/> element and <see cref="xBinPaths"/>.
        /// </summary>
        public static readonly XName xCKSetup = XNamespace.None + "CKSetup";

        /// <summary>
        /// BasePath element name.
        /// This element also appears in the runner configuration file's CKSetup element.
        /// </summary>
        public static readonly XName xBasePath = XNamespace.None + "BasePath";

        /// <summary>
        /// CKSetupName element name.
        /// This element also appears in the runner configuration file's CKSetup element.
        /// </summary>
        public static readonly XName xCKSetupName = XNamespace.None + "CKSetupName";

        /// <summary>
        /// Name of the EngineAssemblyQualifiedName.
        /// This element also appears in the runner configuration file's CKSetup element.
        /// </summary>
        public static readonly XName xEngineAssemblyQualifiedName = XNamespace.None + "EngineAssemblyQualifiedName";

        /// <summary>
        /// Short name for EngineAssemblyQualifiedName: "Engine" is enough.
        /// </summary>
        public static readonly XName xEngine = XNamespace.None + "Engine";

        /// <summary>
        /// Name of the BinPaths element in <see cref="xCKSetup"/>.
        /// This element also appears in the runner configuration file's CKSetup element but its <see cref="xBinPath"/>
        /// children are different..
        /// </summary>
        public static readonly XName xBinPaths = XNamespace.None + "BinPaths";

        /// <summary>
        /// Name of the <see cref="ExternalSignature"/> element.
        /// </summary>
        static readonly XName xExternalSignature = XNamespace.None + "ExternalSignature";

        /// <summary>
        /// BinPath elements carries the Path attribute to consider as a source for setup.
        /// The Path attribute is mutated when Setup context is prepared: it is fully rooted
        /// and [alt|ernate] pieces are resolved.
        /// "BinPath" is the name of the element in &lt;BinPaths&gt; AND the name of the attribute
        /// that contains the final path in the runner configuration file: this final path
        /// has potentially a "/publish" suffix.
        /// </summary>
        public static readonly XName xBinPath = XNamespace.None + "BinPath";

        /// <summary>
        /// Path attribute name. This Path attribute is mutated when Setup context is prepared: it is fully rooted
        /// and [alt|ernate] pieces are resolved.
        /// </summary>
        public static readonly XName xPath = XNamespace.None + "Path";

        /// <summary>
        /// This element appears in the runner configuration file's when <see cref="SharedConfigurationMode"/>
        /// is used as a child of CKSetup/BinPaths/BinPath elements with the <see cref="xModel"/>, <see cref="xDependency"/> and <see cref="xModelDependent"/>
        /// children elements (that contain an assembly name).
        /// <para>
        /// Note that when not in SharedConfigurationMode, the Model/ModelDependent elements are directly under each CKSetup/BinPaths/BinPath element:
        /// this Assemblies element is injected only when sharing configuration to limit potential name clashes.
        /// </para>
        /// </summary>
        public static readonly XName xAssemblies = XNamespace.None + "Assemblies";

        /// <summary>
        /// This element appears in the runner configuration file.
        /// Its value is the current signature and may typically be injected as a <see cref="FileVersionInfo"/>
        /// property of generated files or (if <see cref="xForceRun"/> doesn't also appear) used as a key to skip useless processes.
        /// </summary>
        public static readonly XName xRunSignature = XNamespace.None + "RunSignature";

        /// <summary>
        /// This element appears in the runner configuration file with a true content if
        /// and only if <see cref="SetupConfiguration.ForceSetup"/> is <see cref="ForceSetupLevel.Full"/>.
        /// </summary>
        public static readonly XName xForceRun = XNamespace.None + "ForceRun";

        /// <summary>
        /// Attribute name for <see cref="xModel"/>, <see cref="xDependency"/> and <see cref="xModelDependent"/>
        /// elements.
        /// </summary>
        public static readonly XName xSVersion = XNamespace.None + "SVersion";

        /// <summary>
        /// This element appears in the runner configuration file's CKSetup/BinPaths/BinPath elements.
        /// Its value is an assembly name that is a Model.
        /// </summary>
        public static readonly XName xModel = XNamespace.None + "Model";

        /// <summary>
        /// This element appears in the runner configuration file's CKSetup/BinPaths/BinPath elements.
        /// Its value is an assembly name that is a Dependency.
        /// </summary>
        public static readonly XName xDependency = XNamespace.None + "Dependency";

        /// <summary>
        /// Optional ordered set of <see cref="TargetRuntime"/> that will be selected
        /// if possible. First is better.
        /// It is a semicolon separated string of target runtimes: Net461;NetCoreApp20;NetCoreApp21.
        /// </summary>
        public static readonly XName xPreferredTargetRuntimes = XNamespace.None + "PreferredTargetRuntimes";

        /// <summary>
        /// LogLevel is actually a <see cref="LogFilter"/>.
        /// </summary>
        public static readonly XName xLogLevel = XNamespace.None + "LogLevel";

        /// <summary>
        /// This element appears in the runner configuration file's CKSetup/BinPaths/BinPath elements.
        /// Its value is an assembly name that depends on a Model.
        /// </summary>
        public static readonly XName xModelDependent = XNamespace.None + "ModelDependent";

        /// <summary>
        /// The default configuration name is "Configuration".
        /// </summary>
        public static readonly XName xConfigurationDefaultName = XNamespace.None + "Configuration";

        /// <summary>
        /// The static gates to configure.
        /// </summary>
        public static readonly XName xStaticGates = XNamespace.None + "StaticGates";

        static readonly XName xForceSetup = XNamespace.None + "ForceSetup";

        static readonly XName xWorkingDirectory = XNamespace.None + "WorkingDirectory";
        static readonly XName xKeepOnlyCount = XNamespace.None + "KeepOnlyCount";
        static readonly XName xHasLogs = XNamespace.None + "HasLogs";
        static readonly XName xDependencies = XNamespace.None + "Dependencies";
        #endregion

        readonly XElement _ckSetup;
        readonly PreferredTargetRuntimesList _preferredTargetRuntimes;
        readonly DependenciesList _dependenciesList;
        readonly BinPathList _binPathsList;
        XElement _configuration;

        ForceSetupLevel _forceSetup;

        /// <summary>
        /// Initializes a new empty configuration where <see cref="SharedConfigurationMode"/> is true by default.
        /// </summary>
        public SetupConfiguration()
            : this( true )
        {
        }

        /// <summary>
        /// Initializes a new empty configuration.
        /// </summary>
        /// <param name="sharedMode">Whether the CKSetup and Engine configurations are mixed in one element.</param>
        public SetupConfiguration( bool sharedMode )
        {
            _ckSetup = _configuration = new XElement( xCKSetup );
            if( !sharedMode )
            {
                _configuration = new XElement( xConfigurationDefaultName );
            }
            _preferredTargetRuntimes = new PreferredTargetRuntimesList( this );
            _dependenciesList = new DependenciesList( this );
            _binPathsList = new BinPathList( this );
        }

        /// <summary>
        /// Initializes a new configuration from a xml document.
        /// 2 formats are supported, and by default, for both of them, the <see cref="EngineAssemblyQualifiedName"/> must appear.
        /// If <paramref name="d"/> has a &lt;CKSetup&gt; child, then we use "Independent" mode. When there is no &lt;CKSetup&gt; child
        /// then the "Shared" mode is assumed.
        /// <para>
        /// With "Independent" configuration, the document must have exactly two elements: the first element must be &lt;CKSetup&gt; and is
        /// the configuration of the CKSetup process itself; the second one is the Engine <see cref="Configuration"/> (its name is not constrained).
        /// </para>
        /// <para>
        /// In "Shared" mode (easier if per BinPath configurations are required), the CKSetup process and Engine configuration are shared:
        /// the root element contains both configurations mixed (the root's name is not relevant).
        /// </para>
        /// </summary>
        /// <param name="d">A valid xml document.</param>
        /// <param name="setEngineName">
        /// Optional <see cref="EngineAssemblyQualifiedName"/> to set (may be null): this is particularly useful in <see cref="SharedConfigurationMode"/>
        /// to make the document "valid" without externally manipulating the Xml to add the Engine or EngineAssemblyQualifiedName attribute or element.
        /// </param>
        public SetupConfiguration( XDocument d, string? setEngineName = null )
        {
            Throw.CheckArgument( d?.Root?.HasElements ?? false );
            Debug.Assert( d != null && d.Root != null );

            // Look up for a CKSetup child element.
            var ckSetupElement = d.Root.Element( xCKSetup );
            if( ckSetupElement == null )
            {
                // No CKSetup child: the whole root is the shared configuration.
                _ckSetup = _configuration = new XElement( d.Root );
            }
            else 
            {
                _ckSetup = ckSetupElement;
                if( d.Root.Elements().First() != _ckSetup )
                {
                    Throw.ArgumentException( $"Independent Configuration Mode: first root's element must be '{xCKSetup}', not '{_ckSetup.Name}')." );
                }
                if( d.Root.Elements().Count() != 2 ) Throw.ArgumentException( $"Independent Configuration Mode: Xml document's root must have exactly 2 elements (found {d.Root.Elements().Count()})." );
                _ckSetup = new XElement( _ckSetup );
                _configuration = new XElement( d.Root.Elements().ElementAt( 1 ) );
            }
            // To secure things a little bit: the Engine or xEngineAssemblyQualifiedName attribute or element MUST appear on the CKSetup configuration element.
            if( setEngineName == null )
            {
                setEngineName = (string?)_ckSetup.Attribute( xEngineAssemblyQualifiedName )
                                 ?? (string?)_ckSetup.Attribute( xEngine )
                                 ?? (string?)_ckSetup.Element( xEngineAssemblyQualifiedName )
                                 ?? (string?)_ckSetup.Element( xEngine );
                if( setEngineName == null )
                {
                    if( _ckSetup == _configuration )
                        Throw.ArgumentException( $"Shared Configuration Mode: root element '{d.Root.Name}' element must contain a '{xEngineAssemblyQualifiedName}' or '{xEngine}' attribute or element." );
                    else Throw.ArgumentException( $"Independent Configuration Mode: '{xCKSetup}' element must contain a '{xEngineAssemblyQualifiedName}' or '{xEngine}' attribute or element." );
                }
            }
            // Normalizes it: to be more expressive, in shared mode, Engine attribute is used and in independent mode it is the EngineAssemblyQualifiedName element.
            _ckSetup.Elements().Where( e => e.Name == xEngineAssemblyQualifiedName || e.Name == xEngine ).Remove();
            _ckSetup.Attributes().Where( e => e.Name == xEngineAssemblyQualifiedName || e.Name == xEngine ).Remove();
            EngineAssemblyQualifiedName = setEngineName;

            _preferredTargetRuntimes = new PreferredTargetRuntimesList( this );
            _dependenciesList = new DependenciesList( this );
            _binPathsList = new BinPathList( this );
            var sForce = (string?)_ckSetup.Element( xForceSetup );
            if( sForce != null ) _forceSetup = Enum.Parse<ForceSetupLevel>( sForce );
        }

        /// <summary>
        /// Helper to load a configuration file.
        /// If no <see cref="xBasePath"/> element exists in the file, the absolute <paramref name="path"/>'s directory is set
        /// as the base path.
        /// <para>
        /// If <see cref="CKSetupName"/> is not specified, the file name without the extension (or the name of the parent directory if
        /// the file name is "CKSetup") is used.
        /// </para>
        /// </summary>
        /// <param name="path">The path of the xml file.</param>
        /// <returns>A configuration object.</returns>
        public static SetupConfiguration Load( string path )
        {
            path = System.IO.Path.GetFullPath( path );
            var c = new SetupConfiguration( XDocument.Load( path ) );
            if( c.BasePath.IsEmptyPath ) c.BasePath = System.IO.Path.GetDirectoryName( path );
            if( c.CKSetupName == null )
            {
                var f = c.CKSetupName = System.IO.Path.GetFileNameWithoutExtension( path );
                if( f.Equals( "CKSetup", StringComparison.OrdinalIgnoreCase )
                    || f.Equals( "Setup", StringComparison.OrdinalIgnoreCase ) )
                {
                    f = System.IO.Path.GetFileName( System.IO.Path.GetDirectoryName( path ) );
                }
                c.CKSetupName = f;
            }
            return c;
        }

        /// <summary>
        /// Creates a xml document from this configuration.
        /// Note that the returned elements are independent clones of the managed elements.
        /// </summary>
        /// <param name="forSignature">
        /// True to skip <see cref="ForceSetup"/> that should not be used when computing signatures :).
        /// Note that the LogLevel participates in the signature.
        /// </param>
        /// <returns>The Xml document.</returns>
        public XDocument ToXml( bool forSignature = false )
        {
            var clone = forSignature
                            ? new XElement( _ckSetup.Name,
                                            _ckSetup.Attributes(),
                                            _ckSetup.Nodes().Where( n => !(n is XElement e) || e.Name != xForceSetup ) )
                            : new XElement( _ckSetup );
            var root = SharedConfigurationMode
                        ? clone
                        : new XElement( xDocumentRootName, clone, new XElement( _configuration ) );
            return new XDocument( root );
        }

        /// <summary>
        /// Clones this configuration as an independent object.
        /// </summary>
        /// <returns>A clone of this configuration.</returns>
        public SetupConfiguration Clone()
        {
            return new SetupConfiguration( ToXml() );
        }

        /// <summary>
        /// Gets whether Shared configuration is used.
        /// This is true by default.
        /// </summary>
        public bool SharedConfigurationMode => _ckSetup == _configuration;

        /// <summary>
        /// Assembly qualified name of the engine type. Never null (defaults to <see cref="String.Empty"/>).
        /// In xml this can appear as an attribute or an element that can be "EngineAssemblyQualifiedName"
        /// or simply "Engine" on the CKSetup element.
        /// <para>
        /// An engine can be any object with a public constructor with two parameters
        /// (a IActivityMonitor and a <see cref="XElement"/>) and a public <c>bool Run()</c> method.
        /// <para>
        /// Example: "CK.Setup.StObjEngine, CK.StObj.Engine"
        /// </para>
        /// </para>
        /// </summary>
        public string EngineAssemblyQualifiedName
        {
            get => SharedConfigurationMode
                    ? (string?)_ckSetup.Attribute( xEngine ) ?? String.Empty
                    : (string?)_ckSetup.Element( xEngineAssemblyQualifiedName ) ?? String.Empty;
            set
            {
                if( SharedConfigurationMode ) _ckSetup.SetAttributeValue( xEngine, value ?? String.Empty );
                else _ckSetup.SetElementValue( xEngineAssemblyQualifiedName, value ?? String.Empty );
            }
        }

        /// <summary>
        /// Gets or sets a name for this setup.
        /// Defaults to null.
        /// When not null, this is used as a sub folder in the <see cref="WorkingDirectory"/>
        /// so that Setup working directory are grouped by name.
        /// <para>
        /// See <see cref="Load(string)"/> for its default initialization when configuration is read from a file.
        /// </para>
        /// </summary>
        public string? CKSetupName
        {
            get => (string?)_ckSetup.Element( xCKSetupName );
            set => _ckSetup.SetElementValue( xCKSetupName, value );
        }

        /// <summary>
        /// Gets or sets a base path that applies to relative <see cref="BinPaths"/> and may be used
        /// at the runner level.
        /// When <see cref="NormalizedPath.IsEmptyPath"/>, the current directory is used.
        /// When <see cref="Load"/> is used and the file does not contain <see cref="xBasePath"/>, this
        /// base path is the file's directory.
        /// </summary>
        public NormalizedPath BasePath
        {
            get => (string?)_ckSetup.Element( xBasePath );
            set => _ckSetup.SetElementValue( xBasePath, value.IsEmptyPath ? null : value );
        }

        /// <summary>
        /// Gets or sets the working directory.
        /// When <see cref="NormalizedPath.IsEmptyPath"/>, a temporary folder is used.
        /// </summary>
        public NormalizedPath WorkingDirectory
        {
            get => (string?)_ckSetup.Element( xWorkingDirectory );
            set => _ckSetup.SetElementValue( xWorkingDirectory, value.Path );
        }

        /// <summary>
        /// Gets or sets the maximum count of working directories that are kept.
        /// This is expressed as the KeepOnlyCount attribute of the &lt;WorkingDirectory &gt; element and defaults
        /// to <see cref="DefaultWorkingDirectoryKeepOnlyCount"/> (that is 5).
        /// <para>
        /// When specified, it cannot exceed <see cref="MaxWorkingDirectoryKeepOnlyCount"/> (that is 50).
        /// </para>
        /// </summary>
        public int WorkingDirectoryKeepOnlyCount
        {
            get => (int?)_ckSetup.Element( xWorkingDirectory )?.Attribute( xKeepOnlyCount ) ?? DefaultWorkingDirectoryKeepOnlyCount;
            set
            {
                var e = _ckSetup.Element( xWorkingDirectory );
                if( value == DefaultWorkingDirectoryKeepOnlyCount ) e?.Attribute( xKeepOnlyCount )?.Remove();
                else
                {
                    if( e == null ) _ckSetup.Add( e = new XElement( xWorkingDirectory ) );
                    e.SetAttributeValue( xKeepOnlyCount, value );
                }
            }
        }

        /// <summary>
        /// Gets or sets whether a 'CKSetup.log' text file in the <see cref="WorkingDirectory"/> must contain the setup logs.
        /// Defaults to true.
        /// </summary>
        public bool WorkingDirectoryHasLogs
        {
            get => (bool?)_ckSetup.Element( xWorkingDirectory )?.Attribute( xHasLogs ) ?? true;
            set
            {
                var e = _ckSetup.Element( xWorkingDirectory );
                if( !value || e != null )
                {
                    if( e == null ) _ckSetup.Add( e = new XElement( xWorkingDirectory ) );
                    e.SetAttributeValue( xHasLogs, value );
                }
            }
        }

        /// <summary>
        /// Gets or sets an optional semicolon separated list of <see cref="StaticGate.DisplayName"/> that will
        /// be opened (or closed by using "!Name") locally (by the <see cref="Facade"/>) before a setup and
        /// transmitted and also applied in the runner context.
        /// <para>
        /// The facade will restore the previous gates open/close status once done. Since gates are
        /// static, if parallel setups are run from the same process (that should never happen), the eventual
        /// StaticGates' status will be indeterminate.
        /// </para>
        /// </summary>
        public string? StaticGates
        {
            get => _ckSetup.Element( xStaticGates )?.Value ?? String.Empty;
            set => _ckSetup.SetElementValue( xStaticGates, value );
        }

        /// <summary>
        /// Gets or sets whether setup should be done regardless of the SHA1 of the <see cref="BinPaths"/> Model and
        /// ModelDependent files and the properties of this SetupConfiguration.
        /// Defaults to false.
        /// <para>
        /// Signature files are always generated in BinPaths and in the working folder at the end
        /// of a successful setup. 
        /// </para>
        /// </summary>
        public ForceSetupLevel ForceSetup
        {
            get => _forceSetup;
            set
            {
                if( _forceSetup != value )
                {
                    _forceSetup = value;
                    _ckSetup.SetElementValue( xForceSetup, value != ForceSetupLevel.None ? value.ToString() : null );
                }
            }
        }

        /// <summary>
        /// Gets or sets a string that can be anything: it participates to
        /// signature computation and enable callers to sign a setup with any external information
        /// by concatenating any string to it.
        /// Defaults to the empty string.
        /// </summary>
        public string ExternalSignature
        {
            get => (string?)_ckSetup.Element( xExternalSignature ) ?? String.Empty;
            set => _ckSetup.SetElementValue( xExternalSignature, String.IsNullOrEmpty( value ) ? null : value );
        }

        /// <summary>
        /// Gets or sets a LogLevel that will be used by the runner.
        /// Defaults to <see cref="LogFilter.Undefined"/>: the current <see cref="IActivityMonitor.ActualFilter"/> of the
        /// <see cref="Facade.DoRun(IActivityMonitor, LocalStore, SetupConfiguration, bool, ForceSetupLevel, Uri?, string?)"/> monitor
        /// parameter will be used.
        /// <para>
        /// This LogLevel's type is actually a <see cref="LogFilter"/> but we are always talking about "log levels", aren't we?
        /// </para>
        /// </summary>
        public LogFilter LogLevel
        {
            get
            {
                LogFilter.TryParse( (string?)_ckSetup.Element( xLogLevel ) ?? String.Empty, out var l );
                return l;
            }
            set => _ckSetup.SetElementValue( xLogLevel, value != LogFilter.Undefined ? value.ToString() : null );
        }

        /// <summary>
        /// Optional ordered list of <see cref="TargetRuntime"/> that will be selected if possible. First is better.
        /// In xml, it is a semicolon separated string of target runtimes:
        /// Net461;NetCoreApp20;NetCoreApp21.
        /// </summary>
        public IList<TargetRuntime> PreferredTargetRuntimes => _preferredTargetRuntimes;

        abstract class SyncList<T> : IList<T>
        {
            protected readonly SetupConfiguration Config;
            protected readonly XElement CKSetup;
            protected readonly List<T> List;

            public SyncList( SetupConfiguration c )
            {
                Config = c;
                CKSetup = Config._ckSetup;
                List = new List<T>();
            }

            void Synchronize()
            {
                DoSynchronize();
            }

            protected abstract void DoSynchronize();

            public virtual T this[int index] { get => List[index]; set { List[index] = value; Synchronize(); } }

            public int Count => List.Count;

            public bool IsReadOnly => false;

            public void Add( T item )
            {
                List.Add( item );
                Synchronize();
            }

            public void Clear()
            {
                List.Clear();
                Synchronize();
            }

            public bool Contains( T item ) => List.Contains( item );

            public void CopyTo( T[] array, int arrayIndex ) => List.CopyTo( array, arrayIndex );

            public IEnumerator<T> GetEnumerator() => List.GetEnumerator();

            public int IndexOf( T item ) => List.IndexOf( item );

            public void Insert( int index, T item )
            {
                List.Insert( index, item );
                Synchronize();
            }

            public bool Remove( T item )
            {
                bool r = List.Remove( item );
                if( r ) Synchronize();
                return r;
            }

            public void RemoveAt( int index )
            {
                List.RemoveAt( index );
                Synchronize();
            }

            IEnumerator IEnumerable.GetEnumerator() => List.GetEnumerator();
        }

        sealed class PreferredTargetRuntimesList : SyncList<TargetRuntime>
        {
            public PreferredTargetRuntimesList( SetupConfiguration c )
                : base( c )
            {
                List.AddRange( ((string?)CKSetup.Element( xPreferredTargetRuntimes ) ?? String.Empty)
                                        .Split( ';' )
                                        .Select( x => x.Trim() )
                                        .Where( x => x.Length > 0 )
                                        .Select( x => (TargetRuntime)Enum.Parse( typeof( TargetRuntime ), x, ignoreCase: true ) ) );
            }

            protected override void DoSynchronize()
            {
                CKSetup.SetElementValue( xPreferredTargetRuntimes, List.Count > 0 ? String.Join( ";", List.Select( x => x.ToString() ) ) : null );
            }
        }

        /// <summary>
        /// Gets a list of setup dependencies.
        /// </summary>
        public IList<SetupDependency> Dependencies => _dependenciesList;

        sealed class DependenciesList : SyncList<SetupDependency>
        {
            readonly XElement _dependencies;

            public DependenciesList( SetupConfiguration c )
                : base( c )
            {
                var deps = CKSetup.Element( xDependencies );
                if( deps == null )
                {
                    CKSetup.Add( _dependencies = new XElement( xDependencies ) );
                }
                else
                {
                    List.AddRange( (_dependencies = deps)
                                        .Elements( SetupDependency.xDependency )
                                        .Select( e => new SetupDependency( e ) ) );
                }
            }

            protected override void DoSynchronize()
            {
                _dependencies.Elements( SetupDependency.xDependency ).Remove();
                _dependencies.Add( List.Select( p => p.ToXml() ) );
            }
        }

        /// <summary>
        /// Gets a collection of paths to setup. It must not be empty, and all paths must be valid (when relative the <see cref="BasePath"/>
        /// is used to combine them).
        /// In <see cref="SharedConfigurationMode"/>, it is better to directly interact with the Xml &lt;BinPath Path="..." /&gt;
        /// elements since they may carry configurations specific to each paths, however at this level, we do not expose the underlying
        /// Xml elements.
        /// </summary>
        public ICollection<NormalizedPath> BinPaths => _binPathsList;

        /// <summary>
        /// Offers a way to apply a function to all <see cref="BinPaths"/> while hiding (preserving) underlying Xml
        /// elements.
        /// </summary>
        /// <param name="pathProcess">
        /// Required function that accepts the item index, the original path and the underlying xml element.
        /// When this function returns null, the corresponding path/element is removed, and when this happens, the index is not incremented.
        /// </param>
        public void ProcessBinPaths( Func<int, NormalizedPath, XElement, NormalizedPath?> pathProcess ) => _binPathsList.Apply( pathProcess );

        internal IEnumerable<XElement> BinPathElements => _binPathsList.BinPathElements;

        /// <summary>
        /// Gets the final bin path that must be used: it is Path that has been normalized and
        /// may be adjusted to /publish folder. The BinPath attribute carries it.
        /// This is available only after SetupContext.Create has been called.
        /// </summary>
        internal IEnumerable<string> FinalBinPaths => _binPathsList.BinPathElements.Select( e => (string)e.AttributeRequired( xBinPath ) );

        sealed class BinPathList : SyncList<NormalizedPath>
        {
            readonly XElement _binPaths;

            public BinPathList( SetupConfiguration c )
                : base( c )
            {
                var p  = CKSetup.Element( xBinPaths );
                if( p == null )
                {
                    CKSetup.Add( _binPaths = new XElement( xBinPaths ) );
                }
                else
                {
                    _binPaths = p;
                    // Lift BinPath element value to Path attribute (and clean text content).
                    var oldBinPaths = CKSetup.Elements( xBinPaths ).Elements( xBinPath ).Where( e => e.Attribute( xPath ) == null );
                    foreach( var o in oldBinPaths )
                    {
                        o.SetAttributeValue( xPath, o.Value );
                        o.RemoveNodes();
                    }
                    // Filled the exposed List<NormalizedPath>.
                    List.AddRange( _binPaths.Elements( xBinPath ).Select( e => new NormalizedPath( (string?)e.Attribute( xPath ) ) ) );
                }
            }

            protected override void DoSynchronize()
            {
                // Removes all elements for which Path attribute does not appear in the exposed List<NormalizedPath>.
                _binPaths.Elements().Where( e => !List.Contains( (string?)e.Attribute( xPath ) ) ).Remove();
                // Creates new XElements for new paths.
                var toAdd = List.Where( p => !_binPaths.Elements( xBinPath ).Any( e => (string?)e.Attribute( xPath ) == p.Path ) ).ToList();
                _binPaths.Add( toAdd.Select( p => new XElement( xBinPath, new XAttribute( xPath, p ) ) ) );
            }

            public void Apply( Func<int, NormalizedPath, XElement, NormalizedPath?> pathProcess )
            {
                HashSet<XElement>? toRemove = null;
                int idx = 0;
                foreach( var b in _binPaths.Elements( xBinPath ) )
                {
                    // Silently ignores any BinPath elements where Path attribute is missing.
                    string? initial = b.Attribute( xPath )?.Value;
                    if( initial == null ) continue;
                    var p = pathProcess( idx, initial, b );
                    if( !p.HasValue )
                    {
                        if( toRemove == null ) toRemove = new HashSet<XElement>();
                        toRemove.Add( b );
                        List.RemoveAt( idx );
                    }
                    else
                    {
                        List[idx++] = p.Value;
                        b.SetAttributeValue( xPath, p.Value.Path );
                    }
                }
                toRemove?.Remove();
            }

            public IEnumerable<XElement> BinPathElements => CKSetup.Elements( xBinPaths ).Elements( xBinPath );
        }

        /// <summary>
        /// Gets the configuration that will be provided to the Engine. Its <see cref="XElement.Name"/> can be
        /// changed (it defaults to "Configuration").
        /// It can be set only if <see cref="SharedConfigurationMode"/> is false and in such case, must not be null.
        /// </summary>
        public XElement Configuration
        {
            get => _configuration;
            set
            {
                Throw.CheckState( !SharedConfigurationMode );
                Throw.CheckNotNullArgument( value );
                _configuration = value;
            }
        }
    }
}
;
