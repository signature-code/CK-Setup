using CK.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// Captures informations related to the setup initialization.
    /// This class is currently internal but may be published if needed.
    /// </summary>
    class SetupContext
    {
        /// <summary>
        /// The signature file name that should contain the <see cref="RunSignature"/>.
        /// </summary>
        public static readonly string SignatureFileName = "CKSetup.signature.txt";

        SetupContext( bool success,
                      SetupConfiguration config,
                      IReadOnlyList<BinFolder>? folders,
                      IReadOnlyDictionary<string, BinFileInfo>? files,
                      SHA1Value currentSignature,
                      NormalizedPath workingDir,
                      bool workingDirectoryIsReady,
                      NormalizedPath basePath )
        {
            Success = success;
            Configuration = config;
            BinFolders = folders;
            DedupFiles = files;
            RunSignature = currentSignature;
            FinalWorkingDirectory = workingDir;
            WorkingDirectoryIsReady = workingDirectoryIsReady;
            BasePath = basePath;
        }

        // Error constructor.
        SetupContext( SetupConfiguration config )
            : this( false, config, null, null, SHA1Value.Zero, null, true, null )
        {
        }

        /// <summary>
        /// Gets the configuration.
        /// This is a mutable object but it should no be modified once a <see cref="SetupContext"/> is built.
        /// </summary>
        public SetupConfiguration Configuration { get; }

        /// <summary>
        /// Gets the configuration base path.
        /// When <see cref="SetupConfiguration.BasePath"/> is empty, the current directory is used.
        /// </summary>
        public NormalizedPath BasePath { get; }

        /// <summary>
        /// Gets whether this context has been successfully created.
        /// </summary>
        public bool Success { get; }

        /// <summary>
        /// Gets whether signatures files have been checked and system is up to date:
        /// the <see cref="FinalWorkingDirectory"/> is null since no setup should be done.
        /// </summary>
        public bool UpToDate => Success && FinalWorkingDirectory == null;

        /// <summary>
        /// Gets the current signature based on the <see cref="Configuration"/> and the content of
        /// the <see cref="SetupConfiguration.BinPaths"/>.
        /// It is <see cref="SHA1Value.IsZero"/> if <see cref="Success"/> is false.
        /// </summary>
        public SHA1Value RunSignature { get; }

        /// <summary>
        /// Gets the <see cref="SetupConfiguration.BinPaths"/> as <see cref="BinFolder"/>.
        /// Null if this context is invalid.
        /// </summary>
        public IReadOnlyList<BinFolder>? BinFolders { get; }

        /// <summary>
        /// Gets all the files indexed by their local file names in the different bin paths.
        /// Null if this context is invalid.
        /// </summary>
        public IReadOnlyDictionary<string, BinFileInfo>? DedupFiles { get; }

        /// <summary>
        /// Gets the working directory that must be used.
        /// It is either a sub directory of <see cref="Path.GetTempPath()"/> or the
        /// explicit <see cref="SetupConfiguration.WorkingDirectory"/>.
        /// This is null if <see cref="Success"/> is false or <see cref="UpToDate"/> is true.
        /// </summary>
        public NormalizedPath FinalWorkingDirectory { get; }

        /// <summary>
        /// Gets whether the <see cref="FinalWorkingDirectory"/> contains the <see cref="SignatureFileName"/> with
        /// the current <see cref="RunSignature"/>.
        /// </summary>
        public bool WorkingDirectoryIsReady { get; }

        /// <summary>
        /// Creates a <see cref="SetupContext"/> from a <see cref="SetupConfiguration"/>.
        /// The configuration can be altered by this initialization.
        /// </summary>
        /// <param name="monitor">The monitor to use. Must not be null.</param>
        /// <param name="config">The configuration. Must not be null.</param>
        /// <param name="forceSetup">Whether setup must be done regardless of the SHA1 signatures that may skip it.</param>
        /// <param name="fileClient">Optional <see cref="BufferedFileMonitorClient"/> for which a log file in WorkingDirectory logs should be created.</param>
        /// <returns>A setup context (potentially invalid, see <see cref="Success"/>).</returns>
        public static SetupContext Create( IActivityMonitor monitor, SetupConfiguration config, ForceSetupLevel forceSetup, BufferedFileMonitorClient? fileClient = null )
        {
            Throw.CheckNotNullArgument( monitor );
            Throw.CheckNotNullArgument( config );

            if( config.ForceSetup > forceSetup )
            {
                forceSetup = config.ForceSetup;
            }
            using( monitor.OpenInfo( $"Initializing SetupContext{(forceSetup == ForceSetupLevel.None ? String.Empty : $" (ForceSetup = '{forceSetup}')")}." ) )
            {
                monitor.Info( config.ToXml().ToString() );
                try
                {
                    NormalizedPath basePath = config.BasePath;
                    if( basePath.IsEmptyPath )
                    {
                        basePath = Environment.CurrentDirectory;
                        monitor.Info( $"Configuration BasePath is empty: using current directory '{basePath}' as a base path to resolve relative paths." );
                    }

                    using( monitor.OpenTrace( "Normalizing <BinPaths Path=\"...\" /> attributes and ensuring that the projects are published in their 'BinPath' path." ) )
                    {
                        Debug.Assert( config.BinPathElements.Count() == config.BinPaths.Count );
                        // This is a pure function, but to avoid a 3rd path (Path => SelectedPath => BinPath),
                        // we mutate the BinPath.Path below.
                        NormalizedPath[]? selectedPaths = HandleAlternateBinPaths( monitor, config, basePath );
                        if( selectedPaths == null ) return new SetupContext( config );

                        // Use the encapsulated list apply so that the IReadOnlyCollection<NormalizedPath> BinPaths is
                        // still synchronized. Stops as soon as a publish fails.
                        bool success = true;
                        config.ProcessBinPaths( ( idx, p, e ) =>
                        {
                            var resolved = selectedPaths[idx];
                            if( success )
                            {
                                var r = Facade.DoEnsurePublished( monitor, resolved, false );
                                if( success &= r.Success ) e.SetAttributeValue( SetupConfiguration.xBinPath, r.FinalPath );
                            }
                            return resolved;
                        } );
                        if( !success ) return new SetupContext( config );
                    }
                    IReadOnlyDictionary<string, BinFileInfo>? dedupFiles;
                    IReadOnlyList<BinFolder>? folders = ReadBinFolders( monitor, config.FinalBinPaths, out dedupFiles );
                    if( folders != null )
                    {
                        Debug.Assert( dedupFiles != null, "Since folders have been successfully read." );
                        SHA1Value runSignature = ComputeRunSignature( config, dedupFiles );

                        bool binFoldersSignatureMatch = CheckSignatureFilesFromBinFolders( monitor, folders, runSignature );

                        if( forceSetup == ForceSetupLevel.None && binFoldersSignatureMatch )
                        {
                            monitor.Warn( "Signature match! System is up to date and ForceSetup is 'None'. No WorkingDirectory is created and the Engine is not run." );
                            return new SetupContext( true, config, folders, dedupFiles, runSignature, null, true, basePath );
                        }
                        if( !binFoldersSignatureMatch )
                        {
                            monitor.Info( "No signature match in BinPaths: running a full setup." );
                            forceSetup = ForceSetupLevel.Full;
                        }
                        // If we are forcing a full setup, we send SHA1Value.Zero, otherwise (None or Engine) we
                        // try to find an existing WorkingDirectory with the signature.
                        var (workingDir,isReady) = GetWorkingDirectory( monitor,
                                                                        config.WorkingDirectory,
                                                                        config.CKSetupName,
                                                                        folders,
                                                                        basePath,
                                                                        forceSetup == ForceSetupLevel.Full ? SHA1Value.Zero : runSignature,
                                                                        config.WorkingDirectoryKeepOnlyCount );
                        if( !workingDir.IsEmptyPath )
                        {
                            if( fileClient != null )
                            {
                                fileClient.SetFile( monitor, workingDir.AppendPart( "CKSetup.log" ) );
                            }
                            return new SetupContext( true, config, folders, dedupFiles, runSignature, workingDir, isReady, basePath );
                        }
                    }
                }
                catch( Exception ex )
                {
                    monitor.Fatal( ex );
                }
                monitor.CloseGroup( "Invalid SetupContext." );
                return new SetupContext( config );
            }
        }

        /// <summary>
        /// Resolves [sl|ots] in paths (the first is driving).
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="config">The configuration.</param>
        /// <param name="basePath">The base path.</param>
        /// <returns>The path with slots resolved.</returns>
        static NormalizedPath[]? HandleAlternateBinPaths( IActivityMonitor monitor, SetupConfiguration config, NormalizedPath basePath )
        {
            var selectedPaths = new NormalizedPath[config.BinPaths.Count];
            // Computes the AlternativePath and fills the selectedPaths array with the (potentially alternate) paths.
            int maxAlternativeCount = 1;
            AlternativePath[] altPaths = config.BinPathElements.Select( ( path, idx ) =>
            {
                var p = (string?)path.Attribute( SetupConfiguration.xPath ) ?? String.Empty;
                var ap = new AlternativePath( p, Path.GetFullPath( Path.IsPathRooted( p ) ? p : Path.Combine( basePath, p ) ) );
                selectedPaths[idx] = ap.Path;
                if( ap.Count > maxAlternativeCount ) maxAlternativeCount = ap.Count;
                return ap;
            } ).ToArray();

            if( maxAlternativeCount > 1 )
            {
                using( monitor.OpenInfo( $"Handling {maxAlternativeCount} possibilities for {selectedPaths.Length} paths." ) )
                {
                    var main = altPaths[0];
                    var alien = altPaths.Skip( 1 ).FirstOrDefault( p => !main.CanCover( in p ) );
                    if( alien.IsValid )
                    {
                        monitor.Error( $"First path '{main.Path}' has alternatives, it drives the alternate pieces that will be used in other paths. The path '{alien}' must not define alternatives that are NOT defined in this first path." );
                        return null;
                    }
                    using( monitor.OpenTrace( $"Testing {main.Count} alternate paths in {main.Path}." ) )
                    {
                        int bestIdx = -1;
                        NormalizedPath best = new NormalizedPath();
                        DateTime bestDate = Util.UtcMinValue;
                        for( int i = 0; i < main.Count; ++i )
                        {
                            NormalizedPath path = main[i];
                            var noPub = path.LastPart == "publish" ? path.RemoveLastPart() : path;
                            if( !Directory.Exists( noPub ) )
                            {
                                monitor.Debug( $"Alternate path '{noPub}' not found." );
                                continue;
                            }
                            var files = Directory.EnumerateFiles( noPub );
                            if( files.Any() )
                            {
                                var mostRecent = Directory.EnumerateFiles( noPub ).Max( p => File.GetLastWriteTimeUtc( p ) );
                                if( bestDate < mostRecent )
                                {
                                    bestDate = mostRecent;
                                    best = path;
                                    bestIdx = i;
                                }
                            }
                            else monitor.Debug( $"Alternate path '{noPub}' is empty." );
                        }
                        if( bestIdx < 0 )
                        {
                            monitor.Error( $"Unable to find any file in any of the {main.Count} paths in {main.Path}." );
                            return null;
                        }
                        monitor.Info( $"Selected path is n°{bestIdx}: {best} since it has the most recent file change ({bestDate})." );
                        selectedPaths[0] = best;
                        for( var iFinal = 1; iFinal < altPaths.Length; ++iFinal )
                        {
                            var aP = altPaths[iFinal];
                            NormalizedPath cap = main.Cover( bestIdx, aP );
                            if( aP.OrginPath != cap.Path )
                            {
                                monitor.Trace( $"Path '{altPaths[iFinal].OrginPath}' resolved to '{cap}'." );
                                selectedPaths[iFinal] = cap;
                            }
                        }
                    }
                }
            }
            else monitor.Trace( $"No alternative found among the {selectedPaths.Length} paths." );
            return selectedPaths;
        }

        static bool CheckSignatureFilesFromBinFolders( IActivityMonitor monitor, IReadOnlyList<BinFolder> folders, SHA1Value runSignature )
        {
            string? current = null;
            foreach( var f in folders )
            {
                string fSign = Path.Combine( f.BinPath, SignatureFileName );
                if( !File.Exists( fSign ) )
                {
                    monitor.Info( $"Missing signature file '{fSign}'." );
                    return false;
                }
                var content = File.ReadAllText( fSign );
                if( current == null )
                {
                    if( content != runSignature.ToString() )
                    {
                        monitor.Info( $"Found signature '{content}' in '{fSign}' that is not the current run signature." );
                        return false;
                    }
                    current = content;
                }
                else if( current != content )
                {
                    monitor.Info( $"Signature mismatch between folders: {fSign} is not the same as the one in {folders[0].BinPath}." );
                    return false;
                }
            }
            return true;
        }

        static SHA1Value ComputeRunSignature( SetupConfiguration config, IReadOnlyDictionary<string, BinFileInfo> dedupFiles )
        {
            using( var c = new HashStream( HashAlgorithmName.SHA1 ) )
            using( BinaryWriter cW = new BinaryWriter( c ) )
            {
                cW.Write( config.ToXml( forSignature: true ).ToString( SaveOptions.DisableFormatting ) );
                // Sort them to be hash code (non)ordering independent.
                var content = dedupFiles.Values.Select( f => f.ContentSHA1 ).ToArray();
                Array.Sort( content );
                foreach( var s in content ) s.Write( cW );
                return new SHA1Value( c.GetFinalResult().AsSpan() );
            }
        }

        static IReadOnlyList<BinFolder>? ReadBinFolders( IActivityMonitor monitor,
                                                         IEnumerable<string> binPaths,
                                                         out IReadOnlyDictionary<string, BinFileInfo>? dedupFiles )
        {
            dedupFiles = null;
            using( monitor.OpenInfo( "Reading binary folders." ) )
            {
                var folders = binPaths.Select( p => BinFolder.ReadBinFolder( monitor, p ) ).ToList();
                if( folders.Any( f => f == null ) ) return null;
                if( folders.Count == 0 )
                {
                    monitor.Error( $"No BinPath provided. At least one is required." );
                    return null;
                }
                using( folders.Count > 1 ? monitor.OpenInfo( "Multiple Bin paths: Checking that common files are the same." ) : null )
                {
                    bool conflict = false;
                    Dictionary<string, BinFileInfo> byName = new Dictionary<string, BinFileInfo>();
                    foreach( var folder in folders )
                    {
                        Debug.Assert( folder != null );
                        foreach( var file in folder.Files )
                        {
                            if( byName.TryGetValue( file.LocalFileName, out var exists ) )
                            {
                                if( exists.ContentSHA1 != file.ContentSHA1 )
                                {
                                    conflict = true;
                                    using( monitor.OpenError( $"File mismatch: {file.LocalFileName}" ) )
                                    {
                                        monitor.Info( $"{exists.BinFolder.BinPath} has '{exists}' with SHA1: {exists.ContentSHA1}." );
                                        monitor.Info( $"{file.BinFolder.BinPath} has '{file}' with SHA1: {file.ContentSHA1}." );
                                    }
                                }
                            }
                            else byName.Add( file.LocalFileName, file );
                        }
                    }
                    if( conflict ) return null;
                    if( folders.Count > 1 ) monitor.CloseGroup( "Files are the same." );
                    dedupFiles = byName;
                }
                monitor.CloseGroup( $"{folders.Count} folders read." );
                return (IReadOnlyList<BinFolder>)folders;
            }
        }

        static (NormalizedPath WorkingDir, bool IsReady) GetWorkingDirectory( IActivityMonitor monitor,
                                                                              NormalizedPath workingDir,
                                                                              string? ckSetupName,
                                                                              IReadOnlyList<BinFolder> folders,
                                                                              NormalizedPath basePath,
                                                                              SHA1Value runSignature,
                                                                              int keepOnlyCount )
        {
            Debug.Assert( !basePath.IsEmptyPath );

            if( String.IsNullOrWhiteSpace( ckSetupName ) ) ckSetupName = null;

            string baseDir;

            bool mustCreateDir = false;
            bool isInTempPath;
            NormalizedPath wDir;
            if( !workingDir.IsEmptyPath )
            {
                workingDir = basePath.Combine( workingDir ).ResolveDots();
                if( ckSetupName != null ) workingDir = workingDir.AppendPart( ckSetupName );

                wDir = Path.GetFullPath( workingDir );
                if( folders.Any( b => b.BinPath.StartsWith( wDir ) ) )
                {
                    monitor.Fatal( $"Working directory '{wDir}' cannot be inside one of the bin paths: '{folders.First(b => b.BinPath.StartsWith( wDir )).BinPath}'." );
                    return (default,false);
                }
                baseDir = wDir;
                if( mustCreateDir = !Directory.Exists( baseDir ) )
                {
                    monitor.Info( $"Creating provided Working Directory: {baseDir}." );
                }
                isInTempPath = false;
            }
            else
            {
                baseDir = Path.GetTempPath() + "CKSetup";
                if( ckSetupName != null ) baseDir += Path.DirectorySeparatorChar + ckSetupName;
                if( mustCreateDir = !Directory.Exists( baseDir ) )
                {
                    monitor.Info( $"Creating CKSetup temporary folder: {baseDir}." );
                }
                isInTempPath = true;
            }
            baseDir += Path.DirectorySeparatorChar;
            if( mustCreateDir )
            {
                Directory.CreateDirectory( baseDir );
            }
            else
            {
                wDir = TryFindPreviousFolderOrCleanup( monitor, baseDir, keepOnlyCount, runSignature );
                if( wDir != null )
                {
                    monitor.Info( $"Reusing Working Directory '{wDir}': found file {SignatureFileName} that matches {runSignature}." );
                    return (wDir, true);
                }
            }
            wDir = FileUtil.CreateUniqueTimedFolder( baseDir, null, DateTime.UtcNow );
            monitor.Info( $"Created Working Directory: {wDir}." );
            if( isInTempPath )
            {
                var gitIgnore = wDir.AppendPart( ".gitignore" );
                if( !File.Exists( gitIgnore ) )
                {
                    monitor.Trace( $"Created '.gitignore' file with \"*\"." );
                    File.WriteAllText( gitIgnore, "*" );
                }
            }
            return (wDir,false);
        }

        static string? TryFindPreviousFolderOrCleanup( IActivityMonitor monitor, string baseDir, int keepOnlyCount, SHA1Value runSignature )
        {
            Debug.Assert( Directory.Exists( baseDir ) );

            // EnumerateDirectories usually enumerates in order... but it is safer to sort.
            bool isSorted = true;
            var all = new List<KeyValuePair<string, DateTime>>();
            var prevTime = Util.UtcMinValue;
            foreach( var dir in Directory.EnumerateDirectories( baseDir ) )
            {
                var fileName = Path.GetFileName( dir );
                if( !FileUtil.TryParseFileNameUniqueTimeUtcFormat( fileName, out var time ) )
                {
                    monitor.Warn( $"Found folder that is not a \"timed folder\": '{fileName}'. It is ignored." );
                    continue;
                }
                all.Add( new KeyValuePair<string, DateTime>( dir, time ) );
                if( isSorted &= time >= prevTime ) prevTime = time;
                // Check the signature file asap.
                if( !runSignature.IsZero )
                {
                    var sigFile = Path.Combine( dir, SignatureFileName );
                    if( File.Exists( sigFile ) && File.ReadAllText( sigFile ) == runSignature.ToString() )
                    {
                        // Found a ready WorkingFolder. Logging is done by the caller.
                        return dir;
                    }
                }
            }

            if( keepOnlyCount < 0 || keepOnlyCount > SetupConfiguration.MaxWorkingDirectoryKeepOnlyCount )
            {
                monitor.Warn( $"KeepOnlyCount for WorkingDirectory is out of range ({keepOnlyCount}), using MaxWorkingDirectoryKeepOnlyCount = {SetupConfiguration.MaxWorkingDirectoryKeepOnlyCount}." );
                keepOnlyCount = SetupConfiguration.MaxWorkingDirectoryKeepOnlyCount;
            }
            using( monitor.OpenTrace( $"Cleaning WorkingDirectory with KeepOnlyCount = {keepOnlyCount}." ) )
            {
                int extra = all.Count - keepOnlyCount;
                if( extra == 0 ) monitor.Trace( $"Found {all.Count} timed folders (all are kept)." );
                else monitor.Info( $"Found {all.Count} timed folders, deleting the {extra} older." );
                if( extra >= 0 )
                {
                    if( !isSorted ) all.Sort( ( dt1, dt2 ) => dt1.Value.CompareTo( dt2.Value ) );
                    foreach( var dt in all.Take( extra ) )
                    {
                        Facade.SafeDeleteFolder( monitor, dt.Key );
                    }
                }
            }
            return null;
        }
    }
}
