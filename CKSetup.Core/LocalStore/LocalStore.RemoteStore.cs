using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK.Core;
using System.Xml.Linq;
using CSemVer;
using System.Diagnostics;
using System.IO;
using CKSetup.StreamStore;

namespace CKSetup
{
    public partial class LocalStore
    {
        RemoteAcessor? _remote;

        /// <summary>
        /// Exposes a <see cref="LocalStore"/> as a <see cref="IComponentImporter"/> and/or <see cref="IComponentPushTarget"/>.
        /// </summary>
        public class RemoteAcessor : IComponentImporter, IComponentPushTarget
        {
            readonly LocalStore _remote;
            readonly IStreamStore _privateStore;
            readonly CompressionKind _privateStoreStorageKind;

            internal RemoteAcessor( LocalStore remote )
            {
                _remote = remote;
                _privateStore = remote._store;
                _privateStoreStorageKind = remote._storageKind;
            }

            /// <summary>
            /// Gets the full path of the underlying store as a uri.
            /// </summary>
            public Uri Url => _remote._store.Url;

            /// <summary>
            /// Implements <see cref="IComponentImporter.OpenImportStream(IActivityMonitor, ComponentMissingDescription)"/>.
            /// </summary>
            /// <param name="monitor">Monitor to use.</param>
            /// <param name="missing">Description of what should be obtained.</param>
            /// <returns>A readable stream on success, null on error.</returns>
            public Stream OpenImportStream( IActivityMonitor monitor, ComponentMissingDescription missing )
            {
                var buffer = Util.RecyclableStreamManager.GetStream();
                _remote.ExportComponents( missing, buffer, monitor );
                buffer.Position = 0;
                return buffer;
            }

            /// <summary>
            /// Gets a readable stream of a file content.
            /// </summary>
            /// <param name="monitor">The monitor to use. Can not be null.</param>
            /// <param name="file">The file SHA1.</param>
            /// <param name="preferred">The compression kind of the result.</param>
            /// <returns>An opened readable stream along with its compression kind (or a null <see cref="StoredStream.Stream"/> if it does not exist).</returns>
            public StoredStream GetDownloadStream( IActivityMonitor monitor, SHA1Value file, CompressionKind preferred )
            {
                return _privateStore.OpenRead( file.ToString(), preferred ).ToStoredStream();
            }

            /// <summary>
            /// Pushes a stream of components description and returns
            /// a collection of required files.
            /// </summary>
            /// <param name="monitor">Monitor to use.</param>
            /// <param name="componentsWriter">Component streamer.</param>
            /// <returns>A result object.</returns>
            public PushComponentsResult PushComponents( IActivityMonitor monitor, Action<Stream> componentsWriter )
            {
                using( var m = Util.RecyclableStreamManager.GetStream() )
                {
                    componentsWriter( m );
                    m.Position = 0;
                    return _remote.ImportComponents( m );
                }
            }

            /// <summary>
            /// Pushes a file.
            /// </summary>
            /// <param name="monitor">Monitor to use.</param>
            /// <param name="sessionId">The target's session identifier (<see cref="PushComponentsResult.SessionId"/>).</param>
            /// <param name="sha1">The file SHA1.</param>
            /// <param name="writer">Stream writer.</param>
            /// <param name="kind">Compression kind of the stream.</param>
            /// <returns>Always true.</returns>
            public bool PushFile( IActivityMonitor monitor, string sessionId, SHA1Value sha1, Action<Stream> writer, CompressionKind kind )
            {
                monitor.Debug( $"Pushing {sha1}." );
                _privateStore.Update( sha1.ToString(), writer, kind, true );
                return true;
            }
        }

        /// <summary>
        /// Gets an adapter that enables this <see cref="LocalStore"/> to be used as
        /// a <see cref="IComponentPushTarget"/> and/or a <see cref="IComponentImporter"/>.
        /// </summary>
        public RemoteAcessor Remote => _remote ?? (_remote = new RemoteAcessor(this));
    }
}
