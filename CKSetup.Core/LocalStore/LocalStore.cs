using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK.Core;
using System.Xml.Linq;
using CSemVer;
using System.Diagnostics;
using System.IO;
using CKSetup.StreamStore;
using System.Net.Http;

namespace CKSetup
{
    /// <summary>
    /// Primary object that encapsulates and coordinates a <see cref="IStreamStore"/> and a <see cref="ComponentDB"/>.
    /// It captures a <see cref="IActivityMonitor"/>: it must be used in one activity and be disposed as soon as possible.
    /// </summary>
    public partial class LocalStore : IDisposable
    {
        /// <summary>
        /// The component file.
        /// </summary>
        public const string DbXmlFileName = "db.xml";

        /// <summary>
        /// The store identifier file.
        /// </summary>
        public const string StoreIdentifierFileName = "store-id.txt";

        readonly IStreamStore _store;
        readonly List<string> _cleanupFiles;
        readonly IActivityMonitor _monitor;
        readonly CompressionKind _storageKind;
        readonly string _storeId;

        HttpClient? _httpClient;
        IRemoteStore? _prototypeStore;
        ComponentDB _dbCurrent;
        ComponentDB _dbOrigin;
        DateTime _lastWriteTimeDB;

        /// <summary>
        /// Initializes a new <see cref="LocalStore"/>.
        /// </summary>
        /// <param name="monitor">The monitor that is captured and used by this <see cref="LocalStore"/>.</param>
        /// <param name="store">The stream store.</param>
        /// <param name="storageKind">The compression mode of the store.</param>
        public LocalStore( IActivityMonitor monitor, IStreamStore store, CompressionKind storageKind )
        {
            _store = store;
            _cleanupFiles = new List<string>();
            _monitor = monitor;
            _storageKind = storageKind;
            Tuple<ComponentDB, DateTime>? read = _store.Initialize( monitor );
            if( read == null )
            {
                _dbOrigin = _dbCurrent = ComponentDB.Empty;
                _store.Dispose();
                _storeId = String.Empty;
                _lastWriteTimeDB = Util.UtcMinValue;
            }
            else
            {
                _dbOrigin = _dbCurrent = read.Item1;
                _lastWriteTimeDB = read.Item2;
                var s = _store.ReadText( StoreIdentifierFileName )?.Item1;
                if( s == null )
                {
                    s = Util.GetRandomBase64UrlString( 8 );
                    _store.CreateText( StoreIdentifierFileName, s, CompressionKind.None );
                    monitor.Info( $"Store identifier assigned: '{s}'." );
                }
                _storeId = s;
                // On the default local store path: a Prototype must be set and, by default, it is the DefaultStoreUrl.
                // It can be changed later (typically to a private remote store), but a prototype store MUST exist.
                if( store.Url.LocalPath == Facade.DefaultStorePath )
                {
                    IsDefaultLocalStore = true;
                    if( _dbCurrent.PrototypeStoreUrl == ComponentDB.NoRemote )
                    {
                        monitor.Warn( $"The default local store '{Facade.DefaultStorePath}' must have a prototype store (typically remote). Setting it to the default public remote store '{Facade.DefaultStoreUrl}'." );
                        PrototypeStoreUrl = Facade.DefaultStoreUrl;
                    }
                }
                else
                {
                    // Not the default local store.
                    // If it's a new one, we set its prototype url to the local default store (file path).
                    if( _dbCurrent.UpdateSerialNumber == 0 )
                    {
                        PrototypeStoreUrl = new Uri( Facade.DefaultStorePath, UriKind.Absolute );
                    }
                }
            }
        }

        /// <summary>
        /// Gets whether this database has been successfully opened.
        /// </summary>
        public bool IsValid => _lastWriteTimeDB != Util.UtcMinValue;

        /// <summary>
        /// Gets whether this store is the default local store at <see cref="Facade.DefaultStorePath"/>.
        /// </summary>
        public bool IsDefaultLocalStore { get; }

        /// <summary>
        /// Gets the current (immutable) database.
        /// This is <see cref="ComponentDB.Empty"/> if <see cref="IsValid"/> is false.
        /// </summary>
        public ComponentDB ComponentDB => _dbCurrent;

        /// <summary>
        /// Registers a file that will be automatically deleted when this <see cref="LocalStore"/>
        /// will be disposed.
        /// </summary>
        /// <param name="fullPath">File path to be deleted.</param>
        public void RegisterFileToDelete( string fullPath )
        {
            _cleanupFiles.Add( fullPath );
        }

        /// <summary>
        /// Removes all registered components and all stored files are deleted.
        /// </summary>
        /// <returns>True on success, false on error.</returns>
        public bool Clear()
        {
            Throw.CheckState( IsValid );
            using( _monitor.OpenInfo( $"Clearing file store." ) )
            {
                try
                {
                    _dbCurrent = ComponentDB.Empty.WithPrototypeStoreUrl( PrototypeStoreUrl );
                    int deleted = _store.Delete( f => !f.Equals( DbXmlFileName, StringComparison.OrdinalIgnoreCase ) );
                    _monitor.CloseGroup( $"{deleted} entries removed." );
                    SaveDbCurrent();
                    return true;
                }
                catch( Exception ex )
                {
                    _monitor.Error( ex );
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets or sets the prototype store url that will be used as a fallback to resolve missing components and files.
        /// <see cref="ComponentDB.NoRemote"/> when no prototype store must be challenged.
        /// <para>
        /// The default local store MUST always have prototype store: setting it to <see cref="ComponentDB.NoRemote"/> will throw an <see cref="InvalidOperationException"/>.
        /// By default, its prototype store is the public remote store (<see cref="Facade.DefaultStoreUrl"/>) but this can be changed (typically to a private remote store).
        /// </para>
        /// <para>
        /// Any other local store can have no prototype store and their default prototype store is the local default (<see cref="Facade.DefaultStorePath"/>).
        /// </para>
        /// </summary>
        public Uri PrototypeStoreUrl
        {
            get => _dbCurrent.PrototypeStoreUrl;
            set
            {
                Throw.CheckState( IsValid );
                Throw.CheckNotNullArgument( value );
                if( value != _dbCurrent.PrototypeStoreUrl )
                {
                    if( IsDefaultLocalStore && value == ComponentDB.NoRemote )
                    {
                        Throw.InvalidOperationException( "Default local store must have a Prototype store." );
                    }
                    // We don't try to detect loop here (following the linked list of stores would be silly),
                    // we just avoid stupid mistakes.
                    if( _store.Url == value )
                    {
                        Throw.ArgumentException( $"Prototype store cannot be itself: {value}", nameof( PrototypeStoreUrl ) );
                    }
                    _dbCurrent = _dbCurrent.WithPrototypeStoreUrl( value );
                    if( _prototypeStore != null )
                    {
                        _prototypeStore.Dispose();
                        _prototypeStore = null;
                    }
                    SaveDbCurrent();
                }
            }
        }

        internal string StorePath =>_store.Url.LocalPath;

        internal IComponentImporter? GetPrototypeStore( IActivityMonitor monitor )
        {
            if( _prototypeStore == null && _dbCurrent.PrototypeStoreUrl != ComponentDB.NoRemote )
            {
                _prototypeStore = ClientRemoteStore.Create( monitor, _dbCurrent.PrototypeStoreUrl, null, GetHttpClient );
                if( _prototypeStore == null ) monitor.Warn( $"Unable to create a client for prototype store '{_dbCurrent.PrototypeStoreUrl}'." );
            }
            return _prototypeStore;
        }

        HttpClient GetHttpClient() => _httpClient ?? (_httpClient = new HttpClient());

        /// <summary>
        /// Creates an importer (<see cref="IComponentImporter"/>) for this local store or an export target (<see cref="IComponentPushTarget"/>) on the
        /// provided remote store uri or the "ambient" store if it exists.
        /// <para>
        /// When <paramref name="remoteUri"/> is null, an attempt to find an ambient store (see <see cref="Facade.LocateStorePath(string?, string?, bool, IActivityMonitor?)"/>)
        /// is made and if found, it is used as the remote store. Use <see cref="ComponentDB.NoRemote"/> to skip any ambient store lookup and to always return null.
        /// </para>
        /// <para>
        /// Note that if <see cref="IsDefaultLocalStore"/> is true, no attempt to resolve an ambient store is made.
        /// </para>
        /// </summary>
        /// <param name="monitor">The monitor to use.</param>
        /// <param name="remoteUri">
        /// Explicit remote store Url to use. When let to null, an attempt to locate an ambient store is made.
        /// Use <see cref="ComponentDB.NoRemote"/> to skip ambient store lookup and always return null.
        /// </param>
        /// <param name="apiKey">Optional api key for the <paramref name="remoteUri"/>.</param>
        /// <returns>The remote store or null if none can be resolved.</returns>
        public IRemoteStore? CreateStandardRemote( IActivityMonitor monitor, Uri? remoteUri = null, string? apiKey = null )
        {
            Throw.CheckNotNullArgument( monitor );
            if( remoteUri == ComponentDB.NoRemote ) return null;
            if( !IsDefaultLocalStore && remoteUri == null )
            {
                using( monitor.OpenInfo( $"Looking for an ambient store." ) )
                {
                    var ambientStore = Facade.LocateStorePath( "%CKSETUP_STORE%", StorePath, startFromBaseDirectory: false, monitor );
                    if( ambientStore == Facade.DefaultStorePath || ambientStore == StorePath )
                    {
                        monitor.CloseGroup( "Not found." );
                    }
                    else
                    {
                        remoteUri = new Uri( ambientStore, UriKind.Absolute );
                    }
                }
            }
            if( remoteUri != null && remoteUri == PrototypeStoreUrl )
            {
                monitor.Debug( $"Remote store Uri is the same as this store's prototype store: '{remoteUri}'." );
                remoteUri = null;
            }
            return remoteUri != null ? ClientRemoteStore.Create( monitor, remoteUri, apiKey ) : null;
        }

        /// <summary>
        /// Removes components that match a predicate.
        /// This does not remove any potential dependencies (a <see cref="ComponentDependency"/>
        /// only defines the name of its target and a minimal version).
        /// </summary>
        /// <param name="exclude">A predicate that must return true to remove the component.</param>
        /// <returns>The number of removed components.</returns>
        public int RemoveComponents( Func<Component, bool> exclude )
        {
            int originalCount = _dbCurrent.Components.Count;
            _dbCurrent = _dbCurrent.RemoveComponents( _monitor, exclude );
            SaveDbCurrent();
            return originalCount - _dbCurrent.Components.Count;
        }

        /// <summary>
        /// Deletes all files in the file store that are no more referenced by any existing components.
        /// This operation may take a long time (depending of the file numbers).
        /// </summary>
        /// <returns>The number of files deleted or -1 if an error occurred.</returns>
        public int GarbageCollectFiles()
        {
            Throw.CheckState( IsValid );
            using( _monitor.OpenInfo( $"Garbage collecting of file store." ) )
            {
                try
                {
                    var fileNames = new HashSet<string>( _dbCurrent.Components.SelectMany( c => c.Files ).Select( f => f.SHA1.ToString() ), StringComparer.OrdinalIgnoreCase );
                    fileNames.Add( DbXmlFileName );
                    fileNames.Add( StoreIdentifierFileName );
                    int deleted = _store.Delete( f => !fileNames.Contains( f ) );
                    _monitor.CloseGroup( $"{deleted} files deleted." );
                    return deleted;
                }
                catch( Exception ex )
                {
                    _monitor.Error( ex );
                    return -1;
                }
            }
        }

        /// <summary>
        /// Determines whether a component is registered.
        /// </summary>
        /// <param name="n">Component name.</param>
        /// <param name="t">Target framework of the component.</param>
        /// <param name="v">Version of the component.</param>
        public bool Contains( string n, TargetFramework t, SVersion v )
        {
            return _dbCurrent.Find( new ComponentRef( n, t, v ) ) != null;
        }

        /// <summary>
        /// Applies a mutation to the <see cref="ComponentDB"/>.
        /// This is a low level function that should be used with care (mainly for tests purposes).
        /// </summary>
        /// <param name="transform">The transform function that can return null to do nothing.</param>
        /// <returns>True if db changed, false otherwise.</returns>
        public bool ApplyDirectMutation( Func<ComponentDB,ComponentDB> transform )
        {
            Throw.CheckNotNullArgument( transform );
            Throw.CheckState( IsValid );
            var db = transform( _dbCurrent );
            if( db == null || db == _dbCurrent ) return false;
            _dbCurrent = db;
            SaveDbCurrent();
            return true;
        }

        /// <summary>
        /// Intermediate state that handles import.
        /// </summary>
        public class LocalImporter
        {
            readonly LocalStore _store;
            readonly HashSet<BinFolder> _toAdd;
            readonly IComponentImporter? _missingImporter;

            internal LocalImporter( LocalStore a, IComponentImporter? missingImporter )
            {
                _store = a;
                _toAdd = new HashSet<BinFolder>();
                _missingImporter = missingImporter;
            }

            /// <summary>
            /// Adds one or more components. Ignores them when already registered.
            /// </summary>
            /// <param name="folder">The (potentially multiples) component folder.</param>
            /// <returns>This importer (enable fluent syntax).</returns>
            public LocalImporter AddComponent( params BinFolder[] folder ) => AddComponent( (IEnumerable<BinFolder>)folder );

            /// <summary>
            /// Adds one or more components. Ignores them when already registered.
            /// </summary>
            /// <param name="folders">Folders to add. Must not contain null folders (<see cref="Import"/> will fail).</param>
            /// <returns>This importer (enable fluent syntax).</returns>
            public LocalImporter AddComponent( IEnumerable<BinFolder> folders )
            {
                Throw.CheckNotNullArgument( folders );
                _toAdd.AddRange( folders );
                return this;
            }

            /// <summary>
            /// Tries to import added component.
            /// </summary>
            /// <returns>True on success, false on error.</returns>
            public bool Import()
            {
                using( _store._monitor.OpenInfo( $"Importing {_toAdd.Count} local folders." ) )
                {
                    if( _toAdd.Count == 0 ) return true;
                    if( _toAdd.Any( f => f == null ) )
                    {
                        _store._monitor.Error( $"Added BinFolder at index {_toAdd.IndexOf( f => f == null )} is null." );
                        return false;
                    }
                    // Small trick: ordering by files count will tend to reduce
                    // Components mutation and multiple heads registration errors
                    // since smallest (ie. most basic) components come first.
                    return _store.LocalImport( _toAdd.OrderBy( f => f.Files.Count ), _missingImporter );
                }
            }
        }

        /// <summary>
        /// Creates a local importer object that can be used to import local <see cref="BinFolder"/>.
        /// </summary>
        /// <param name="missingImporter">
        /// Optional importer that should be provided in order to resolve embedded components
        /// that may not already be in this local store.
        /// </param>
        /// <returns>An importer.</returns>
        public LocalImporter CreateLocalImporter( IComponentImporter? missingImporter = null )
        {
            Throw.CheckState( IsValid );
            return new LocalImporter( this, missingImporter );
        }

        bool LocalImport( IOrderedEnumerable<BinFolder> folders, IComponentImporter? missingImporter )
        {
            Throw.CheckState( IsValid );
            ComponentDB db = _dbCurrent;
            var added = new Dictionary<ComponentRef, BinFolder>();
            foreach( var f in folders )
            {
                using( _monitor.OpenInfo( $"Adding {f.BinPath}." ) )
                {
                    var result = db.AddLocal( _monitor, f );
                    if( result.Error ) return false;
                    // We do not store files for models.
                    if( result.NewComponent != null && result.NewComponent.StoreFiles )
                    {
                        added[result.NewComponent.GetRef()] = f;
                    }
                    Debug.Assert( result.NewDB != null, "Since there is no error." );
                    db = result.NewDB;
                }
            }
            var newC = added.Select( kv => new { C = db.Components.Single( c => c.GetRef().Equals( kv.Key ) ), F = kv.Value } );
            if( newC.Any( c => c.C.Embedded.Count > 0 ) )
            {
                if( missingImporter != null || GetPrototypeStore( _monitor ) != null )
                {
                    using( _monitor.OpenInfo( $"Components have missing embedded components. Trying to use importer." ) )
                    {
                        var downloader = new ComponentDownloader( missingImporter, db, _store, _storageKind, _storeId, GetPrototypeStore );
                        var missing = new ComponentMissingDescription( newC.SelectMany( c => c.C.Embedded ).ToList() );
                        var updatedDB = downloader.Download( _monitor, missing );
                        if( updatedDB == null || updatedDB == db ) return false;
                        db = updatedDB;
                    }
                }
                else
                {
                    using( _monitor.OpenError( "Components have missing embedded components and no remote has been specified. Embedded components are required to be resolved." ) )
                    {
                        foreach( var c in newC.Where( c => c.C.Embedded.Count > 0 ) )
                        {
                            _monitor.Trace( $"'{c.C}' embeds: '{c.C.Embedded.Select( e => e.ToString() ).Concatenate( "', '" )}'." );
                        }
                    }
                    return false;
                }
            }
            foreach( var c in newC )
            {
                foreach( var f in c.C.Files )
                {
                    using( _monitor.OpenTrace( $"Importing file '{f}'." ) )
                    {
                        try
                        {
                            string key = f.SHA1.ToString();
                            if( _store.Exists( key ) )
                            {
                                _monitor.CloseGroup( "Already stored." );
                            }
                            else
                            {
                                using( var content = File.OpenRead( c.F.Files.First( b => b.ContentSHA1 == f.SHA1 ).FullPath ) )
                                {
                                    _store.Create( key, content, CompressionKind.None, _storageKind );
                                }
                            }
                        }
                        catch( Exception ex )
                        {
                            _monitor.Error( ex );
                            return false;
                        }
                    }
                }
            }
            _dbCurrent = db;
            SaveDbCurrent();
            return true;
        }

        /// <summary>
        /// Implementation of <see cref="IComponentDownloader"/>.
        /// It can use a <see cref="IComponentImporter"/> importer and/or the prototype store
        /// to update a local store with as much components and files that it can.
        /// </summary>
        class ComponentDownloader : IComponentDownloader
        {
            readonly IComponentImporter? _importer;
            readonly IStreamStore _store;
            readonly CompressionKind _storageKind;
            readonly string _storeId;
            readonly Func<IActivityMonitor, IComponentImporter?>? _prototypeStoreAccessor;
            ComponentDB _db;

            /// <summary>
            /// Initializes a downloader on a local store.
            /// </summary>
            /// <param name="a">The local store.</param>
            /// <param name="importer">An optional importer that will be used before the local store's prototype store (if any).</param>
            public ComponentDownloader( LocalStore a, IComponentImporter? importer )
                : this( importer, a._dbCurrent, a._store, a._storageKind, a._storeId, a.GetPrototypeStore )
            {
            }

            public ComponentDownloader( IComponentImporter? importer,
                                        ComponentDB db,
                                        IStreamStore store,
                                        CompressionKind storageKind,
                                        string storeId,
                                        Func<IActivityMonitor, IComponentImporter?>? prototypeStore )
            {
                _importer = importer;
                _store = store;
                _storageKind = storageKind;
                _storeId = storeId;
                _db = db;
                _prototypeStoreAccessor = prototypeStore;
            }

            public ComponentDB? Download( IActivityMonitor monitor, ComponentMissingDescription missing )
            {
                Throw.CheckNotNullArgument( monitor );
                Throw.CheckNotNullArgument( missing );
                if( missing.RequestStack.Contains( _storeId ) )
                {
                    monitor.Warn( $"This store ({_storeId}) has already been solicited." );
                    return _db;
                }

                IComponentImporter? store1, store2;
                IDisposable? monitorGroup = null;
                IComponentImporter? prototypeStore = _prototypeStoreAccessor?.Invoke( monitor );
                if( prototypeStore != null )
                {
                    store1 = prototypeStore;
                    if( _importer != null )
                    {
                        if( _importer.Url != prototypeStore.Url )
                        {
                            // First consider the remote, then the prototype.
                            store1 = _importer;
                            store2 = prototypeStore;
                            monitorGroup = monitor.OpenInfo( $"Using first specified remote store ({_importer.Url}) and then prototype store ({prototypeStore.Url}) if needed." );
                        }
                        else
                        {
                            store2 = null;
                            monitorGroup = monitor.OpenInfo( $"Using prototype store that is the same as specified remote store ({prototypeStore.Url})" );
                        }
                    }
                    else
                    {
                        store2 = null;
                        monitorGroup = monitor.OpenInfo( $"No remote store has been specified. Using prototype store only ({prototypeStore.Url})." );
                    }
                }
                else if( _importer != null )
                {
                    store1 = _importer;
                    store2 = null;
                    monitorGroup = monitor.OpenInfo( $"No prototype store. Using specified remote store ({_importer.Url})." );
                }
                else
                {
                    monitor.Warn( $"No prototype store nor remote store specified. Unable to download components." );
                    return _db;
                }
                try
                {
                    missing = missing.WithStoreIdentifier( _storeId );
                    monitor.Trace( "Downloading missing components:" );
                    monitor.Trace( missing.ToXml().ToString() );
                    return Download( monitor, missing, store1, store2 );
                }
                finally
                {
                    monitorGroup.Dispose();
                }
            }

            ComponentDB? Download( IActivityMonitor monitor, ComponentMissingDescription missing, IComponentImporter store1, IComponentImporter? store2 )
            {
                Debug.Assert( store1 != null );
                Debug.Assert( store2 == null || store1.Url != store2.Url );
                ComponentDB? db = DoDownload( monitor, missing, store1 );
                if( db != null && store2 != null )
                {
                    var unresolved = missing.Components.Where( c => db.Find( c ) == null ).ToList();
                    var notPerfectDeps = missing.Dependencies
                                            .Select( d => new { Dep = d, Best = db.FindBest( missing.TargetRuntime, d.UseName, d.UseMinVersion ) } )
                                            // The dependency has not been imported at all...
                                            // ...or there is a MinVersion and we found a greater version.
                                            //    (When there is no MinVersion requirement, we keep what the remote sent us.)
                                            // ...or the component has a perfect runtime and we did not obtain it.
                                            .Where( t => t.Best == null
                                                         || (t.Dep.UseMinVersion != null && t.Best.Version != t.Dep.UseMinVersion) 
                                                         || (t.Best.TargetFramework.GetSinglePerfectRuntime() != TargetRuntime.None
                                                             && t.Best.TargetFramework.GetSinglePerfectRuntime() != missing.TargetRuntime) )
                                            .ToList();
                    if( unresolved.Count > 0 || notPerfectDeps.Count > 0 )
                    {
                        using( monitor.OpenInfo($"Second store must be used for {unresolved.Count} components and {notPerfectDeps.Count} not perfectly resolved dependencies.") )
                        {
                            if( unresolved.Count > 0 ) monitor.Info( $"Unresolved components: {unresolved.Select( c => c.ToString() ).Concatenate()}" );
                            var notPerfect = Array.Empty<ComponentDependency>();
                            if( notPerfectDeps.Count > 0 )
                            {
                                notPerfect = new ComponentDependency[notPerfectDeps.Count];
                                using( monitor.OpenTrace( "Not perfectly resolved dependencies:" ) )
                                {
                                    for( int i = 0; i < notPerfectDeps.Count; ++i )
                                    {
                                        var np = notPerfectDeps[i];
                                        if( np.Best == null )
                                        {
                                            monitor.Info( $"{np.Dep} not found." );
                                        }
                                        else if( np.Dep.UseMinVersion != null && np.Best.Version != np.Dep.UseMinVersion )
                                        {
                                            monitor.Info( $"{np.Dep} found but in version {np.Best.Version}." );
                                        }
                                        else
                                        {
                                            monitor.Info( $"{np.Dep} found but for target runtime {np.Best.TargetFramework.GetSinglePerfectRuntime()}." );
                                        }
                                        notPerfect[i] = np.Dep;
                                    }
                                }
                            }
                            var stillMissing = new ComponentMissingDescription( missing.TargetRuntime, notPerfect, unresolved, missing.RequestStack );
                            db = DoDownload( monitor, stillMissing, store2 );
                        }
                    }
                }
                return db;
            }

            ComponentDB? DoDownload( IActivityMonitor monitor, ComponentMissingDescription missing, IComponentImporter store )
            {
                using( monitor.OpenTrace( $"Download from store: {store.Url}." ) )
                {
                    using( Stream? s = store.OpenImportStream( monitor, missing ) )
                    {
                        if( s == null || !ImportComponents( monitor, s, store ) ) return null;
                    }
                }
                return _db;
            }

            bool ImportComponents( IActivityMonitor monitor, Stream input, IComponentFileDownloader downloader )
            {
                using( monitor.OpenInfo( $"Starting import from {downloader.Url}." ) )
                {
                    var importResult = _db.Import( monitor, input );
                    if( importResult.NewDB == null ) return false;
                    var r = _store.DownloadImportResult( monitor, downloader, importResult, _storageKind );
                    if( r.Item2 > 0 ) return false;
                    _db = importResult.NewDB;
                    return true;
                }
            }
        }

        /// <summary>
        /// Creates a downloader for this local store.
        /// This is mainly for testing purposes.
        /// </summary>
        /// <param name="importer">Optional importer that will be used before this <see cref="PrototypeStoreUrl"/> if any.</param>
        /// <returns>The downloader that can be used to import components into this store.</returns>
        public IComponentDownloader CreateComponentDownloader( IComponentImporter? importer = null ) => new ComponentDownloader( this, importer );

        /// <summary>
        /// Captures result of <see cref="ExtractRuntimeDependencies"/>.
        /// </summary>
        public readonly struct ExtractRuntimeDependenciesResult
        {
            readonly DependencyResolver.RunResult _depsResult;

            internal ExtractRuntimeDependenciesResult( DependencyResolver.RunResult r )
            {
                _depsResult = r;
            }

            /// <summary>
            /// Gets whether the extraction succeeded.
            /// </summary>
            public bool Success => _depsResult.Success;

            /// <summary>
            /// Gets the target final runtime.
            /// <see cref="TargetRuntime.None"/> if <see cref="Success"/> is false.
            /// </summary>
            public TargetRuntime TargetRuntime => _depsResult.Origin.TargetRuntime;

            /// <summary>
            /// Gets the list of <see cref="BinFolder"/>.
            /// Null if <see cref="Success"/> is false.
            /// </summary>
            public IReadOnlyList<BinFolder> Targets => _depsResult.Origin.Targets;

            /// <summary>
            /// Gets the Models found among the <see cref="Targets"/>.
            /// Null if <see cref="Success"/> is false.
            /// </summary>
            public IReadOnlyList<BinFileAssemblyInfo> Models => _depsResult.Origin.Models;

            /// <summary>
            /// Gets the ModelDependents assemblies found among the <see cref="Targets"/>.
            /// Null if <see cref="Success"/> is false.
            /// </summary>
            public IReadOnlyList<BinFileAssemblyInfo> ModelDependents => _depsResult.Origin.ModelDependents;

        }

        /// <summary>
        /// Extracts required runtime support for Models in targets.
        /// </summary>
        /// <param name="workingDirectory">The working directory.</param>
        /// <param name="targets">The target folders.</param>
        /// <param name="missingImporter">Component importer. Can be null.</param>
        /// <param name="explicitDependencies">Optional extra dependencies that, when specified, must be resolved.</param>
        /// <param name="preferredTargetRuntimes">
        /// Optional ordered set of <see cref="TargetRuntime"/> that will be selected
        /// if possible. First is better.
        /// </param>
        /// <param name="fileHandler">
        /// Defaults to the function: ( monitor, componentFile, targetPath ) => true.
        /// <para>
        /// With this default, an attempt to copy the runtime file is always done and the target must not exist
        /// in the working directory otherwise an exception is thrown.
        /// </para>
        /// <para>
        /// To adapt the behavior, this function can return false to skip the copy or may delete the existing file and
        /// return true.
        /// </para>
        /// </param>
        /// <returns>Result encapsulation.</returns>
        public ExtractRuntimeDependenciesResult ExtractRuntimeDependencies(
            NormalizedPath workingDirectory,
            IEnumerable<BinFolder> targets,
            IComponentImporter? missingImporter,
            IEnumerable<SetupDependency> explicitDependencies,
            IEnumerable<TargetRuntime>? preferredTargetRuntimes = null,
            Func<IActivityMonitor,ComponentFile,NormalizedPath,bool>? fileHandler = null )
        {
            Throw.CheckState( IsValid );
            if( workingDirectory.IsEmptyPath || !Directory.Exists( workingDirectory ) )
            {
                Throw.ArgumentException( $"WorkingDirectory '{workingDirectory}' must exist.", nameof( workingDirectory ) );
            }
            if( !targets.Any() ) Throw.ArgumentException( "At least one target is required.", nameof( targets ) );

            DependencyResolver.RunResult resolveResult;
            ComponentFileCollector fileCollector;
            using( _monitor.OpenInfo( $"Resolving runtime support for '{targets.Select( t => t.BinPath.Path ).Concatenate()}' from local store '{StorePath}' with prototype store '{PrototypeStoreUrl}' and {(missingImporter == null ? "no additional remote store" : $"additional imports from '{missingImporter.Url}'")}." ) )
            {
                var resolver = _dbCurrent.GetRuntimeDependenciesResolver( _monitor, targets, explicitDependencies, preferredTargetRuntimes );
                if( resolver == null || resolver.IsEmpty ) return new ExtractRuntimeDependenciesResult();
                IComponentDownloader downloader = new ComponentDownloader( this, missingImporter );
                resolveResult = resolver.Run( _monitor, _storeId, downloader );
                if( !resolveResult.Success ) return new ExtractRuntimeDependenciesResult();
                Debug.Assert( resolveResult.Components != null );

                Debug.Assert( resolveResult.FinalDB != null );
                using( _monitor.OpenInfo( $"Selecting files from runtime dependencies (skipping Model components)." ) )
                {
                    fileCollector = new ComponentFileCollector();
                    fileCollector.Add( resolveResult.Components
                                                    .Where( x => x.ComponentKind != ComponentKind.Model )
                                                    .OrderByDescending( x => x.TargetFramework ) );
                    fileCollector.DumpResult( _monitor );
                }
            }
            if( fileHandler == null ) fileHandler = ( monitor, componentFile, targetPath ) => true;

            using( _monitor.OpenInfo( $"Copying runtime files to '{workingDirectory}'." ) )
            {
                foreach( var f in fileCollector.Result )
                {
                    var targetPath = workingDirectory.Combine( f.Name );
                    try
                    {
                        if( !fileHandler( _monitor, f, targetPath ) ) continue;
                        string fileKey = f.SHA1.ToString();
                        if( !_store.Exists( fileKey ) )
                        {
                            if( missingImporter == null )
                            {
                                _monitor.Error( $"Missing file '{f}' in local store." );
                                return new ExtractRuntimeDependenciesResult();
                            }
                            if( !_store.Download( _monitor, missingImporter, f, _storageKind ) ) return new ExtractRuntimeDependenciesResult();
                        }
                        _store.ExtractToFile( fileKey, targetPath, f.SHA1 );
                        _monitor.Debug( $"Extracted {f.Name}." );
                    }
                    catch( Exception ex )
                    {
                        _monitor.Error( $"While extracting '{f.Name}'.", ex );
                        return new ExtractRuntimeDependenciesResult();
                    }
                }
            }
            if( _dbCurrent != resolveResult.FinalDB )
            {
                _monitor.Info( $"Store changed: {resolveResult.FinalDB.Components.Count - _dbCurrent.Components.Count} new components." );
                _dbCurrent = resolveResult.FinalDB;
            }
            return new ExtractRuntimeDependenciesResult( resolveResult );
        }

        /// <summary>
        /// Exports available components.
        /// </summary>
        /// <param name="what">Required description.</param>
        /// <param name="output">Output stream.</param>
        /// <param name="monitor">Monitor to use.</param>
        /// <param name="remoteStoreUrl">
        /// Optional remote store url to use (in addition to the <see cref="PrototypeStoreUrl"/>) to resolve locally missing components.
        /// When let to null, an attempt to locate an ambient store is made (see <see cref="Facade.LocateStorePath(string?, string?, bool, IActivityMonitor?)"/>).
        /// To explicitly not use any remote store "no" or "none" can be specified (see <see cref="ComponentDB.CreateRemoteUri(string?)"/>).
        /// </param>
        public void ExportComponents( ComponentMissingDescription what,
                                      Stream output,
                                      IActivityMonitor monitor,
                                      string? remoteStoreUrl = null )
        {
            Throw.CheckState( IsValid );
            var locallyAvailable = _dbCurrent.FindAvailable( monitor, what );
            if( locallyAvailable.ShouldBeCompleted )
            {
                // To trigger download, this store should not already be in the request stack (this check is done in ComponentDownloader.Donwload anyway)
                // and we must have a prototype store or a remote store to call...
                IRemoteStore? remote = null;
                if( !what.RequestStack.Contains( _storeId )
                    && (GetPrototypeStore( monitor ) != null
                        || (remote = CreateStandardRemote( monitor, ComponentDB.CreateRemoteUri( remoteStoreUrl ) )) != null ) )
                {
                    try
                    {
                        using( monitor.OpenInfo( "Using remotes to complete the export." ) )
                        {
                            var m = new ComponentMissingDescription( what.TargetRuntime, locallyAvailable.NotPerfectDependencies, locallyAvailable.Unresolved, what.RequestStack );
                            var newDB = new ComponentDownloader( this, remote ).Download( monitor, m );
                            if( newDB != null && newDB != _dbCurrent )
                            {
                                _dbCurrent = newDB;
                                SaveDbCurrent();
                                locallyAvailable = _dbCurrent.FindAvailable( monitor, what );
                            }
                        }
                    }
                    finally
                    {
                        remote?.Dispose();
                    }
                }
            }
            _dbCurrent.Export( c => locallyAvailable.Resolved.Contains( c ), output );
        }

        /// <summary>
        /// Imports a set of components from a <see cref="Stream"/> and a downloader.
        /// </summary>
        /// <param name="input">Input stream.</param>
        /// <param name="downloader">Missing files downloader.</param>
        /// <returns>True on success, false on error.</returns>
        public bool ImportComponents( Stream input, IComponentFileDownloader downloader )
        {
            Throw.CheckNotNullArgument( downloader );
            Throw.CheckState( IsValid );
            using( _monitor.OpenInfo( "Starting import with file downloader." ) )
            {
                var n = _dbCurrent.Import( _monitor, input );
                if( n.NewDB == null ) return false;
                var r = _store.DownloadImportResult( _monitor, downloader, n, _storageKind );
                if( r.Item2 > 0 )
                {
                    _monitor.Error( $"{r.Item2} download errors. Import canceled." );
                    return false;
                }
                _dbCurrent = n.NewDB;
                SaveDbCurrent();
                return true;
            }
        }

        /// <summary>
        /// Imports a set of components from a <see cref="Stream"/> and returns
        /// a <see cref="PushComponentsResult"/>.
        /// </summary>
        /// <param name="input">Input stream.</param>
        /// <param name="sessionId">
        /// Optional session identifier.
        /// When not set, <see cref="PushComponentsResult.SessionId"/> is null on error
        /// and a new guid is generated on success.
        /// </param>
        /// <returns>True on success, false on error.</returns>
        public PushComponentsResult ImportComponents( Stream input, string? sessionId = null )
        {
            Throw.CheckState( IsValid );
            using( _monitor.OpenInfo( "Starting import." ) )
            {
                var n = _dbCurrent.Import( _monitor, input );
                if( n.NewDB == null ) return new PushComponentsResult( "Error while importing component into ComponentDB.", sessionId );
                IReadOnlyList<SHA1Value> missingFiles;
                if( n.Components != null && n.Components.Count > 0 )
                {
                    missingFiles = n.Components
                                    .Where( c => c.StoreFiles )
                                    .SelectMany( c => c.Files )
                                    .Select( f => f.SHA1 )
                                    .Distinct()
                                    .Where( sha => !_store.Exists( sha.ToString() ) )
                                    .ToList();
                }
                else missingFiles = Array.Empty<SHA1Value>();
                _dbCurrent = n.NewDB;
                SaveDbCurrent();
                return new PushComponentsResult( missingFiles, sessionId ?? Guid.NewGuid().ToString() );
            }
        }

        /// <summary>
        /// Pushes selected components to a <see cref="IComponentPushTarget"/>.
        /// </summary>
        /// <param name="filter">Filter for components to export.</param>
        /// <param name="target">Target for the components.</param>
        /// <returns>True on success, false otherwise.</returns>
        public bool PushComponents( Func<Component, bool> filter, IComponentPushTarget target )
        {
            Throw.CheckNotNullArgument( filter );
            Throw.CheckNotNullArgument( target );
            Throw.CheckState( IsValid );

            bool fileError = false;
            using( _monitor.OpenInfo( $"Starting component push." ) )
            {
                var result = target.PushComponents( _monitor, w => _dbCurrent.Export( filter, w ) );
                if( result.ErrorText != null )
                {
                    _monitor.Error( "Target error: " + result.ErrorText );
                    return false;
                }
                Debug.Assert( result.Files != null, "Since no error occurred." );
                int fileCount = 0;
                if( result.Files.Count > 0 )
                {
                    Debug.Assert( result.SessionId != null, "Since no error occurred and there are files." );
                    using ( _monitor.OpenInfo( $"Starting {result.Files.Count} push. SessionId={result.SessionId}." ) )
                    {
                        foreach( var sha in result.Files )
                        {
                            ++fileCount;
                            LocalStoredStream sf = _store.OpenRead( sha.ToString() );
                            if( sf.Stream != null )
                            {
                                try
                                {
                                    if( !target.PushFile( _monitor, result.SessionId, sha, w => sf.Stream.CopyTo( w ), sf.Kind ) )
                                    {
                                        _monitor.Error( $"Failed to push file {sha}." );
                                        --fileCount;
                                        fileError = true;
                                    }
                                }
                                finally
                                {
                                    sf.Stream.Dispose();
                                }
                            }
                            else
                            {
                                _monitor.Warn( $"Target requested file '{sha}' that does not locally exist." );
                                --fileCount;
                            }
                        }
                    }
                }
                if( !fileError ) _monitor.CloseGroup( $"Target is up to date. {fileCount} file uploaded." );
            }
            return !fileError;
        }


        /// <summary>
        /// Pushes selected components to a remote or local url.
        /// </summary>
        /// <param name="filter">Filter for components to export.</param>
        /// <param name="url">Url of the remote. Must not be <see cref="ComponentDB.NoRemote"/>.</param>
        /// <param name="apiKey">Optional api key.</param>
        /// <returns>True on success, false otherwise.</returns>
        public bool PushComponents( Func<Component, bool> filter, Uri url, string? apiKey )
        {
            Throw.CheckNotNullArgument( filter );
            Throw.CheckNotNullArgument( url );
            Throw.CheckState( IsValid );
            using( var remote = ClientRemoteStore.Create( _monitor, url, apiKey ) )
            {
                return remote != null && PushComponents( filter, remote );
            }
        }

        void SaveDbCurrent()
        {
            if( _dbOrigin != _dbCurrent && _lastWriteTimeDB != Util.UtcMaxValue )
            {
                _lastWriteTimeDB = _store.Save( _dbCurrent, _lastWriteTimeDB );
                _dbOrigin = _dbCurrent;
            }
            if( _lastWriteTimeDB == Util.UtcMaxValue )
            {
                _monitor.Error( $"Concurrency error detected: store has been updated by another process. Changes to the component database from this one will be lost." );
            }
        }

        /// <summary>
        /// Closes this store. Files registered by <see cref="RegisterFileToDelete"/> 
        /// are deleted and updated component database is written in the zip if required.
        /// </summary>
        public void Dispose()
        {
            if( _lastWriteTimeDB == Util.UtcMinValue ) return;
            using( _monitor.OpenInfo( "Closing Store." ) )
            {
                if( _cleanupFiles.Count > 0 )
                {
                    using( _monitor.OpenTrace( $"Cleaning {_cleanupFiles.Count} runtime files." ) )
                    {
                        foreach( var f in _cleanupFiles )
                        {
                            try
                            {
                                File.Delete( f );
                            }
                            catch( Exception ex )
                            {
                                _monitor.Warn( ex );
                            }
                        }
                        _cleanupFiles.Clear();
                    }
                }
                SaveDbCurrent();
                if( _httpClient != null ) _httpClient.Dispose();
                _httpClient = null;
                if( _prototypeStore != null ) _prototypeStore.Dispose();
                _prototypeStore = null;
                _store.Dispose();
                _dbCurrent = ComponentDB.Empty;
                _lastWriteTimeDB = Util.UtcMinValue;
            }
        }

        /// <summary>
        /// Opens an existing store directory. The directory must exist otherwise null is returned.
        /// </summary>
        /// <param name="monitor">Monitor to use. Can not be null.</param>
        /// <param name="path">Path to the directory to open.</param>
        /// <returns>A store or null on error.</returns>
        static public LocalStore? Open( IActivityMonitor monitor, string path )
        {
            Throw.CheckNotNullArgument( path );
            if( !Directory.Exists( path ) )
            {
                monitor.Error( $"Local store directory not found: '{path}'." );
                return null;
            }
            return OpenOrCreate( monitor, path );
        }

        /// <summary>
        /// Opens an existing store or creates a new one.
        /// Returns null on error or throws an exception.
        /// </summary>
        /// <param name="m">Monitor to use. Can not be null.</param>
        /// <param name="path">Path to the directory to open or create.</param>
        /// <param name="throwOnError">True to throw on error instead of returning null.</param>
        /// <returns>An store or null on error.</returns>
        static public LocalStore? OpenOrCreate( IActivityMonitor m, string path, bool throwOnError = false )
        {
            try
            {
                IStreamStore store;
                CompressionKind kind;
                store = new DirectoryStreamStore( path );
                kind = CompressionKind.GZiped;
                var a = new LocalStore( m, store, kind );
                if( a.IsValid )
                {
                    m.Trace( $"Local store opened on : '{path}'." );
                    return a;
                }
            }
            catch( Exception ex )
            {
                m.Fatal( $"While opening or creating store '{path}'.", ex );
                if( throwOnError ) throw;
            }
            if( throwOnError ) Throw.Exception( $"Unable to Open or Create a local store (path: {path}). Check previous logged error to fix the issue." );
            return null;
        }

    }
}
