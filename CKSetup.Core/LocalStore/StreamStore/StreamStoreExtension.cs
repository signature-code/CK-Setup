using CK.Core;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup.StreamStore
{
    /// <summary>
    /// Extends <see cref="StoredStream"/> with useful extension methods.
    /// </summary>
    static public class StreamStoreExtension
    {
        /// <summary>
        /// Creates a new entry with an initial text content.
        /// </summary>
        /// <param name="this">This stream store.</param>
        /// <param name="fullName">The resource full name (case insensitive).</param>
        /// <param name="text">Text (can not be null).</param>
        /// <param name="storageKind">Specifies the content's stream storage compression.</param>
        /// <returns>The creation time in Utc. Can be used for optimistic concurrency check.</returns>
        static public DateTime CreateText( this IStreamStore @this, string fullName, string text, CompressionKind storageKind )
        {
            void writer( Stream w )
            {
                byte[] b = Encoding.UTF8.GetBytes( text );
                w.Write( b, 0, b.Length );
            }
            return @this.Create( fullName,
                                 storageKind == CompressionKind.GZiped ? GetCompressShell( writer ) : writer,
                                 storageKind );
        }

        /// <summary>
        /// Updates an entry, optionnaly allow creating it if it does not existsand optionnally
        /// handles optimistic concurrency: the only case where this method must return false
        /// is when <paramref name="checkLastWriteTimeUtc"/> is provided and do not match the current
        /// last write time of the entry.
        /// </summary>
        /// <param name="this">This stream store.</param>
        /// <param name="fullName">The resource full name (case insensitive).</param>
        /// <param name="text">Text (can not be null).</param>
        /// <param name="storageKind">Specifies the content's stream storage compression.</param>
        /// <param name="allowCreate">True to automatically creates the entry if it does not already exist.</param>
        /// <param name="checkLastWriteTimeUtc">Optional optimistic concurrency check.</param>
        /// <returns>The new last write time in Utc or <see cref="CK.Core.Util.UtcMaxValue"/> if optimistic concurrency check failed.</returns>
        static public DateTime UpdateText( this IStreamStore @this, string fullName, string text, CompressionKind storageKind, bool allowCreate = false, DateTime checkLastWriteTimeUtc = default( DateTime ) )
        {
            void writer( Stream w )
            {
                byte[] b = Encoding.UTF8.GetBytes( text );
                w.Write( b, 0, b.Length );
            }
            return @this.Update( fullName,
                                  storageKind == CompressionKind.GZiped ? GetCompressShell( writer ) : writer,
                                  storageKind,
                                  allowCreate,
                                  checkLastWriteTimeUtc );
        }

        /// <summary>
        /// Reads an existing resource previously written by <see cref="CreateText"/> or <see cref="UpdateText"/>
        /// or null if it does not exist.
        /// </summary>
        /// <param name="this">This stream store.</param>
        /// <param name="fullName">The resource full name (case insensitive).</param>
        /// <returns>The text and its last write time in Utc or null if not found.</returns>
        static public Tuple<string,DateTime>? ReadText( this IStreamStore @this, string fullName )
        {
            LocalStoredStream s = @this.OpenRead( fullName, CompressionKind.None );
            if( s.Stream == null ) return null;
            using( var r = new StreamReader( s.Stream, Encoding.UTF8, false ) )
            {
                return Tuple.Create( r.ReadToEnd(), s.LastWriteTimeUtc );
            }
        }

        /// <summary>
        /// Creates a new entry with an initial content from an input stream.
        /// </summary>
        /// <param name="this">This component storage.</param>
        /// <param name="fullName">The resource full name (case insensitive).</param>
        /// <param name="input"></param>
        /// <param name="inputKind">Specifies the content's stream compression.</param>
        /// <param name="storageKind">Specifies the content's stream storage compression.</param>
        /// <returns>The creation time in Utc. Can be used for optimistic concurrency check.</returns>
        static public DateTime Create( this IStreamStore @this, string fullName, Stream input, CompressionKind inputKind, CompressionKind storageKind )
        {
            Throw.CheckNotNullArgument( input );
            switch( inputKind )
            {
                case CompressionKind.None:
                    switch( storageKind )
                    {
                        case CompressionKind.None:
                            return @this.Create( fullName, w => input.CopyTo( w ), storageKind );
                        case CompressionKind.GZiped:
                            return @this.Create( fullName, GetCompressShell( w => input.CopyTo( w ) ), storageKind );
                    }
                    break;
                case CompressionKind.GZiped:
                    switch( storageKind )
                    {
                        case CompressionKind.None:
                            using( var decompressor = new GZipStream( input, CompressionMode.Decompress, true ) )
                            {
                                return @this.Create( fullName, w => decompressor.CopyTo( w ), storageKind );
                            }
                        case CompressionKind.GZiped:
                            return @this.Create( fullName, w => input.CopyTo( w ), storageKind );
                    }
                    break;
            }
            return Throw.ArgumentException<DateTime>( "kind", $"Unknown {inputKind} or {storageKind}." );
        }

        /// <summary>
        /// Updates an entry, optionally allow creating it if it does not exists and optionally
        /// handles optimistic concurrency: the only case where this method must return false
        /// is when <paramref name="checkLastWriteTimeUtc"/> is provided and do not match the current
        /// last write time of the entry.
        /// </summary>
        /// <param name="this">This component storage.</param>
        /// <param name="fullName">The resource full name (case insensitive).</param>
        /// <param name="writer">The stream writer.</param>
        /// <param name="inputKind">Specifies the content's writer stream compression.</param>
        /// <param name="storageKind">Specifies the content's stream storage compression.</param>
        /// <param name="allowCreate">True to automatically creates the entry if it does not already exist.</param>
        /// <param name="checkLastWriteTimeUtc">Optional optimistic concurrency check.</param>
        /// <returns>The new last write time in Utc or <see cref="CK.Core.Util.UtcMaxValue"/> if optimistic concurrency check failed.</returns>
        static public DateTime Update( this IStreamStore @this, string fullName, Action<Stream> writer, CompressionKind inputKind, CompressionKind storageKind, bool allowCreate, DateTime checkLastWriteTimeUtc = default( DateTime ) )
        {
            Throw.CheckNotNullArgument( writer );
            switch( inputKind )
            {
                case CompressionKind.None:
                    switch( storageKind )
                    {
                        case CompressionKind.None:
                            return @this.Update( fullName, writer, storageKind, allowCreate, checkLastWriteTimeUtc );
                        case CompressionKind.GZiped:
                            return @this.Update( fullName, GetCompressShell( writer ), storageKind, allowCreate, checkLastWriteTimeUtc );
                    }
                    break;
                case CompressionKind.GZiped:
                    switch( storageKind )
                    {
                        case CompressionKind.None:
                            using( var buffer = Util.RecyclableStreamManager.GetStream() )
                            {
                                writer( buffer );
                                buffer.Position = 0;
                                using( var decompressor = new GZipStream( buffer, CompressionMode.Decompress, true ) )
                                {
                                    return @this.Update( fullName, w => decompressor.CopyTo( w ), storageKind, allowCreate, checkLastWriteTimeUtc );
                                }
                            }
                        case CompressionKind.GZiped:
                            return @this.Update( fullName, writer, storageKind, allowCreate, checkLastWriteTimeUtc );
                    }
                    break;
            }
            return Throw.ArgumentException<DateTime>( "kind", $"Unknown {inputKind} or {storageKind}." );
        }

        /// <summary>
        /// Creates a stream compressor wrapper action.
        /// </summary>
        /// <param name="writer">Stream writer.</param>
        /// <returns>The writer or an adapted writer.</returns>
        static public Action<Stream> GetCompressShell( Action<Stream> writer )
        {
            return w =>
            {
                using( var compressor = new GZipStream( w, CompressionLevel.Optimal, true ) )
                {
                    writer( compressor );
                    compressor.Flush();
                }
            };
        }

#pragma warning disable VSTHRD200 // Use "Async" suffix for async methods
        /// <summary>
        /// Creates a stream compressor wrapper action.
        /// </summary>
        /// <param name="writer">Stream writer.</param>
        /// <returns>The writer or an adapted writer.</returns>
        static public Func<Stream,Task> GetCompressShellAsync( Func<Stream,Task> writer )
        {
            return async w =>
            {
                using( var compressor = new GZipStream( w, CompressionLevel.Optimal, true ) )
                {
                    await writer( compressor );
                    await compressor.FlushAsync();
                }
            };
        }
#pragma warning restore VSTHRD200 // Use "Async" suffix for async methods

    }
}
