using CK.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup.StreamStore
{
    /// <summary>
    /// Captures a <see cref="Stream"/>, its <see cref="CompressionKind"/>
    /// and its <see cref="LastWriteTimeUtc"/>.
    /// </summary>
    public readonly struct LocalStoredStream
    {
        /// <summary>
        /// The kind of compression of the <see cref="Stream"/>.
        /// </summary>
        public readonly CompressionKind Kind;

        /// <summary>
        /// The actual stream.
        /// Null if the stream does not exist.
        /// </summary>
        public readonly Stream Stream;

        /// <summary>
        /// The last write time. Used for optimistic concurrency.
        /// </summary>
        public readonly DateTime LastWriteTimeUtc;

        /// <summary>
        /// Initializes a new <see cref="StoredStream"/> descriptor.
        /// </summary>
        /// <param name="k">The compression kind.</param>
        /// <param name="s">The stream itself.</param>
        /// <param name="lastWriteTimeUtc">The last write time.</param>
        public LocalStoredStream( CompressionKind k, Stream s, DateTime lastWriteTimeUtc )
        {
            Throw.CheckArgument( lastWriteTimeUtc.Kind == DateTimeKind.Utc );
            Kind = k;
            Stream = s;
            LastWriteTimeUtc = lastWriteTimeUtc;
        }

        /// <summary>
        /// Converts this <see cref="LocalStoredStream"/> to a <see cref="StoredStream"/>.
        /// </summary>
        /// <returns>The stored stream (no <see cref="LastWriteTimeUtc"/>).</returns>
        public StoredStream ToStoredStream() => new StoredStream( Kind, Stream );
    }
}
