using CK.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup.StreamStore
{
    /// <summary>
    /// Implements a <see cref="IStreamStore"/> with files in a directory.
    /// </summary>
    public sealed class DirectoryStreamStore : IStreamStore
    {
        readonly string _path;
        readonly string _pathNone;
        readonly string _pathGZiped;
        // Contains None files in gzip when GZip is requested and a None file exists.
        // Used as the lock when ensuring None -> NoneCached files.
        readonly string _pathGZipedNone;
        readonly string[] _paths;
        readonly Uri _url;

        readonly struct MetaEntry
        {
            public readonly CompressionKind Kind;
            public readonly FileInfo File;

            public MetaEntry( FileInfo e, CompressionKind k )
            {
                Kind = k;
                File = e;
            }
        }

        /// <summary>
        /// Initializes a new <see cref="DirectoryStreamStore"/> on a directory:
        /// the directory is created if it does not exist.
        /// An <see cref="IOException"/> is throw if the path is an existing file.
        /// </summary>
        /// <param name="path">The local directory path.</param>
        public DirectoryStreamStore( string path )
        {
            Debug.Assert( Enum.GetNames( typeof( CompressionKind ) ).SequenceEqual( new[] { "None", "GZiped" } ) );
            Debug.Assert( ((int[])Enum.GetValues( typeof( CompressionKind ) )).SequenceEqual( new[] { 0, 1 } ) );

            _path = Path.GetFullPath( path );
            _pathNone = FileUtil.NormalizePathSeparator( Path.Combine( _path, "None" ), true );
            _pathGZiped = FileUtil.NormalizePathSeparator( Path.Combine( _path, "GZiped" ), true );
            _pathGZipedNone = FileUtil.NormalizePathSeparator( Path.Combine( _path, "NoneGZiped" ), true );
            _paths = new string[] { _pathNone, _pathGZiped };
            _url = new Uri( _path, UriKind.Absolute );

            Directory.CreateDirectory( _pathNone );
            Directory.CreateDirectory( _pathGZiped );
        }

        MetaEntry Find( string fullName )
        {
            fullName = fullName.ToLowerInvariant();
            FileInfo e = new FileInfo( _pathNone + fullName );
            if( e.Exists ) return new MetaEntry( e, CompressionKind.None );
            e = new FileInfo( _pathGZiped + fullName );
            if( e.Exists ) return new MetaEntry( e, CompressionKind.GZiped );
            return new MetaEntry();
        }

        /// <summary>
        /// Gets the full and rooted path of this <see cref="DirectoryStreamStore"/>.
        /// </summary>
        public string StorePath => _path;

        /// <summary>
        /// Gets the full path of a file.
        /// </summary>
        /// <param name="k">The compression kind.</param>
        /// <param name="fullName">The entry name.</param>
        /// <returns>The full path of the stored file.</returns>
        public string GetFullPath( CompressionKind k, string fullName ) => _paths[(int)k] + fullName.ToLowerInvariant();

        bool IStreamStore.IsEmptyStore => !Directory.EnumerateFileSystemEntries( _pathNone ).Any()
                                          && !Directory.EnumerateFileSystemEntries( _pathGZiped ).Any();

        /// <summary>
        /// Checks whether the entry exists (regardless of its actual <see cref="CompressionKind"/>).
        /// </summary>
        /// <param name="fullName">The entry name.</param>
        /// <returns>True if the file exists, false otherwise.</returns>
        public bool Exists( string fullName ) => Find( fullName ).File != null;

        Uri IStreamStore.Url => _url;

        DateTime IStreamStore.Create( string fullName, Action<Stream> writer, CompressionKind storageKind )
        {
            fullName = GetFullPath( storageKind, fullName );
            try
            {
                using( var output = new FileStream( fullName, FileMode.CreateNew, FileAccess.Write, FileShare.None, 4096, FileOptions.SequentialScan ) )
                {
                    writer( output );
                }
                return File.GetLastWriteTimeUtc( fullName );
            }
            catch( Exception )
            {
                File.Delete( fullName );
                throw;
            }
        }

        DateTime IStreamStore.Update( string fullName, Action<Stream> writer, CompressionKind storageKind, bool allowCreate, DateTime checkLastWriteTimeUtc )
        {
            Debug.Assert( Enum.GetNames( typeof( CompressionKind ) ).SequenceEqual( new[] { "None", "GZiped" } ) );
            var e = Find( fullName );
            if( e.File == null && !allowCreate ) Throw.ArgumentException( $"{fullName} does not exist.", nameof( fullName ) );
            if( e.File != null )
            {
                if( checkLastWriteTimeUtc != default
                    && checkLastWriteTimeUtc != e.File.LastWriteTimeUtc )
                {
                    return Util.UtcMaxValue;
                }
                if( e.Kind != storageKind ) e.File.Delete();
            }
            fullName = GetFullPath( storageKind, fullName );
            using( var output = new FileStream( fullName, FileMode.Create, FileAccess.Write, FileShare.None, 4096, FileOptions.SequentialScan ) )
            {
                writer( output );
            }
            return File.GetLastWriteTimeUtc( fullName );
        }

        void IStreamStore.Delete( string fullName )
        {
            var e = Find( fullName );
            if( e.File != null ) e.File.Delete();
        }

        void IDisposable.Dispose()
        {
        }

        void IStreamStore.Flush()
        {
        }

        /// <inheritdoc />
        public LocalStoredStream OpenRead( string fullName, CompressionKind? preferred )
        {
            var e = Find( fullName );
            if( e.File == null ) return new LocalStoredStream();
            switch( preferred )
            {
                case null:
                    {
                        return new LocalStoredStream( e.Kind, e.File.OpenRead(), e.File.LastWriteTimeUtc );
                    }
                case CompressionKind.None:
                    {
                        Stream stream = e.File.OpenRead();
                        if( e.Kind == CompressionKind.GZiped )
                        {
                            stream = new GZipStream( stream, CompressionMode.Decompress, leaveOpen: false );
                        }
                        return new LocalStoredStream( CompressionKind.None, stream, e.File.LastWriteTimeUtc );
                    }
                case CompressionKind.GZiped:
                    {
                        if( e.Kind == CompressionKind.None )
                        {
                            e = EnsureCachedGZipedNone( e, fullName );
                        }
                        return new LocalStoredStream( CompressionKind.GZiped, e.File.OpenRead(), e.File.LastWriteTimeUtc );
                    }
            }
            return Throw.ArgumentException<LocalStoredStream>( nameof( e.Kind ), $"Unknown {e.Kind} or {preferred}." );
        }

        MetaEntry EnsureCachedGZipedNone( MetaEntry noneEntry, string fullName )
        {
            fullName = fullName.ToLowerInvariant();
            lock( _pathGZipedNone )
            {
                FileInfo e = new FileInfo( _pathGZipedNone + fullName );
                if( !e.Exists || e.LastWriteTime != noneEntry.File.LastWriteTime )
                {
                    Directory.CreateDirectory( _pathGZipedNone );
                    FileUtil.CompressFileToGzipFile( noneEntry.File.FullName, e.FullName, deleteSourceFileOnSuccess: false, CompressionLevel.Optimal );
                    File.SetLastWriteTime( e.FullName, noneEntry.File.LastWriteTime );
                }
                return new MetaEntry( e, CompressionKind.GZiped );
            }
        }

        void IStreamStore.ExtractToFile( string fullName, string targetPath, SHA1Value? checkSHA1, bool checkSHA1AlsoOnCopy )
        {
            var e = Find( fullName );
            if( e.File == null ) Throw.ArgumentException( $"'{fullName}' not found in Directory store.", nameof( fullName ) );
            Directory.CreateDirectory( Path.GetDirectoryName( targetPath )! );
            if( e.Kind == CompressionKind.None )
            {
                e.File.CopyTo( targetPath, false );
                if( checkSHA1.HasValue && checkSHA1AlsoOnCopy )
                {
                    ThrowOnInvalid( e, checkSHA1.Value, SHA1Value.ComputeFileHash( targetPath ) );
                }
            }
            else
            {
                SHA1Value v = default;
                try
                {
                    using( var s = OpenRead( fullName, CompressionKind.None ).Stream )
                    using( var sha = checkSHA1.HasValue ? new HashStream( HashAlgorithmName.SHA1, s, true, false ) : null )
                    using( var output = new FileStream( targetPath, FileMode.CreateNew, FileAccess.Write, FileShare.None, 4096, FileOptions.SequentialScan ) )
                    {
                        if( sha != null )
                        {
                            sha.CopyTo( output );
                            v = new SHA1Value( sha.GetFinalResult().AsSpan() );
                        }
                        else s.CopyTo( output );
                    }
                    if( checkSHA1.HasValue ) ThrowOnInvalid( e, checkSHA1.Value, v );
                }
                catch( InvalidDataException ex )
                {
                    Throw.InvalidDataException( $"Unable to decompress file. The file has probably been tampered. Please inspect the file '{e.File.FullName}' that may be an Html file modified by an antivirus system.", ex );
                }
            }

            static void ThrowOnInvalid( MetaEntry e, SHA1Value checkSHA1, SHA1Value sha1 )
            {
                if( sha1 != checkSHA1 )
                {
                    throw new SecurityException( $"File '{e.File.FullName}' has been tampered. Please check it. Current SHA1 is '{sha1}', expected SHA1 is {checkSHA1}." );
                }
            }
        }

        int IStreamStore.Delete( Func<string, bool> predicate )
        {
            int count = DoDelete( predicate, _pathNone );
            count += DoDelete( predicate, _pathGZiped );
            if( Directory.Exists( _pathGZipedNone ) )
            {
                count += DoDelete( predicate, _pathGZipedNone );
            }
            return count;
        }

        static int DoDelete( Func<string, bool> predicate, string prefix )
        {
            int count = 0;
            foreach( var e in Directory.EnumerateFiles( prefix, "*", SearchOption.AllDirectories ) )
            {
                if( predicate( e.Substring( prefix.Length ) ) )
                {
                    File.Delete( e );
                    ++count;
                }
            }
            return count;
        }
    }
}
