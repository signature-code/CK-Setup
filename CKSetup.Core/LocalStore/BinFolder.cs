using CK.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CKSetup
{
    /// <summary>
    /// Encapsulates binary folder informations.
    /// </summary>
    public class BinFolder
    {
        BinFolder( NormalizedPath p, IReadOnlyList<BinFileInfo> files )
        {
            BinPath = p;
            Files = files;
            Assemblies = files.OfType<BinFileAssemblyInfo>().ToArray();
            foreach( var b in files )
            {
                b.SetBinFolderAndUpdateLocalDependencies( this );
            }
            var heads = Assemblies.Where( f => f.ComponentKind != ComponentKind.None ).ToList();
            Components = heads.ToArray();
            foreach( var c in Components )
            {
                foreach( var dep in c.LocalDependencies ) heads.Remove( dep );
            }
            Heads = heads.ToArray();
        }

        bool Initialize( IActivityMonitor m )
        {
            if( !Components.Any() )
            {
                m.Error( "Unable to find a Model or Setup Dependency assembly in this folder." );
                return false;
            }
            if( !Heads.Any() )
            {
                m.Error( "Components are co-dependent. How did you get there?" );
                return false;
            }
            return true;
        }

        /// <summary>
        /// Reads the content of a folder.
        /// Returns null on error.
        /// </summary>
        /// <param name="monitor">The monitor to use. Can not be null.</param>
        /// <param name="binPath">Directory path to read.</param>
        /// <returns>The bin folder or null on error.</returns>
        static public BinFolder? ReadBinFolder( IActivityMonitor monitor, string binPath )
        {
            Throw.CheckNotNullArgument( monitor );
            Throw.CheckNotNullArgument( binPath );
            NormalizedPath bPath = Path.GetFullPath( binPath );
            using( monitor.OpenInfo( $"Reading files from '{bPath}'." ) )
            {
                try
                {
                    if( !Directory.Exists( bPath ) )
                    {
                        monitor.Error( $"Directory not found: {bPath}." );
                        return null;
                    }
                    var b = new BinFolder( bPath, BinFileInfo.ReadFiles( monitor, bPath ) );
                    return b.Initialize( monitor ) ? b : null;

                }
                catch( Exception ex )
                {
                    monitor.Error( ex );
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the normalized full path of this BinFolder (<see cref="FileUtil.NormalizePathSeparator(string, bool)"/>).
        /// It ends with a directory separator.
        /// </summary>
        public NormalizedPath BinPath { get; }

        /// <summary>
        /// Gets all the discovered files.
        /// </summary>
        public IReadOnlyList<BinFileInfo> Files { get; }

        /// <summary>
        /// Gets all the discovered files.
        /// </summary>
        public IReadOnlyList<BinFileAssemblyInfo> Assemblies { get; }

        /// <summary>
        /// Gets the components (Model or Setup Dependency).
        /// </summary>
        public IReadOnlyList<BinFileAssemblyInfo> Components { get; }

        /// <summary>
        /// Gets the head components.
        /// These are the <see cref="Components"/> (Model or Setup Dependency) that are not
        /// referenced by other Components.
        /// </summary>
        public IReadOnlyList<BinFileAssemblyInfo> Heads { get; }

        /// <summary>
        /// Overridden to return the path and the number of files.
        /// </summary>
        /// <returns>A readable string.</returns>
        public override string ToString()
        {
            return $"{BinPath} ({Files.Count} files)";
        }

    }
}
