using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup
{
    /// <summary>
    /// Extends <see cref="TargetFramework"/> and <see cref="TargetRuntime"/> with
    /// helpers.
    /// </summary>
    public static class TargetRuntimeOrFrameworkExtension
    {

        /// <summary>
        /// Gets the runtimes that are common to multiple frameworks.
        /// </summary>
        /// <param name="this">This framework.</param>
        /// <param name="others">Other frameworks.</param>
        /// <returns>The common runtimes.</returns>
        static public IEnumerable<TargetRuntime> GetCommonRuntimes( this TargetFramework @this, IEnumerable<TargetFramework> others )
        {
            var intersect = new HashSet<TargetRuntime>( @this.GetAllowedRuntimes() );
            foreach( var f in others )
            {
                intersect.IntersectWith( f.GetAllowedRuntimes() );
            }
            return intersect;
        }

        static TargetRuntime[] NetFramework461Runtimes = new TargetRuntime[] { TargetRuntime.Net461, TargetRuntime.Net462, TargetRuntime.Net47, TargetRuntime.Net471, TargetRuntime.Net472 };
        static TargetRuntime[] NetFramework462Runtimes = new TargetRuntime[] { TargetRuntime.Net462, TargetRuntime.Net47, TargetRuntime.Net471, TargetRuntime.Net472 };
        static TargetRuntime[] NetFramework47Runtimes = new TargetRuntime[] { TargetRuntime.Net47, TargetRuntime.Net471, TargetRuntime.Net472 };
        static TargetRuntime[] NetFramework471Runtimes = new TargetRuntime[] { TargetRuntime.Net471, TargetRuntime.Net472 };
        static TargetRuntime[] NetFramework472Runtimes = new TargetRuntime[] { TargetRuntime.Net472 };

        static TargetRuntime[] NetCoreApp11Runtimes = new TargetRuntime[] { TargetRuntime.NetCoreApp11, TargetRuntime.NetCoreApp20, TargetRuntime.NetCoreApp21, TargetRuntime.NetCoreApp22, TargetRuntime.NetCoreApp30, TargetRuntime.NetCoreApp31, TargetRuntime.Net50, TargetRuntime.Net60, TargetRuntime.Net70, TargetRuntime.Net80 };
        static TargetRuntime[] NetCoreApp20Runtimes = new TargetRuntime[] { TargetRuntime.NetCoreApp20, TargetRuntime.NetCoreApp21, TargetRuntime.NetCoreApp22, TargetRuntime.NetCoreApp30, TargetRuntime.NetCoreApp31, TargetRuntime.Net50, TargetRuntime.Net60, TargetRuntime.Net70, TargetRuntime.Net80 };
        static TargetRuntime[] NetCoreApp21Runtimes = new TargetRuntime[] { TargetRuntime.NetCoreApp21, TargetRuntime.NetCoreApp22, TargetRuntime.NetCoreApp30, TargetRuntime.NetCoreApp31, TargetRuntime.Net50, TargetRuntime.Net60, TargetRuntime.Net70, TargetRuntime.Net80 };
        static TargetRuntime[] NetCoreApp22Runtimes = new TargetRuntime[] { TargetRuntime.NetCoreApp22, TargetRuntime.NetCoreApp30, TargetRuntime.NetCoreApp31, TargetRuntime.Net50, TargetRuntime.Net60, TargetRuntime.Net70, TargetRuntime.Net80 };
        static TargetRuntime[] NetCoreApp30Runtimes = new TargetRuntime[] { TargetRuntime.NetCoreApp30, TargetRuntime.NetCoreApp31, TargetRuntime.Net50, TargetRuntime.Net60, TargetRuntime.Net70, TargetRuntime.Net80 };
        static TargetRuntime[] NetCoreApp31Runtimes = new TargetRuntime[] { TargetRuntime.NetCoreApp31, TargetRuntime.Net50, TargetRuntime.Net60, TargetRuntime.Net70, TargetRuntime.Net80 };
        static TargetRuntime[] Net5Runtimes = new TargetRuntime[] { TargetRuntime.Net50, TargetRuntime.Net60, TargetRuntime.Net70, TargetRuntime.Net80 };
        static TargetRuntime[] Net6Runtimes = new TargetRuntime[] { TargetRuntime.Net60, TargetRuntime.Net70, TargetRuntime.Net80 };
        static TargetRuntime[] Net7Runtimes = new TargetRuntime[] { TargetRuntime.Net70, TargetRuntime.Net80 };
        static TargetRuntime[] Net8Runtimes = new TargetRuntime[] { TargetRuntime.Net80 };

        static TargetRuntime[] SupportedNetStandardRuntimes = NetCoreApp11Runtimes.Concat( NetFramework461Runtimes ).ToArray();
        //
        // NetStandard 1.0, 1.1, 1.2 and 1.3 are actually supported by older platforms (i.e. netstandard1.0 is net45 compliant).
        // However, we start support at Net461 and netcoreapp1.1: the following mappings are not accurate regarding old platforms
        // but we don't care about them.
        // See: https://docs.microsoft.com/en-us/dotnet/standard/net-standard#net-implementation-support
        //
        static TargetRuntime[] NetStandard10Runtimes = SupportedNetStandardRuntimes;
        static TargetRuntime[] NetStandard11Runtimes = SupportedNetStandardRuntimes;
        static TargetRuntime[] NetStandard12Runtimes = SupportedNetStandardRuntimes;
        static TargetRuntime[] NetStandard13Runtimes = SupportedNetStandardRuntimes;
        // Those are accurate.
        static TargetRuntime[] NetStandard14Runtimes = SupportedNetStandardRuntimes;
        static TargetRuntime[] NetStandard15Runtimes = SupportedNetStandardRuntimes;
        static TargetRuntime[] NetStandard16Runtimes = SupportedNetStandardRuntimes;
        static TargetRuntime[] NetStandard20Runtimes = SupportedNetStandardRuntimes;
        static TargetRuntime[] NetStandard21Runtimes = NetCoreApp30Runtimes;

        /// <summary>
        /// Gets the different <see cref="TargetRuntime"/> that can handle a <see cref="TargetFramework"/>.
        /// </summary>
        /// <param name="this">This framework.</param>
        /// <returns>The compatible runtimes.</returns>
        static public IReadOnlyList<TargetRuntime> GetAllowedRuntimes( this TargetFramework @this )
        {
            switch( @this )
            {
                case TargetFramework.Net451:
                case TargetFramework.Net46:
                case TargetFramework.Net461: return NetFramework461Runtimes;
                case TargetFramework.Net462: return NetFramework462Runtimes;
                case TargetFramework.Net47: return NetFramework47Runtimes;
                case TargetFramework.Net471: return NetFramework471Runtimes;
                case TargetFramework.Net472: return NetFramework472Runtimes;
                case TargetFramework.NetStandard10: return NetStandard10Runtimes;
                case TargetFramework.NetStandard11: return NetStandard11Runtimes;
                case TargetFramework.NetStandard12: return NetStandard12Runtimes;
                case TargetFramework.NetStandard13: return NetStandard13Runtimes;
                case TargetFramework.NetStandard14: return NetStandard14Runtimes;
                case TargetFramework.NetStandard15: return NetStandard15Runtimes;
                case TargetFramework.NetStandard16: return NetStandard16Runtimes;
                case TargetFramework.NetStandard20: return NetStandard20Runtimes;
                case TargetFramework.NetStandard21: return NetStandard21Runtimes;
                case TargetFramework.NetCoreApp11: return NetCoreApp11Runtimes;
                case TargetFramework.NetCoreApp20: return NetCoreApp20Runtimes;
                case TargetFramework.NetCoreApp21: return NetCoreApp21Runtimes;
                case TargetFramework.NetCoreApp22: return NetCoreApp22Runtimes;
                case TargetFramework.NetCoreApp30: return NetCoreApp30Runtimes;
                case TargetFramework.NetCoreApp31: return NetCoreApp31Runtimes;
                case TargetFramework.Net50: return Net5Runtimes;
                case TargetFramework.Net60: return Net6Runtimes;
                case TargetFramework.Net70: return Net7Runtimes;
                case TargetFramework.Net80: return Net8Runtimes;
                default: return Array.Empty<TargetRuntime>();
            }
        }

        /// <summary>
        /// Gets the <see cref="TargetRuntime"/> that is a perfect match to this <see cref="TargetFramework"/>.
        /// NetStandard frameworks have no perfect runtime, but for <see cref="TargetFramework"/> that
        /// actually are <see cref="TargetRuntime"/> this returns the associated TargetRuntime enum value.
        /// </summary>
        /// <param name="this">This framework.</param>
        /// <returns>The single perfect runtime or <see cref="TargetRuntime.None"/>.</returns>
        static public TargetRuntime GetSinglePerfectRuntime( this TargetFramework @this )
        {
            switch( @this )
            {
                case TargetFramework.Net451:
                case TargetFramework.Net46:
                case TargetFramework.Net461: return TargetRuntime.Net461;
                case TargetFramework.Net462: return TargetRuntime.Net462;
                case TargetFramework.Net47: return TargetRuntime.Net47;
                case TargetFramework.Net471: return TargetRuntime.Net471;
                case TargetFramework.Net472: return TargetRuntime.Net472;
                case TargetFramework.NetStandard20: return TargetRuntime.None;
                case TargetFramework.NetCoreApp11: return TargetRuntime.NetCoreApp11;
                case TargetFramework.NetCoreApp20: return TargetRuntime.NetCoreApp20;
                case TargetFramework.NetCoreApp21: return TargetRuntime.NetCoreApp21;
                case TargetFramework.NetCoreApp22: return TargetRuntime.NetCoreApp22;
                case TargetFramework.NetCoreApp30: return TargetRuntime.NetCoreApp30;
                case TargetFramework.NetCoreApp31: return TargetRuntime.NetCoreApp31;
                case TargetFramework.Net50: return TargetRuntime.Net50;
                case TargetFramework.Net60: return TargetRuntime.Net60;
                case TargetFramework.Net70: return TargetRuntime.Net70;
                case TargetFramework.Net80: return TargetRuntime.Net80;
                default: return TargetRuntime.None; 
            }
        }

        /// <summary>
        /// Gets whether a <see cref="TargetFramework"/> is compatible with a <see cref="TargetRuntime"/>.
        /// </summary>
        /// <param name="this">This framework.</param>
        /// <param name="r">The runtime.</param>
        /// <returns>True if the framework can run on the given runtime.</returns>
        static public bool CanWorkOn( this TargetFramework @this, TargetRuntime r )
        {
            return GetAllowedRuntimes( @this ).Contains( r );
        }

        /// <summary>
        /// Gets whether this <see cref="TargetFramework"/> is a .NetFramework.
        /// </summary>
        /// <param name="this">This framework.</param>
        /// <returns>True if this is a .Net framework.</returns>
        static public bool IsNetFramework( this TargetFramework @this )
        {
            return (((int)@this >> 16) & 127) > 0;
        }

        /// <summary>
        /// Gets the "net461", "netcoreapp1.0" string representation as it appears in csproj files
        /// and output folders.
        /// </summary>
        /// <param name="f">This target framework.</param>
        /// <returns>The framework string.</returns>
        public static string? ToStringFramework( this TargetFramework f )
        {
            switch( f )
            {
                case TargetFramework.Net451: return "net451";
                case TargetFramework.Net46: return "net46";
                case TargetFramework.Net461: return "net461";
                case TargetFramework.Net462: return "net462";
                case TargetFramework.Net47: return "net47";
                case TargetFramework.Net471: return "net471";
                case TargetFramework.Net472: return "net472";
                case TargetFramework.NetStandard10: return "netstandard1.0";
                case TargetFramework.NetStandard11: return "netstandard1.1";
                case TargetFramework.NetStandard12: return "netstandard1.2";
                case TargetFramework.NetStandard13: return "netstandard1.3";
                case TargetFramework.NetStandard14: return "netstandard1.4";
                case TargetFramework.NetStandard15: return "netstandard1.5";
                case TargetFramework.NetStandard16: return "netstandard1.6";
                case TargetFramework.NetStandard20: return "netstandard2.0";
                case TargetFramework.NetStandard21: return "netstandard2.1";
                case TargetFramework.NetCoreApp10: return "netcoreapp1.0";
                case TargetFramework.NetCoreApp11: return "netcoreapp1.1";
                case TargetFramework.NetCoreApp20: return "netcoreapp2.0";
                case TargetFramework.NetCoreApp21: return "netcoreapp2.1";
                case TargetFramework.NetCoreApp22: return "netcoreapp2.2";
                case TargetFramework.NetCoreApp30: return "netcoreapp3.0";
                case TargetFramework.NetCoreApp31: return "netcoreapp3.1";
                case TargetFramework.Net50: return "net5.0";
                case TargetFramework.Net60: return "net6.0";
                case TargetFramework.Net70: return "net7.0";
                case TargetFramework.Net80: return "net8.0";
                default: return null;
            }
        }

        /// <summary>
        /// Parses the TargetFrameworkAttribute (from System.Runtime.Versioning, like ".NETStandard,Version=v1.6") or like target
        /// frameworks in csproj (and published folders, like "net461").
        /// </summary>
        /// <param name="rawTargetFramework">
        /// String to parse like ".NETFramework,Version=v4.7", ".NETStandard,Version=v1.6", or ".NETCoreApp,Version=v2.0" or
        /// like target frameworks in csproj (and published folders) like "net451", "netstandard1.2", "netcoreapp2.2".
        /// </param>
        /// <returns>The target framework or <see cref="TargetFramework.None"/> if it is not known.</returns>
        static public TargetFramework TryParse( string? rawTargetFramework )
        {
            switch( rawTargetFramework )
            {
                case "net451": 
                case ".NETFramework,Version=v4.5.1": return TargetFramework.Net451;
                case "net461": 
                case ".NETFramework,Version=v4.6.1": return TargetFramework.Net461;
                case "net462":
                case ".NETFramework,Version=v4.6.2": return TargetFramework.Net462;
                case "net47":
                case ".NETFramework,Version=v4.7": return TargetFramework.Net47;
                case "netstandard1.0":
                case ".NETStandard,Version=v1.0": return TargetFramework.NetStandard10;
                case "netstandard1.1":
                case ".NETStandard,Version=v1.1": return TargetFramework.NetStandard11;
                case "netstandard1.2":
                case ".NETStandard,Version=v1.2": return TargetFramework.NetStandard12;
                case "netstandard1.3":
                case ".NETStandard,Version=v1.3": return TargetFramework.NetStandard13;
                case "netstandard1.4":
                case ".NETStandard,Version=v1.4": return TargetFramework.NetStandard14;
                case "netstandard1.5":
                case ".NETStandard,Version=v1.5": return TargetFramework.NetStandard15;
                case "netstandard1.6":
                case ".NETStandard,Version=v1.6": return TargetFramework.NetStandard16;
                case "netstandard2.0":
                case ".NETStandard,Version=v2.0": return TargetFramework.NetStandard20;
                case "netstandard2.1":
                case ".NETStandard,Version=v2.1": return TargetFramework.NetStandard21;
                case "netcoreapp1.0":
                case ".NETCoreApp,Version=v1.0": return TargetFramework.NetCoreApp10;
                case "netcoreapp1.1":
                case ".NETCoreApp,Version=v1.1": return TargetFramework.NetCoreApp11;
                case "netcoreapp2.0":
                case ".NETCoreApp,Version=v2.0": return TargetFramework.NetCoreApp20;
                case "netcoreapp2.1":
                case ".NETCoreApp,Version=v2.1": return TargetFramework.NetCoreApp21;
                case "netcoreapp2.2":
                case ".NETCoreApp,Version=v2.2": return TargetFramework.NetCoreApp22;
                case "netcoreapp3.0":
                case ".NETCoreApp,Version=v3.0": return TargetFramework.NetCoreApp30;
                case "netcoreapp3.1":
                case ".NETCoreApp,Version=v3.1": return TargetFramework.NetCoreApp31;
                case "net5.0":
                case ".NETCoreApp,Version=v5.0": return TargetFramework.Net50;
                case "net6.0":
                case ".NETCoreApp,Version=v6.0": return TargetFramework.Net60;
                case "net7.0":
                case ".NETCoreApp,Version=v7.0": return TargetFramework.Net70;
                case "net8.0":
                case ".NETCoreApp,Version=v8.0": return TargetFramework.Net80;
            }
            return TargetFramework.None;
        }


    }
}
