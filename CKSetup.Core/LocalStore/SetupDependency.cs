using CK.Core;
using CSemVer;
using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CKSetup
{

    /// <summary>
    /// Describes a setup dependency between <see cref="ComponentKind.Model"/> or <see cref="ComponentKind.SetupDependency"/>
    /// to <see cref="ComponentKind.SetupDependency"/> components.
    /// </summary>
    public class SetupDependency 
    {
        /// <summary>
        /// Default name of a manual dependency source.
        /// </summary>
        public const string ManualSourceName = "(Manual)";

        /// <summary>
        /// Initializes a manual, explicit, setup dependency.
        /// </summary>
        /// <param name="useName">The assembly name of the dependency. Must not end with .exe or .dll.</param>
        /// <param name="sourceName">The source name should be the name of the assembly that requires this dependency.</param>
        /// <param name="useMinVersion">The minimal version. Null to let the version free.</param>
        public SetupDependency( string useName, SVersion? useMinVersion = null, string sourceName = ManualSourceName )
        {
            Throw.CheckNotNullOrWhiteSpaceArgument( sourceName );
            Throw.CheckNotNullOrWhiteSpaceArgument( useName );
            SourceName = sourceName;
            UseName = useName;
            UseMinVersion = useMinVersion;
        }

        /// <summary>
        /// Name of the XElement that represents a SetupDependency.
        /// </summary>
        public static readonly XName xDependency = XNamespace.None + "Dependency";
        static readonly XName xSource = XNamespace.None + "Source";
        static readonly XName xName = XNamespace.None + "Name";
        static readonly XName xMinVersion = XNamespace.None + "MinVersion";

        /// <summary>
        /// Initializes a new dependency from a <see cref="XElement"/>.
        /// </summary>
        /// <param name="e">The xml element.</param>
        public SetupDependency( XElement e )
        {
            SourceName = (string?)e.Attribute( xSource ) ?? ManualSourceName;
            var useName = (string?)e.Attribute( xName );
            if( String.IsNullOrWhiteSpace( useName ) ) Throw.ArgumentNullException( "SetupDependency must have a non empty Name attribute.", nameof(e) );
            UseName = useName;
            var v = (string?)e.Attribute( xMinVersion );
            if( !String.IsNullOrWhiteSpace( v ) )
            {
                UseMinVersion = SVersion.Parse( v );
            }
        }

        /// <summary>
        /// Creates a Dependency xml element.
        /// </summary>
        /// <returns>The xml element.</returns>
        public XElement ToXml()
        {
            Debug.Assert( UseMinVersion == null || (UseMinVersion.IsValid && UseMinVersion.NormalizedText != null) );
            return new XElement( xDependency,
                                 SourceName != ManualSourceName ? new XAttribute( xSource, SourceName ) : null,
                                 new XAttribute( xName, UseName ),
                                 UseMinVersion != null ? new XAttribute( xMinVersion, UseMinVersion.NormalizedText! ) : null
                               );
        }

        /// <summary>
        /// We use a static special SVersion (reference equality) to mark UseThisVersion and the 
        /// BinFileAssemblyInfo ctor will mutate the UseMinVersion property.
        /// </summary>
        static SVersion UseThisVersionMarker = SVersion.Create( 0, 0, 0 );

        /// <summary>
        /// Parses "UseName/UseMinVersion", "UseName" alone or "UseName/&lt;no min version&gt;".
        /// </summary>
        /// <param name="d">The dependency as a string.</param>
        /// <param name="sourceName">Optional <see cref="SourceName"/>.</param>
        /// <returns>The parsed dependency.</returns>
        public static SetupDependency Parse( string d, string sourceName = "(Manual)" )
        {
            if( !TryParse( d, out var dep, sourceName ) ) Throw.ArgumentException( $"Invalid SetupDependency format: {d}. Expected a string like CKSeup.Runner/6.0.0-a01", nameof(d) );
            return dep;
        }

        /// <summary>
        /// Tries to parse "UseName/UseMinVersion", "UseName" alone or "UseName/&lt;no min version&gt;".
        /// </summary>
        /// <param name="dependency">The dependency as a string.</param>
        /// <param name="dep">The parsed dependency on success, null otherwise.</param>
        /// <param name="sourceName">Optional <see cref="SourceName"/>.</param>
        /// <returns>True on success.</returns>
        public static bool TryParse( string dependency, [NotNullWhen( returnValue: true )] out SetupDependency? dep, string sourceName = "(Manual)" )
        {
            Throw.CheckNotNullOrEmptyArgument( dependency );
            var both = dependency.Split( '/' );
            SVersion? v = null;
            if( both.Length == 1 ) dep = new SetupDependency( both[0] );
            else if( both.Length == 2
                        && ((v = SVersion.TryParse( both[1] )).IsValid
                            || both[1].Length == 0
                            || both[1] == "<no min version>" ) )
            {
                dep = new SetupDependency( both[0], v, sourceName );
            }
            else
            {
                dep = null;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Called by the BinFileAssemblyInfo constructor for RequiredSetupDependency attributes.
        /// </summary>
        /// <param name="ctorArgs">Arguments of the constructor.</param>
        /// <param name="source">The source model or setup dependency.</param>
        internal SetupDependency( IList<CustomAttributeArgument> ctorArgs, BinFileAssemblyInfo source )
        {
            if( ctorArgs.Count != 2
                || !(ctorArgs[0].Value is string)
                || !(ctorArgs[1].Value == null || ctorArgs[1].Value is string) )
            {
                Throw.ArgumentException( $"{source.Name.Name} has an invalid RequiredSetupDependency attribute: there must be a first non null string and a second nullable string arguments." );
            }
            UseName = (string)ctorArgs[0].Value;
            if( string.IsNullOrWhiteSpace( UseName ) )
            {
                Throw.ArgumentException( $"{source.Name.Name} has an empty name in its RequiredSetupDependency attribute." );
            }
            if( UseName.EndsWith(".dll", StringComparison.OrdinalIgnoreCase )
                || UseName.EndsWith( ".exe", StringComparison.OrdinalIgnoreCase ) )
            {
                UseName = UseName.Substring( 0, UseName.Length - 4 );
            }
            string v = (string)ctorArgs[1].Value;
            if( v == "UseThisVersion" )
            {
                UseMinVersion = UseThisVersionMarker;
            }
            else if( !string.IsNullOrWhiteSpace( v ) )
            {
                UseMinVersion = SVersion.TryParse( v );
                if( !UseMinVersion.IsValid )
                {
                    Throw.ArgumentException( $"{source.Name.Name} has an invalid version '{v}' in its RequiredSetupDependency attribute." );
                }
            }
            SourceName = source.Name.Name;
        }

        /// <summary>
        /// Gets the source name of this SetupDependency.
        /// It is the short name of the assembly that declares this dependency
        /// or the "(Manual)" marker.
        /// </summary>
        public string SourceName { get; }

        /// <summary>
        /// Gets the dependency name.
        /// </summary>
        public string UseName { get; }

        /// <summary>
        /// Gets the minimal version of the dependency.
        /// </summary>
        public SVersion? UseMinVersion { get; private set; }

        /// <summary>
        /// Overridden to return "UseName/UseMinVersion" or "UseName/&lt;no min version&gt;" if no version.
        /// </summary>
        /// <returns>A readable string.</returns>
        public override string ToString()
        {
            return $"{UseName}/{(UseMinVersion == null ? "<no min version>" : UseMinVersion.NormalizedText)}";
        }

        internal void OnSourceVersionKnown( SVersion v )
        {
            if( ReferenceEquals( UseMinVersion, UseThisVersionMarker ) )
            {
                UseMinVersion = v;
            }
        }
    }
}
