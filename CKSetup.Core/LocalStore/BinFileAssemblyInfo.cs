using CK.Core;
using CSemVer;
using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup
{
    /// <summary>
    /// Captures assembly related information when a <see cref="BinFileInfo"/> is an assembly.
    /// </summary>
    public class BinFileAssemblyInfo : BinFileInfo
    {
        HashSet<BinFileAssemblyInfo>? _localDependencies;
        ComponentRef _cRef;

        internal BinFileAssemblyInfo( string fullPath, string localFileName, int len, AssemblyDefinition a, IActivityMonitor m )
            : base( fullPath, localFileName, len )
        {
            InfoVersion = a.GetInformationalVersion();
            Name = a.Name;
            RawTargetFramework = a.CustomAttributes
                                        .Where( x => x.AttributeType.FullName == "System.Runtime.Versioning.TargetFrameworkAttribute" && x.HasConstructorArguments )
                                        .Select( x => x.ConstructorArguments[0].Value as string )
                                        .FirstOrDefault();
            TargetFramework = TargetRuntimeOrFrameworkExtension.TryParse( RawTargetFramework );
            AssemblyVersion = a.Name.Version;
            AssemblyReferences = a.MainModule.AssemblyReferences.ToArray();
            SetupDependencies = a.CustomAttributes
                                    .Select( x => (x.AttributeType.FullName == "CK.Setup.RequiredSetupDependencyAttribute"
                                                        ? new SetupDependency( x.ConstructorArguments, this )
                                                        : null) )
                                    .Where( x => x != null )
                                    .Select( x => x! )
                                    .ToArray();
            bool isSetupDependency = false, isModel = false, isModelDependent = false, isModelDependentSource = false;
            foreach( var attr in a.CustomAttributes )
            {
                if( attr.AttributeType.FullName == "CK.Setup.IsSetupDependencyAttribute" )
                {
                    isSetupDependency = true;
                }
                else if( attr.AttributeType.FullName == "CK.Setup.IsModelAttribute" )
                {
                    isModel = true;
                }
                else if( attr.AttributeType.FullName == "CK.Setup.IsModelDependentSourceAttribute" )
                {
                    isModelDependentSource = true;
                }
                else if( attr.AttributeType.FullName == "CK.Setup.IsModelDependentAttribute" )
                {
                    isModelDependent = true;
                }
            }
            if( isSetupDependency )
            {
                if( isModel )
                {
                    Throw.CKException( $"Component '{localFileName}' is marked with both IsModel and IsSetupDependency attribute." );
                }
                if( isModelDependent )
                {
                    Throw.CKException( $"Component '{localFileName}' is marked with both IsModelDependent and IsSetupDependency attribute." );
                }
                if( isModelDependentSource )
                {
                    Throw.CKException( $"Component '{localFileName}' is marked with both IsModelDependentSource and IsSetupDependency attribute." );
                }
                ComponentKind = ComponentKind.SetupDependency;
            }
            else if( isModel )
            {
                isModelDependentSource = true;
                ComponentKind = ComponentKind.Model;
            }
            else if( SetupDependencies.Count > 0 )
            {
                Throw.CKException( $"Component '{localFileName}' has at least one RequiredSetupDepency attribute. It must also be marked with IsModel or IsSetupDependency attribute." );
            }
            if( ComponentKind != ComponentKind.None )
            {
                if( TargetFramework == TargetFramework.None )
                {
                    if( RawTargetFramework == null )
                    {
                        Throw.CKException( $"Component '{localFileName}' must be marked with a TargetFrameworkAttribute." );
                    }
                    Throw.CKException( $"Component '{localFileName}' has TargetFrameworkAttribute '{RawTargetFramework}' that is invalid or not currently handled." );
                }
                if( InfoVersion.OriginalInformationalVersion == null )
                {
                    InfoVersion = InformationalVersion.Zero;
                    m.Warn( $"Component '{localFileName}' does not have a standard CSemVer version in its InformationalVersion. Using the ZeroVersion." );
                }
                else if( !InfoVersion.IsValidSyntax )
                {
                    Throw.CKException( $"Component '{localFileName}' standard CSemVer version error: {InfoVersion.ParseErrorMessage}. InfoVersion: '{InfoVersion.OriginalInformationalVersion}'." );
                }
                Debug.Assert( InfoVersion.Version != null );
                foreach( var d in SetupDependencies ) d.OnSourceVersionKnown( InfoVersion.Version );
                _cRef = new ComponentRef( Name.Name, TargetFramework, InfoVersion.Version );
            }
            IsModelDependent = isModelDependent;
            // A Model and a ModelDependent assembly is a ModelDependentSource.
            IsModelDependentSource = isModel || isModelDependent || isModelDependentSource;
        }

        /// <summary>
        /// Gets the <see cref="ComponentKind"/>. Can be <see cref="ComponentKind.None"/>.
        /// </summary>
        public ComponentKind ComponentKind { get; }

        /// <summary>
        /// Gets whether files should be stored: only Models in .Net framework don't need to be stored.
        /// </summary>
        public bool StoreFiles => ComponentKind == ComponentKind.SetupDependency
                                    || (ComponentKind == ComponentKind.Model && !_cRef.TargetFramework.IsNetFramework());

        /// <summary>
        /// Gets the assembly <see cref="AssemblyNameDefinition"/>.
        /// Null if this file is not an assembly.
        /// </summary>
        public AssemblyNameDefinition Name { get; }

        /// <summary>
        /// Gets the CSemVer standard informational version info if it exists.
        /// </summary>
        public InformationalVersion InfoVersion { get; }

        /// <summary>
        /// Gets the .Net assembly version if it exists.
        /// Null otherwise.
        /// </summary>
        public override Version AssemblyVersion { get; }

        /// <summary>
        /// Gets the TargetFramework if <see cref="System.Runtime.Versioning.TargetFrameworkAttribute"/>
        /// exists on the assembly.
        /// </summary>
        public string? RawTargetFramework { get; }

        /// <summary>
        /// Gets the TargetFramework parsed from <see cref="RawTargetFramework"/>.
        /// <see cref="TargetFramework.None"/> if unknown on null.
        /// </summary>
        public TargetFramework TargetFramework { get; }

        /// <summary>
        /// Gets the corresponding <see cref="ComponentRef"/> if this is a Component.
        /// </summary>
        public ComponentRef ComponentRef => _cRef;

        /// <summary>
        /// Gets the setup dependencies (from attributes named CK.Setup.RequiredSetupDependencyAttribute).
        /// </summary>
        public IReadOnlyList<SetupDependency> SetupDependencies { get; }

        /// <summary>
        /// Gets whether this is a source for Model Dependent assemblies.
        /// Assemblies that depend on this one will appear in ModelDependent list but this one will not.
        /// When <see cref="ComponentKind"/> is <see cref="ComponentKind.Model"/> this is necessarily true.
        /// This is also true when <see cref="IsModelDependent"/> is true.
        /// </summary>
        public bool IsModelDependentSource { get; }

        /// <summary>
        /// Gets whether this is a a Model Dependent assembly.
        /// This assembly and assemblies that depends on this one will appear in ModelDependent list. 
        /// </summary>
        public bool IsModelDependent { get; }

        /// <summary>
        /// Gets the list of <see cref="AssemblyNameReference"/>.
        /// </summary>
        public IReadOnlyCollection<AssemblyNameReference> AssemblyReferences { get; }

        /// <summary>
        /// Gets the recursive local dependencies.
        /// This is a subset of the files in the owning <see cref="BinFolder"/>.
        /// </summary>
        public IReadOnlyCollection<BinFileAssemblyInfo> LocalDependencies => _localDependencies!;

        internal override HashSet<BinFileAssemblyInfo> SetBinFolderAndUpdateLocalDependencies( BinFolder binFolder )
        {
            base.SetBinFolderAndUpdateLocalDependencies( binFolder );
            if( _localDependencies == null )
            {
                _localDependencies = new HashSet<BinFileAssemblyInfo>();
                foreach( var dep in AssemblyReferences
                                        .Select( n => binFolder.Assemblies.FirstOrDefault( b => b.Name.Name == n.Name ) )
                                        .Where( dep => dep != null ) )
                {
                    Debug.Assert( dep != null );  
                    if( _localDependencies.Add( dep ) )
                    {
                        _localDependencies.UnionWith( dep.SetBinFolderAndUpdateLocalDependencies( binFolder ) );
                    }
                }
            }
            return _localDependencies;
        }

        /// <summary>
        /// Overridden to return detailed informations.
        /// </summary>
        /// <returns>A readable string.</returns>
        public override string ToString()
        {
            string s = Name.FullName;
            if( ComponentKind != ComponentKind.None ) s += $" - SVersion: {_cRef.Version} ({ComponentKind})";
            else s += " - InformationalVersion: " + InfoVersion.OriginalInformationalVersion == null ? "(null)" : InfoVersion.OriginalInformationalVersion;
            return s;
        }

    }
}
