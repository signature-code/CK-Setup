using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK.Core;
using System.IO;
using System.Threading;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// Captures missing information from a <see cref="ComponentDB"/>.
    /// This is used to request a remote store when needed.
    /// </summary>
    public class ComponentMissingDescription
    {
        /// <summary>
        /// Initializes a <see cref="ComponentMissingDescription"/> with dependencies and explicit components.
        /// </summary>
        /// <param name="t">Target runtime for dependencies.</param>
        /// <param name="d">Dependencies. Can be null.</param>
        /// <param name="c">Explicit missing components. Can be null.</param>
        /// <param name="r">Request stack signatures. Can be null.</param>
        public ComponentMissingDescription( TargetRuntime t, IReadOnlyCollection<ComponentDependency>? d, IReadOnlyCollection<ComponentRef>? c, IReadOnlyList<string>? r )
        {
            TargetRuntime = t;
            Dependencies = d ?? Array.Empty<ComponentDependency>();
            Components = c ?? Array.Empty<ComponentRef>();
            RequestStack = r ?? Array.Empty<string>();
        }

        /// <summary>
        /// Initializes a <see cref="ComponentMissingDescription"/> only with explicit missing components.
        /// </summary>
        /// <param name="components">Explicit missing components. Can not be null.</param>
        public ComponentMissingDescription( IReadOnlyCollection<ComponentRef> components )
        {
            Throw.CheckNotNullArgument( components );
            TargetRuntime = TargetRuntime.None;
            Dependencies = Array.Empty<ComponentDependency>();
            Components = components;
            RequestStack = Array.Empty<string>();
        }

        /// <summary>
        /// Initializes a <see cref="ComponentMissingDescription"/> from a XElement.
        /// </summary>
        /// <param name="e">Xml element. Can not be null.</param>
        public ComponentMissingDescription( XElement e )
        {
            TargetRuntime = e.AttributeEnum( DBXmlNames.Runtime, TargetRuntime.None );
            Dependencies = e.Elements( DBXmlNames.Dependency ).Select( d => new ComponentDependency( d ) ).ToArray();
            Components = e.Elements( DBXmlNames.Ref ).Select( d => new ComponentRef( d ) ).ToArray();
            RequestStack = e.Elements( DBXmlNames.RequestStack ).Elements().Select( e => e.Value ).ToArray();
        }

        /// <summary>
        /// Creates a xml representation of this <see cref="ComponentMissingDescription"/>.
        /// </summary>
        /// <returns>The XElement.</returns>
        public XElement ToXml()
        {
            return new XElement( DBXmlNames.Missing,
                                    new XAttribute( DBXmlNames.Runtime, TargetRuntime.ToString() ),
                                    Dependencies.Select( d => d.ToXml() ),
                                    Components.Select( c => c.ToXml() ),
                                    new XElement( DBXmlNames.RequestStack, RequestStack.Select( s => new XElement( DBXmlNames.S, s ) ) ) );
        }

        /// <summary>
        /// Target runtime for <see cref="Dependencies"/>.
        /// </summary>
        public TargetRuntime TargetRuntime { get; }

        /// <summary>
        /// Dependencies to resolve according to the <see cref="TargetRuntime"/>.
        /// Never null.
        /// </summary>
        public IReadOnlyCollection<ComponentDependency> Dependencies { get; }

        /// <summary>
        /// Explicit components missing.
        /// Never null.
        /// </summary>
        public IReadOnlyCollection<ComponentRef> Components { get; }

        /// <summary>
        /// Gets a set of unique string signatures that identify stores that are submitting
        /// the missing request.
        /// </summary>
        public IReadOnlyList<string> RequestStack { get; }

        /// <summary>
        /// Returns a new <see cref="ComponentMissingDescription"/> with a new item in the <see cref="RequestStack"/>.
        /// </summary>
        /// <param name="storeId">The store identifier to append.</param>
        /// <returns>A new missing description.</returns>
        public ComponentMissingDescription WithStoreIdentifier( string storeId )
        {
            return new ComponentMissingDescription( TargetRuntime, Dependencies, Components, RequestStack.Append( storeId ).ToArray() );
        }
    }

}
