using CK.Core;
using CSemVer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CKSetup
{
    /// <summary>
    /// A component dependency is defined by a <see cref="UseName"/> component's name
    /// and its <see cref="UseMinVersion"/> minimal version.
    /// This is an immutable and equatable value (both name and minimal version must be equal).
    /// </summary>
    public class ComponentDependency : IEquatable<ComponentDependency>
    {
        /// <summary>
        /// Initializes a new <see cref="ComponentDependency"/>.
        /// </summary>
        /// <param name="useName">See <see cref="UseName"/>.</param>
        /// <param name="useMinVersion">
        /// Optional minimal version: can be null.
        /// When not null, it must be valid and should be based on a short form (see <see cref="CSVersion.IsLongForm"/>).
        /// If it happens to be a <see cref="CSVersion"/>, <see cref="CSVersion.ToNormalizedForm()"/> is called.
        /// </param>
        public ComponentDependency( string useName, SVersion? useMinVersion )
        {
            Debug.Assert( !string.IsNullOrWhiteSpace( useName ) );
            Debug.Assert( useMinVersion == null || useMinVersion.IsValid );
            UseName = useName;
            UseMinVersion = useMinVersion?.AsCSVersion?.ToNormalizedForm() ?? useMinVersion;
        }

        internal ComponentDependency( XElement e )
        {
            UseName = (string)e.AttributeRequired( DBXmlNames.Name );
            var vS = (string?)e.Attribute( DBXmlNames.SVersion ) ?? (string?)e.Attribute( DBXmlNames.Version );
            if( vS != null )
            {
                var v = SVersion.Parse( vS );
                UseMinVersion = v.AsCSVersion?.ToNormalizedForm() ?? v;
            }
        }

        internal XElement ToXml()
        {
            return new XElement( DBXmlNames.Dependency, 
                                    new XAttribute( DBXmlNames.Name, UseName ),
                                    UseMinVersion != null 
                                        ? new XAttribute( DBXmlNames.Version, UseMinVersion ) 
                                        : null );
        }

        /// <summary>
        /// Gets the name of the referenced component.
        /// </summary>
        public string UseName { get; }

        /// <summary>
        /// Gets the minimal version. Can be null.
        /// When not null, this version is normalized to the short form (<see cref="SVersion.ToNormalizedString()"/>).
        /// </summary>
        public SVersion? UseMinVersion { get; }

        /// <summary>
        /// Overridden to return a string with <see cref="UseName"/> and <see cref="UseMinVersion"/>.
        /// </summary>
        /// <returns>A readable string.</returns>
        public override string ToString() => $"-> {UseName}/{(UseMinVersion == null ? "<no min version>" : UseMinVersion.NormalizedText)}";

        /// <summary>
        /// Checks equality with another dependency.
        /// </summary>
        /// <param name="other">The other dependency.</param>
        /// <returns>True if they are equal, false otherwise.</returns>
        public bool Equals( ComponentDependency? other ) => other != null && other.UseName == UseName && other.UseMinVersion == UseMinVersion;

        /// <summary>
        /// Gets the hash code.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode() => (UseMinVersion?.GetHashCode() ?? 0) ^ UseName.GetHashCode();

        /// <summary>
        /// Overridden to support value semantics.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True if they are equal, false otherwise.</returns>
        public override bool Equals( object? obj ) => obj is ComponentDependency d ? Equals( d ) : false;

    }
}
