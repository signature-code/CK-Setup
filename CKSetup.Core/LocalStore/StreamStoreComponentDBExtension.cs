using CK.Core;
using CKSetup.StreamStore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CKSetup.StreamStore
{
    /// <summary>
    /// Extends <see cref="IStreamStore"/> with <see cref="ComponentDB"/> related method extensions.
    /// </summary>
    public static class StreamStoreComponentDBExtension
    {
        /// <summary>
        /// Reads or creates the <see cref="LocalStore.DbXmlFileName"/> manifest file in this store.
        /// </summary>
        /// <param name="this">This store.</param>
        /// <param name="db">Database to update.</param>
        /// <returns>The component database or null on error.</returns>
        /// <param name="checkLastWriteTimeUtc">Optimistic concurrency check.</param>
        /// <returns>The new last write time in Utc or <see cref="CK.Core.Util.UtcMaxValue"/> if optimistic concurrency check failed.</returns>
        static public DateTime Save( this IStreamStore @this, ComponentDB db, DateTime checkLastWriteTimeUtc )
        {
            return @this.UpdateText( LocalStore.DbXmlFileName, db.ToXml().ToString(), CompressionKind.None, false, checkLastWriteTimeUtc );
        }


        /// <summary>
        /// Reads or creates the <see cref="LocalStore.DbXmlFileName"/> manifest file in this store.
        /// If the store is not empty but the <see cref="LocalStore.DbXmlFileName"/> is not found, a new empty one is created.
        /// </summary>
        /// <param name="this">This store.</param>
        /// <param name="monitor">The monitor to use.</param>
        /// <returns>The component database and the last write time of the db.xml file in Utc (or null on error).</returns>
        static public Tuple<ComponentDB,DateTime>? Initialize( this IStreamStore @this, IActivityMonitor monitor )
        {
            ComponentDB db = ComponentDB.Empty;
            Debug.Assert( db.PrototypeStoreUrl == ComponentDB.NoRemote );
            DateTime lastWriteTime;
            if( @this.IsEmptyStore )
            {
                monitor.Info( $"Initializing new empty store." );
                lastWriteTime = @this.CreateText( LocalStore.DbXmlFileName, db.ToXml().ToString( SaveOptions.DisableFormatting ), CompressionKind.None );
            }
            else
            {
                Tuple<string,DateTime>? text = null;
                try
                {
                    text = @this.ReadText( LocalStore.DbXmlFileName );
                    if( text != null )
                    {
                        db = new ComponentDB( XDocument.Parse( text.Item1 ).Root! );
                        lastWriteTime = text.Item2;
                        monitor.Trace( $"Opened store with {db.Components.Count} components." );
                    }
                    else
                    {
                        monitor.Warn( $"Store is not valid ({LocalStore.DbXmlFileName} manifest not found). Initializing a new one." );
                        lastWriteTime = @this.CreateText( LocalStore.DbXmlFileName, db.ToXml().ToString( SaveOptions.DisableFormatting ), CompressionKind.None );
                    }
                }
                catch( Exception ex )
                {
                    using( monitor.OpenFatal( $"Invalid {LocalStore.DbXmlFileName} manifest.", ex ) )
                    {
                        monitor.Trace( text?.Item1 ?? "Unable to read text from manifest." );
                    }
                    return null;
                }
            }
            return Tuple.Create( db, lastWriteTime );
        }

        /// <summary>
        /// Tries to download a missing file to this store.
        /// </summary>
        /// <param name="this">This store.</param>
        /// <param name="monitor">Monitor to use. Can not be null.</param>
        /// <param name="downloader">Downloader. Can not be null.</param>
        /// <param name="f">File to download. Can not be null.</param>
        /// <param name="storageKind">Compression kind to use in store.</param>
        /// <returns>True on success, false otherwise.</returns>
        static public bool Download( this IStreamStore @this, IActivityMonitor monitor, IComponentFileDownloader downloader, ComponentFile f, CompressionKind storageKind )
        {
            Debug.Assert( monitor != null && downloader != null && f != null );
            using( monitor.OpenInfo( $"Downloading {f}." ) )
            {
                var storedStream = downloader.GetDownloadStream( monitor, f.SHA1, storageKind );
                if( storedStream.Stream == null )
                {
                    monitor.Error( $"Unable to obtain file by its SHA1 from downloader." );
                    return false;
                }
                try
                {
                    @this.Create( f.SHA1.ToString(), storedStream.Stream, storedStream.Kind, storageKind );
                    monitor.CloseGroup( "Successfully downloaded." );
                    return true;
                }
                catch( Exception ex )
                {
                    monitor.Error( ex );
                    return false;
                }
            }
        }

        /// <summary>
        /// Tries to download any missing files from the new component's files 
        /// of a <see cref="ComponentDB.ImportResult"/> to this store.
        /// </summary>
        /// <param name="this">This store.</param>
        /// <param name="monitor">Monitor to use. Can not be null.</param>
        /// <param name="downloader">Downloader. Can not be null.</param>
        /// <param name="r">Import result. <see cref="ComponentDB.ImportResult.NewDB"/> must not be null (no error must have occurred).</param>
        /// <param name="storageKind">Compression kind to use in store.</param>
        /// <returns>A tuple with the number of successfully imported files and number of failures.</returns>
        static public Tuple<int,int> DownloadImportResult( 
            this IStreamStore @this, 
            IActivityMonitor monitor, 
            IComponentFileDownloader downloader, 
            in ComponentDB.ImportResult r, 
            CompressionKind storageKind )
        {
            Debug.Assert( r.NewDB != null );
            Debug.Assert( monitor != null && downloader != null );
            int successCount = 0;
            int failedCount = 0;
            if( r.Components != null && r.Components.Count > 0 )
            {
                var newFiles = r.Components
                                .Where( c => c.StoreFiles )
                                .SelectMany( c => c.Files )
                                .ToLookup( f => f.SHA1 )
                                .Where( g => !@this.Exists( g.Key.ToString() ) )
                                .ToList();
                if( newFiles.Count == 0 ) monitor.Info( "No files to download: all files are already in the store." );
                else using( monitor.OpenInfo( $"Downloading {newFiles.Count} missing files." ) )
                {
                    foreach( var f in newFiles.Select( g => g.First() ) )
                    {
                        if( !@this.Download( monitor, downloader, f, storageKind ) ) ++failedCount;
                        else ++successCount;
                    }
                }
            }
            return Tuple.Create( successCount, failedCount );
        }
    }
}
