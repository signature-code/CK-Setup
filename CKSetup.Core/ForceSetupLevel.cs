using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKSetup
{
    /// <summary>
    /// Describes the behavior of the setup regarding any changes in the <see cref="SetupConfiguration.BinPaths"/> Model
    /// and ModelDependent files from previous runs.
    /// </summary>
    public enum ForceSetupLevel
    {
        /// <summary>
        /// Setup is skipped if the <see cref="SetupConfiguration.BinPaths"/> Model and ModelDependent files
        /// SHA1 is the same as the one in <see cref="SetupContext.SignatureFileName"/>.
        /// </summary>
        None,

        /// <summary>
        /// The engine is called even if <see cref="SetupConfiguration.BinPaths"/> Model and ModelDependent files
        /// are the same as the previous run.
        /// <para>
        /// If an already existing working directory folder can be found with the signature, it is used instead of being
        /// recreated.
        /// </para>
        /// </summary>
        Engine,

        /// <summary>
        /// Forces a full setup. Even if the <see cref="SetupConfiguration.BinPaths"/> Model and ModelDependent files
        /// are the same as the previous run, a new working folder is created, setup dependencies are resolved
        /// and engine is ran.
        /// </summary>
        Full
    }
}
