using CK.Core;
using Microsoft.Extensions.CommandLineUtils;
using System;

namespace CKSetup
{
    class RemoteUriOptions
    {
        public RemoteUriOptions( CommandOption url, CommandOption apiKey )
        {
            UrlOption = url;
            ApiKeyOption = apiKey;
        }

        public CommandOption UrlOption { get; }

        public CommandOption ApiKeyOption { get; }

        public Uri? RemoteUri { get; private set; }

        public string? ApiKey { get; private set; }

        /// <summary>
        /// Initializes this option.
        /// </summary>
        /// <param name="m">The monitor.</param>
        /// <param name="required">True to raise an error is a --remote is not specified.</param>
        /// <returns>True on success, false on error.</returns>
        public bool Initialize( IActivityMonitor m, bool required )
        {
            if( !ComponentDB.TryCreateRemoteUri( UrlOption.Value(), out Uri? u ) )
            {
                m.Error( $"--remote {UrlOption.Value()} is not a valid absolute url." );
                return false;
            }
            if( required && (u == null || u == ComponentDB.NoRemote) )
            {
                m.Error( $"--remote url is required." );
                return false;
            }
            RemoteUri = u;
            ApiKey = ApiKeyOption.Value();
            if( String.IsNullOrEmpty( ApiKey ) ) ApiKey = null;

            m.Info( $"Using remote: {RemoteUri} with" + ApiKey != null ? " an API key." : "out API key." );
            return true;
        }

    }
}
