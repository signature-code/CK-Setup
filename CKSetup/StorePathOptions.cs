using CK.Core;
using Microsoft.Extensions.CommandLineUtils;
using System;

namespace CKSetup
{
    class StorePathOptions
    {
        public StorePathOptions( CommandOption arg )
        {
            StorePathOption = arg;
            StorePath = null!;
        }

        public CommandOption StorePathOption { get; }

        public string StorePath { get; private set; }
        
        /// <summary>
        /// Initializes this option.
        /// </summary>
        /// <param name="m">The monitor.</param>
        /// <returns>True on success, false on error.</returns>
        public bool Initialize( IActivityMonitor m )
        {
            StorePath = Facade.LocateStorePath( StorePathOption.Value() ?? "%CKSETUP_STORE%", Environment.CurrentDirectory, true, m );
            return true;
        }

    }
}
