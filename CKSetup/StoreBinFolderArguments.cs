using Microsoft.Extensions.CommandLineUtils;
using System.Collections.Generic;

namespace CKSetup
{
    class StoreBinFolderArguments
    {
        public StoreBinFolderArguments( CommandArgument arg )
        {
            CommandArgument = arg;
        }

        public CommandArgument CommandArgument { get; }

        public IReadOnlyList<BinFolder>? Folders { get; private set; }
        
        public bool Initialize( ConsoleMonitor m )
        {
            Folders = Facade.ReadBinFolders( m, CommandArgument.Values );
            return Folders != null;
        }

    }
}
