using Microsoft.Extensions.CommandLineUtils;
using System.Diagnostics;

namespace CKSetup
{
    /// <summary>
    /// Run command implementation.
    /// </summary>
    static public class CommandRun
    {
        /// <summary>
        /// Implements CKSetup run command.
        /// </summary>
        /// <param name="c">The application.</param>
        public static void Define( CommandLineApplication c )
        {
            c.FullName = c.Parent.FullName;
            c.Description = "Resolves Model/Setup dependencies across one or more directories, and runs a command in a consolidated folder.";
            c.StandardConfiguration( true );
            ConfigurationPathArgument pathArg = c.ConfigurationPathArgument();
            StorePathOptions storePath = c.AddStorePathOption();
            RemoteUriOptions remoteOpt = c.AddRemoteUriOptions();
            var debugOption = c.Option( "--launchDebug", $"Calls Debugger.Launch() at the very start of the runner process.", CommandOptionType.NoValue );
            var forceSetupOption = c.Option( "--force", $"Runs the full setup, regardless of the signature files that may exist in the configured BinPaths folders.", CommandOptionType.NoValue );
            var forceEngineOption = c.Option( "--forceEngine", $"Runs the engine, regardless of the signature files that may exist in the configured BinPaths folders.", CommandOptionType.NoValue );

            c.OnExecute( monitor =>
            {
                if( !pathArg.Initialize( monitor ) ) return Program.RetCodeError;
                if( !storePath.Initialize( monitor ) ) return Program.RetCodeError;
                if( !remoteOpt.Initialize( monitor, false ) ) return Program.RetCodeError;
                
                using( LocalStore? store = LocalStore.OpenOrCreate( monitor, storePath.StorePath ) )
                {
                    if( store == null ) return Program.RetCodeError;
                    Debug.Assert( pathArg.Configuration != null );
                    return Facade.DoRun( monitor,
                                         store,
                                         pathArg.Configuration,
                                         debugOption.HasValue(),
                                         forceSetupOption.HasValue()
                                            ? ForceSetupLevel.Full
                                            : (forceEngineOption.HasValue()
                                                ? ForceSetupLevel.Engine
                                                : ForceSetupLevel.None),
                                         remoteOpt.RemoteUri,
                                         remoteOpt.ApiKey ) != CKSetupRunResult.Failed
                                            ? Program.RetCodeSuccess
                                            : Program.RetCodeError;
                }
            } );
        }
    }
}
