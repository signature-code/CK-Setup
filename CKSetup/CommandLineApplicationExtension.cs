using CK.Core;
using CSemVer;
using Microsoft.Extensions.CommandLineUtils;
using System;
using System.Linq;
using System.Reflection;

namespace CKSetup
{
    static class CommandLineApplicationExtension
    {
        public const string LogLevelOptionName = "verbosity";
        public const string LogFileOptionName = "logFile";
        public const string LogFilterDesc = "Valid verbosity levels: \"Off\", \"Release\", \"Monitor\", \"Terse\", \"Verbose\", \"Debug\", or any \"{Group,Line}\" format where Group and Line can be: \"Debug\", \"Trace\", \"Info\", \"Warn\", \"Error\", \"Fatal\", or \"Off\".";

        static readonly InformationalVersion _thisVersion;
        static readonly string _longVersion;
        static readonly string _shortVersion;

        // There can be at most one console monitor creation during a run.
        static bool _consoleMonitorCreated;

        static CommandLineApplicationExtension()
        {
            var a = (AssemblyInformationalVersionAttribute?)Attribute.GetCustomAttribute( Assembly.GetExecutingAssembly(), typeof( AssemblyInformationalVersionAttribute ) );
            _thisVersion = new InformationalVersion( a?.InformationalVersion );
            _longVersion = _thisVersion.ToString();
            _shortVersion = _thisVersion.Version?.NormalizedText ?? "<No valid version>";
        }

        static public void StandardConfiguration( this CommandLineApplication @this, bool withMonitor )
        {
            @this.LongVersionGetter = () => _longVersion;
            @this.ShortVersionGetter = () => _shortVersion;
            @this.HelpOption( "-?|-h|--help" );
            @this.VersionOption( "--version", @this.LongVersionGetter() );
            if( withMonitor )
            {
                @this.Option( $"-v|--{LogLevelOptionName}", 
                              $"Sets a verbosity level for console and/or file output. {LogFilterDesc}", 
                              CommandOptionType.SingleValue );

                @this.Option( $"-l|--{LogFileOptionName}",
                              $"Path of a log file which will contain the log output. Defaults to none (console logging only or determined by the Setup configuration).",
                              CommandOptionType.SingleValue );
            }
        }


        static public ConfigurationPathArgument ConfigurationPathArgument( this CommandLineApplication @this )
        {
            return new ConfigurationPathArgument( @this.Argument( "ConfigurationPath",
                                                 "Xml configuration to use.",
                                                 false ) );
        }

        static public StoreBinFolderArguments AddStoreDirArguments( this CommandLineApplication @this, string description )
        {
            return new StoreBinFolderArguments( @this.Argument( "BinFolders",
                                               description,
                                               true ) );
        }

        static public BinPathsOption AddBinPathsOption( this CommandLineApplication @this, string description )
        {
            return new BinPathsOption( @this.Option(
                                      "-p|--binPath",
                                      description,
                                      CommandOptionType.MultipleValue ) );
        }

        static public StorePathOptions AddStorePathOption( this CommandLineApplication @this )
        {
            return new StorePathOptions( @this.Option(
                                         "--store",
                                         "Full path of the runtime store directory to use. Defaults to '%CKSETUP_STORE%' and is the result of the following process:" + Environment.NewLine
                                         + Environment.NewLine
                                         + " - The environment variable 'CKSETUP_STORE' like any other environment variables is expanded." + Environment.NewLine
                                         + "   If the expansion of the environment variables of this string leads to a path with invalid characters in it this is an error," + Environment.NewLine
                                         + "   but if the resuting path still contains a '%', this is just a warning (a referenced environment variable is missing)." + Environment.NewLine
                                         + Environment.NewLine
                                         + " - When the step above failed to resolve a path, a 'CKSetupStore.txt' file (that must contain the final store path)" + Environment.NewLine
                                         +$"   is searched from the current directory up to the root. (Current directory is '{Environment.CurrentDirectory}'.)" + Environment.NewLine
                                         + Environment.NewLine
                                         + " - When the step above failed to resolve a path, then the same lookup of a 'CKSetupStore.txt' file is done from the " + Environment.NewLine
                                         +$"   current AppContext.BaseDirectory up to the root. (AppContext.BaseDirectory is '{AppContext.BaseDirectory}'.)" + Environment.NewLine
                                         + Environment.NewLine
                                         +$" - Ultimately, the shared local store is used (that is '{Facade.DefaultStorePath}')." + Environment.NewLine,
                                         CommandOptionType.SingleValue ) );
        }

        static public RemoteUriOptions AddRemoteUriOptions( this CommandLineApplication @this, string? message = null )
        {
            return new RemoteUriOptions( @this.Option( 
                                            "-r|--remote",
                                            message ?? $"Url of the remote store. This uses the ambient store if it exists: use \"no\" or \"none\" to skip ambient store lookup.",
                                            CommandOptionType.SingleValue ),
                                            @this.Option(
                                                "-k|--apiKey",
                                                $"Api key for the remote.",
                                                CommandOptionType.SingleValue ) );
        }

        /// <summary>
        /// Creates the console monitor.
        /// The verbosity (-v log level) is set as the MinimalFilter of the console client (ColoredActivityMonitorConsoleClient).
        /// This must be called once and only once.
        /// </summary>
        /// <param name="this">This application.</param>
        /// <returns>The monitor to use.</returns>
        static public ConsoleMonitor CreateConsoleMonitor( this CommandLineApplication @this )
        {
            Throw.CheckState( "CreateConsoleMonitor must be called at most once.", !_consoleMonitorCreated );
            _consoleMonitorCreated = true;
            CommandOption? verbosity = @this.Options.FirstOrDefault( o => o.LongName == LogLevelOptionName );
            CommandOption? logFile = @this.Options.FirstOrDefault( o => o.LongName == LogFileOptionName );
            return new ConsoleMonitor( @this, verbosity?.Value(), logFile?.Value() );
        }

        static public void OnExecute( this CommandLineApplication @this, Func<ConsoleMonitor,int> invoke )
        {
            @this.OnExecute( () =>
            {
                using( var monitor = @this.CreateConsoleMonitor() )
                {
                    try
                    {
                        int r = invoke( monitor );
                        if( r == Program.RetCodeSuccess ) monitor.Info( $"Command {@this.Name} succeed." );
                        return r;
                    }
                    catch( Exception ex )
                    {
                        monitor.Fatal( $"Command {@this.Name} failed.", ex );
                        return Program.RetCodeError;
                    }
                }
            } );
        }

    }
}
