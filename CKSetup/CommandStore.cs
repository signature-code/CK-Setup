using CK.Core;
using Microsoft.Extensions.CommandLineUtils;
using System;
using System.Diagnostics;

namespace CKSetup
{
    static partial class CommandStore
    {
        public static void Define( CommandLineApplication c )
        {
            c.FullName = c.Parent.FullName + ".Store";
            c.Description = "Manages the runtime store.";
            c.StandardConfiguration( withMonitor: false );
            c.OnExecute( () => { c.ShowHelp(); return Program.RetCodeHelp; } );
            c.Command( "add", DefineAdd );
            c.Command( "clear", DefineClear );
            c.Command( "push", DefinePush );
            c.Command( "prototype", DefinePrototype );
        }

        public static void DefineAdd( CommandLineApplication c )
        {
            c.FullName = c.Parent.FullName + ".Add";
            c.Description = "Adds components (Engine, Runtime or Model) to the store.";
            c.StandardConfiguration( true );

            StoreBinFolderArguments toAdd = c.AddStoreDirArguments( "Components to add to the store." );
            StorePathOptions storePath = c.AddStorePathOption();
            RemoteUriOptions remoteOpt = c.AddRemoteUriOptions();

            c.OnExecute( monitor =>
            {
                if( !toAdd.Initialize( monitor ) ) return Program.RetCodeError;
                if( !storePath.Initialize( monitor ) ) return Program.RetCodeError;
                if( !remoteOpt.Initialize( monitor, false ) ) return Program.RetCodeError;
                using( LocalStore? store = LocalStore.OpenOrCreate( monitor, storePath.StorePath ) )
                {
                    if( store == null ) return Program.RetCodeError;
                    using( IRemoteStore? remote = store.CreateStandardRemote( monitor, remoteOpt.RemoteUri, remoteOpt.ApiKey ) )
                    {
                        Debug.Assert( toAdd.Folders != null );
                        if( !store.CreateLocalImporter( remote ).AddComponent( toAdd.Folders ).Import() )
                        {
                            return Program.RetCodeError;
                        }
                    }
                }
                return Program.RetCodeSuccess;
            } );
        }

        static void DefineClear( CommandLineApplication c )
        {
            c.FullName = c.Parent.FullName + ".Clear";
            c.Description = "Clears the store.";
            c.StandardConfiguration( true );
            StorePathOptions storePath = c.AddStorePathOption();

            c.OnExecute( monitor =>
            {
                if( !storePath.Initialize( monitor ) ) return Program.RetCodeError;
                using( LocalStore? store = LocalStore.OpenOrCreate( monitor, storePath.StorePath ) )
                {
                    if( store == null || !store.Clear() ) return Program.RetCodeError;
                }
                return Program.RetCodeSuccess;
            } );
        }

        static void DefinePush( CommandLineApplication c )
        {
            c.FullName = c.Parent.FullName + ".Push";
            c.Description = "Push local components to a remote store.";
            c.StandardConfiguration( true );
            StorePathOptions storePath = c.AddStorePathOption();
            RemoteUriOptions remoteOpt = c.AddRemoteUriOptions( "The required target url." );

            c.OnExecute( monitor =>
            {
                if( !storePath.Initialize( monitor ) ) return Program.RetCodeError;
                if( !remoteOpt.Initialize( monitor, required: true ) ) return Program.RetCodeError;
                using( LocalStore? store = LocalStore.OpenOrCreate( monitor, storePath.StorePath ) )
                {
                    if( store == null ) return Program.RetCodeError;
                    Debug.Assert( remoteOpt.RemoteUri != null );
                    if( !store.PushComponents( comp => true, remoteOpt.RemoteUri, remoteOpt.ApiKey ) ) return Program.RetCodeError;
                }
                return Program.RetCodeSuccess;
            } );
        }

        static void DefinePrototype( CommandLineApplication c )
        {
            c.FullName = c.Parent.FullName + ".Prototype";
            c.Description = "Gets or sets the store prototype Url store.";
            c.StandardConfiguration( true );
            CommandOption proto = c.Option( "-p|--prototype",
                                            $"Url of the prototype store. Use 'none' to remove current property store. Note that it can be a rooted file path to a local store.",
                                            CommandOptionType.SingleValue );
            StorePathOptions storePath = c.AddStorePathOption();

            c.OnExecute( monitor =>
            {
                if( !storePath.Initialize( monitor ) ) return Program.RetCodeError;
                using( LocalStore? store = LocalStore.OpenOrCreate( monitor, storePath.StorePath ) )
                {
                    if( store == null ) return Program.RetCodeError;
                    if( proto.HasValue() )
                    {
                        Uri? protoUrl;
                        if( proto.Value() == "'none'" || proto.Value() == "none" )
                        {
                            protoUrl = null;
                        }
                        else if( !Uri.TryCreate( proto.Value(), UriKind.Absolute, out protoUrl ) )
                        {
                            monitor.Error( $"--prototype {proto.Value()} is not a valid absolute url or local path." );
                            return Program.RetCodeError;
                        }
                        store.PrototypeStoreUrl = protoUrl ?? ComponentDB.NoRemote;
                    }
                    Console.WriteLine( $"Current Store Prototype Url: {store.PrototypeStoreUrl}" );
                }
                return Program.RetCodeSuccess;
            } );
        }

    }
}
