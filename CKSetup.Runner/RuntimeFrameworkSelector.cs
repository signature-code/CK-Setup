#if !NET461

#if NETCOREAPP3_0 || NETCOREAPP3_1 || NET6_0
// Newtonsoft is not automatically available on .NET Core 3. System.Text.Json is.
#define USE_SYSTEM_TEXT
#else
#define USE_NEWTONSOFT
#endif


using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using CK.Core;
#if USE_NEWTONSOFT
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
#endif
#if USE_SYSTEM_TEXT
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Linq;
#endif

namespace CKSetup.Runner
{
    public class RuntimeFrameworkSelector
    {
        readonly List<FrameworkCandidate> _candidates;
#if USE_SYSTEM_TEXT
        bool _hasIncludedFramework;
#endif
        bool _hasError;

        public RuntimeFrameworkSelector()
        {
            _candidates = new List<FrameworkCandidate>();
        }

        public static void MergeRuntimeConfig( string folder, IActivityMonitor m )
        {
            using( m.OpenLog( LogLevel.Info, "Selecting shared framework from .runtimeconfig.json files." ) )
            {
                // Include CKSetup.Runner.runtimeconfig.json this time.
                string runnerConfigPath = Path.Combine( folder, "CKSetup.Runner.runtimeconfig.json" );
                if( !File.Exists( runnerConfigPath ) )
                {
                    throw new FileNotFoundException( "CKSetup.Runner.runtimeconfig.json file not found", runnerConfigPath );
                }
                // The .runtimeconfig.json.merged will be swapped with the original .runtimeconfig.json
                // in CKSetup.Facade.RunSetupRunner().
                string runnerConfigTargetPath = Path.Combine( folder, "CKSetup.Runner.runtimeconfig.json.merged" );

                var s = new RuntimeFrameworkSelector();
                // Lexical ordering to be determinisitic - no LINQ in netcoreapp2.1 and net461.
                var files = new List<string>( Directory.EnumerateFiles( folder, "*.runtimeconfig.json" ) );
                files.Sort();
                foreach( var f in files )
                {
                    if( f != runnerConfigPath ) s.AddRuntimeConfigFile( m, f );
                }
                // Adds the CKSetup.Runner.runtimeconfig.json last.
                s.AddRuntimeConfigFile( m, runnerConfigPath );
                s.ReplaceFramework( m, runnerConfigPath, runnerConfigTargetPath );
            }
        }

        void AddRuntimeConfigFile( IActivityMonitor m, string path )
        {
            string fileName = Path.GetFileName( path );

            bool AddCandidate( string frameworkName, string frameworkVersion )
            {
                Version v;
                try
                {
                    v = Version.Parse( frameworkVersion );
                    _candidates.Add( new FrameworkCandidate( fileName, frameworkName, v ) );
                    return true;
                }
                catch( Exception ex )
                {
                    m.Log( LogLevel.Error, $"Unable to parse version '{frameworkVersion}'.", ex );
                    _hasError = true;
                    return false;
                }
            }

            int nbRead = 0;
            using( m.OpenLog( LogLevel.Info, $"Reading file '{fileName}'." ) )
            {
                try
                {
                    using( StreamReader file = File.OpenText( path ) )
                    {
#if USE_SYSTEM_TEXT
                        bool Add( JsonElement e ) => AddCandidate( e.GetProperty( "name" ).GetString(), e.GetProperty( "version" ).GetString() );

                        void SetIncludedFramework()
                        {
                            if( !_hasIncludedFramework )
                            {
                                m.Log( LogLevel.Info, $"'includedFramework' detected: switching to Self-contained deployment." );
                                _hasIncludedFramework = true;
                            }
                        }

                        using( var doc = JsonDocument.Parse( file.ReadToEnd() ) )
                        {
                            if( !doc.RootElement.TryGetProperty( "runtimeOptions", out var options ) )
                            {
                                m.Log( LogLevel.Warn, $"No 'runtimeOptions' object found." );
                            }
                            else
                            {
                                bool isFramework = options.TryGetProperty( "framework", out var framework );
                                bool isIncludedFramework = !isFramework && options.TryGetProperty( "includedFramework", out framework );
                                if( isFramework || isIncludedFramework )
                                {
                                    if( Add( framework ) ) ++nbRead;
                                }
                                else
                                {
                                    isFramework = options.TryGetProperty( "frameworks", out var frameworks );
                                    isIncludedFramework = !isFramework && options.TryGetProperty( "includedFrameworks", out frameworks );
                                    if( isFramework || isIncludedFramework )
                                    {
                                        foreach( var f in frameworks.EnumerateArray() )
                                        {
                                            if( Add( f ) ) ++nbRead;
                                        }
                                    }
                                    else
                                    {
                                        m.Log( LogLevel.Warn, $"No 'framework', 'frameworks' or 'includedFrameworks' objects found." );
                                    }
                                    if( isIncludedFramework ) SetIncludedFramework();
                                }
                            }
                        }
#endif
#if USE_NEWTONSOFT
                    using( JsonTextReader reader = new JsonTextReader( file ) )
                    {
                        var token = JToken.ReadFrom( reader );
                        if( token is JObject root )
                        {
                            if( root.Property( "runtimeOptions" )?.Value is JObject runtimeOptions
                                && runtimeOptions.Property( "framework" )?.Value is JObject framework )
                            {
                                string frameworkName = null, frameworkVersion = null;
                                if( framework.Property( "name" )?.Value is JValue name )
                                {
                                    frameworkName = name.Value as string;
                                    // Not present in SCD .runtimeconfig.json files
                                }

                                if( framework.Property( "version" )?.Value is JValue version )
                                {
                                    frameworkVersion = version.Value as string;
                                    // Not present in SCD .runtimeconfig.json files
                                }
                                if( frameworkName != null && frameworkVersion != null )
                                {
                                    if( AddCandidate( frameworkName, frameworkVersion ) ) ++nbRead;
                                }
                            }
                            else
                            {
                                m.Log( LogLevel.Warn, $"No object runtimeOptions/framework." );
                            }
                        }
                        else
                        {
                            m.Log( LogLevel.Error, $"Failed to read RuntimeConfig: missing root object." );
                            _hasError = true;
                        }
                    }
#endif
                    }
                }
                catch( Exception ex )
                {
                    m.Log( LogLevel.Error, $"Failed to read file.", ex );
                    _hasError = true;
                }
                m.CloseGroup( $"{nbRead} framework name(s) read." );
            }
        }

        IReadOnlyCollection<FrameworkCandidate> Unify( IActivityMonitor m )
        {
            if( _hasError ) return null;
            if( _candidates.Count == 0 )
            {
                m.Log( LogLevel.Error, "No framework found in *.runtimeconfig.json files." );
                return null;
            }
            else if( _candidates.Count == 1 ) return _candidates;
            var dedup = new Dictionary<string, FrameworkCandidate>();
            foreach( var c in _candidates )
            {
                if( !dedup.TryGetValue( c.FrameworkName, out var current ) )
                {
                    dedup.Add( c.FrameworkName, c );
                }
                else
                {
                    if( current.FrameworkVersion != c.FrameworkVersion )
                    {
                        if( current.FrameworkVersion < c.FrameworkVersion ) dedup[c.FrameworkName] = c;
                        m.Log( LogLevel.Warn, $"Version differ between {current} and {c}." );
                    }
                }
            }
            return dedup.Values;
        }

        public readonly struct FrameworkCandidate
        {
            public FrameworkCandidate( string fName, string frameworkName, Version v )
            {
                OriginFileName = fName;
                FrameworkName = frameworkName;
                FrameworkVersion = v;
            }

            public string OriginFileName { get; }
            public string FrameworkName { get; }
            public Version FrameworkVersion { get; }
            public override string ToString() => $"{FrameworkName}: {FrameworkVersion} in '{OriginFileName}'.";
        }

        public void ReplaceFramework( IActivityMonitor m, string runnerConfigPath, string runnerConfigTargetPath )
        {
            var unified = Unify( m );
            if( unified == null )
            {
                throw new CKException( "Stop processing because of previous errors." );
            }
#if USE_SYSTEM_TEXT

            void WriteFrameworkObject( Utf8JsonWriter writer, FrameworkCandidate c )
            {
                writer.WriteStartObject();
                writer.WritePropertyName( "name" );
                writer.WriteStringValue( c.FrameworkName );
                writer.WritePropertyName( "version" );
                writer.WriteStringValue( c.FrameworkVersion.ToString() );
                writer.WriteEndObject();
            }

            using( Stream file = File.Open( runnerConfigPath, FileMode.Open, FileAccess.Read, FileShare.Read ) )
            using( JsonDocument jsonDoc = JsonDocument.Parse( file ) )
            {
                using( Stream targetfile = File.Open( runnerConfigTargetPath, FileMode.Create, FileAccess.Write, FileShare.Read ) )
                using( Utf8JsonWriter writer = new Utf8JsonWriter( targetfile, new JsonWriterOptions { Indented = true } ) )
                {
                    writer.WriteStartObject();
                    foreach( var obj in jsonDoc.RootElement.EnumerateObject() )
                    {
                        if( obj.Name != "runtimeOptions" )
                        {
                            obj.WriteTo( writer );
                            continue;
                        }
                        writer.WritePropertyName( obj.Name );
                        writer.WriteStartObject();
                        foreach( var objRun in obj.Value.EnumerateObject() )
                        {
                            // We are on the CKSetup.Runner config: it has one and only one "framework".
                            if( objRun.Name != "framework" )
                            {
                                objRun.WriteTo( writer );
                                continue;
                            }
                            if( unified.Count == 1 )
                            {
                                writer.WritePropertyName( "framework" );
                                WriteFrameworkObject( writer, unified.First() );
                            }
                            else
                            {
                                writer.WritePropertyName( "frameworks" );
                                writer.WriteStartArray();
                                foreach( var c in unified )
                                {
                                    WriteFrameworkObject( writer, c );
                                }
                                writer.WriteEndArray();
                            }
                        }
                        writer.WriteEndObject();
                    }
                    writer.WriteEndObject();
                }
            }
#endif
#if USE_NEWTONSOFT
            // No Linq available in NetCore 2.
            int FrameworkPower( FrameworkCandidate c ) => c.FrameworkName switch
            {
                "Microsoft.AspNetCore.All" => 3,
                "Microsoft.AspNetCore.App" => 2,
                "Microsoft.NETCore.App" => 1,
                _ => throw new Exception( $"Invalid framework in NetCore 2: {c}" )
            };

            int currentPower = 0;
            Version max = null;
            string name = null;
            foreach( var c in unified )
            {
                var p = FrameworkPower( c );
                if( p > currentPower )
                {
                    currentPower = p;
                    if( name != null ) m.Log( LogLevel.Trace, $"Framework '{name}' replaced by '{c.FrameworkName}'." );
                    name = c.FrameworkName;
                }
                if( max == null || max < c.FrameworkVersion )
                {
                    if( max != null ) m.Log( LogLevel.Warn, $"Framework version '{max}' replaced by '{c.FrameworkVersion}'." );
                    max = c.FrameworkVersion;
                }
            }

            // Load JObject
            JObject o;
            using( StreamReader file = File.OpenText( runnerConfigPath ) )
            using( JsonTextReader reader = new JsonTextReader( file ) )
            {
                o = (JObject)JToken.ReadFrom( reader );
            }

            // Edit in-place
            o["runtimeOptions"]["framework"]["name"] = name;
            o["runtimeOptions"]["framework"]["version"] = max.ToString();
            m.Log( LogLevel.Info, $"Using Framework '{name}' in '{max}' version." );

            // Save
            using( var fs = File.Open( runnerConfigTargetPath, FileMode.Create, FileAccess.Write, FileShare.Read ) )
            using( var sw = new StreamWriter( fs, Encoding.UTF8, 4096, true ) )
            using( JsonTextWriter writer = new JsonTextWriter( sw ) )
            {
                // Defaults from runtimeconfig.json files
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 2;
                writer.IndentChar = ' ';

                o.WriteTo( writer );
            }
#endif
        }
    }
}

#endif
