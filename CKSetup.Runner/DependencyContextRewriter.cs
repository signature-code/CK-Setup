#if !NET461
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using CK.Core;
using Microsoft.Extensions.DependencyModel;

namespace CKSetup.Runner
{
    /// <summary>
    /// Mashes a number of dependency contexts from different build contexts together.
    /// For .NET Core only.
    /// <para>
    /// This is obviously not supported by supported by .NET Core:
    /// The resulting dependency context will only work as long as assemblies are compatible.
    /// Beware of SystemExceptions like MissingMethodException down the line.
    /// </para>
    /// </summary>
    class DependencyContextRewriter : IDisposable
    {
        private readonly DependencyContextJsonReader _dependencyContextJsonReader;
        private readonly List<DependencyContextCandidate> _candidates;
        private readonly Dictionary<string, List<DependencyContextCandidate>> _candidatesByRuntimeLibrary;
        private readonly Dictionary<string, List<DependencyContextCandidate>> _candidatesByCompileLibrary;

        public DependencyContextRewriter()
        {
            _dependencyContextJsonReader = new DependencyContextJsonReader();
            _candidates = new List<DependencyContextCandidate>();
            // OrdinalIgnoreCase seems to be the canonical comparison.
            // See https://github.com/dotnet/core-setup/blob/release/2.2/src/managed/Microsoft.Extensions.DependencyModel/DependencyContext.cs#L103
            _candidatesByRuntimeLibrary = new Dictionary<string, List<DependencyContextCandidate>>( StringComparer.OrdinalIgnoreCase );
            _candidatesByCompileLibrary = new Dictionary<string, List<DependencyContextCandidate>>( StringComparer.OrdinalIgnoreCase );
        }

        public void AddDependencyContext( IActivityMonitor m, string depsJsonPath, string name )
        {
            try
            {
                if( _candidates.Any( c => c.Name == name ) )
                {
                    throw new InvalidOperationException( $"DependencyContext {name} was already added." );
                }
                DependencyContext dctx;
                using( var content = File.OpenRead( depsJsonPath ) )
                {
                    dctx = _dependencyContextJsonReader.Read( content );
                    m.Log( LogLevel.Info, $"Loaded DependencyContext from \"{name}\" (Framework: {dctx.Target.Framework})." );
                }
                RegisterCandidate( new DependencyContextCandidate( name, dctx ) );
            }
            catch( Exception ex )
            {
                m.Log( LogLevel.Error, $"Failed to load DependencyContext from \"{name}\".", ex );
            }
        }

        private void RegisterCandidate( DependencyContextCandidate candidate )
        {
            _candidates.Add( candidate );
            foreach( var compileLibrary in candidate.DependencyContext.CompileLibraries )
            {
                if( !_candidatesByCompileLibrary.TryGetValue( compileLibrary.Name, out var candidateList ) )
                {
                    candidateList = new List<DependencyContextCandidate>();
                    _candidatesByCompileLibrary.Add( compileLibrary.Name, candidateList );
                }
                candidateList.Add( candidate );
            }
            foreach( var runtimeLibrary in candidate.DependencyContext.RuntimeLibraries )
            {
                if( !_candidatesByRuntimeLibrary.TryGetValue( runtimeLibrary.Name, out var candidateList ) )
                {
                    candidateList = new List<DependencyContextCandidate>();
                    _candidatesByRuntimeLibrary.Add( runtimeLibrary.Name, candidateList );
                }
                candidateList.Add( candidate );
            }
        }


        /// <summary>
        /// Builds a DependencyContext using all contexts added with <see cref="AddDependencyContext"/>.
        /// </summary>
        public DependencyContext Build( IActivityMonitor m )
        {
            m.Log( LogLevel.Info, $"Merging {_candidates.Count} dependency contexts." );
            // Simple Merge, before dep checking
            var context = ReorderAndMergeContexts( m );

            // Remove SDK libs
            context = RemoveImplicitLibraries( context );

            // Check and fix versions and deps
            context = FixCompileRuntimeConflicts( m, context );

            DumpContextToMonitor( m, context );

            return context;
        }

        private static DependencyContext RemoveImplicitLibraries( DependencyContext c )
        {
            // Implicitly referenced by standard host, added by Sdk.Web.
            var compileLibNamesToRemove = new[]
            {
                "Microsoft.NETCore.DotNetAppHost",
                "Microsoft.NETCore.DotNetHostResolver",
                "NETStandard.Library"
            };
            return new DependencyContext(
                c.Target,
                c.CompilationOptions,
                c.CompileLibraries.Where( l => !compileLibNamesToRemove.Contains( l.Name, StringComparer.InvariantCultureIgnoreCase ) ),
                c.RuntimeLibraries.Where( l => !l.Name.StartsWith( "Microsoft.NETCore.App.Runtime.", StringComparison.InvariantCultureIgnoreCase ) ),
                c.RuntimeGraph );
        }

        /// <summary>
        /// Compares DependencyContext candidates to order by TargetFramework, then by RuntimeLibraries Count, then by RuntimeGraph.
        /// </summary>
        /// <remarks>
        /// This seems completely stupid... but ".NetCoreApp..." < ".NetStandard..." (and that's
        /// what we are looking for right now) then we try to privilegiate "small" dependencies
        /// (so that the order has a chance to follow the actual dependency order) and ultimately
        /// we ensure deterministic order (based on the deps.json file name).
        /// This may require more work (".NETCoreApp,Version=v2.0" vs. ".NETCoreApp,Version=v2.1"?)
        /// in the future.
        /// </remarks>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
        static int CompareTargetFramework( DependencyContextCandidate c1, DependencyContextCandidate c2 )
        {
            int cmp = String.Compare( c1.DependencyContext.Target.Framework, c2.DependencyContext.Target.Framework, StringComparison.OrdinalIgnoreCase );
            if( cmp != 0 ) return cmp;
            // Prefer deps.json with a small number of RuntimeLibraries.
            cmp = c1.DependencyContext.RuntimeLibraries.Count - c2.DependencyContext.RuntimeLibraries.Count;
            if( cmp != 0 ) return cmp;
            // Prefer deps.json with little RuntimeGraph.
            cmp = c1.DependencyContext.RuntimeGraph.Count - c2.DependencyContext.RuntimeGraph.Count;
            if( cmp != 0 ) return cmp;
            return String.Compare( c1.Name, c2.Name, StringComparison.OrdinalIgnoreCase );
        }

        /// <summary>
        /// Merges dependency contexts after ordering them by TargetFramework.
        /// <para>The resulting context might still have issues!</para>
        /// </summary>
        /// <returns></returns>
        private DependencyContext ReorderAndMergeContexts( IActivityMonitor m )
        {
            _candidates.Sort( CompareTargetFramework ); // Sort list in-place

            DependencyContext c = DependencyContext.Default;

            // Merge all candidates inside the default context, in order.
            // Note that DependencyContext.Merge() simply creates a new DependencyContext with some Unions on libraries.
            // If there are conflicting libraries, the first one that was merged will be kept.
            // See: https://github.com/dotnet/core-setup/blob/release/2.2/src/managed/Microsoft.Extensions.DependencyModel/DependencyContext.cs#L65
            foreach( var d in _candidates )
            {
                m.Log( LogLevel.Info, $"Merging DependencyContext: \"{d.Name}\" (Framework: {d.DependencyContext.Target.Framework})." );
                c = c.Merge( d.DependencyContext );
            }

            return c;
        }

        private static void DumpContextToMonitor( IActivityMonitor m, DependencyContext c )
        {
            using( m.OpenLog( LogLevel.Debug, "Final Dependency Context" ) )
            {
                var b = new StringBuilder();
                b.Append( "CompilationOptions:" ).AppendLine()
                    .Append( "   Platform: " ).Append( c.CompilationOptions.Platform ).AppendLine()
                    .Append( "   LanguageVersion: " ).Append( c.CompilationOptions.LanguageVersion ).AppendLine()
                    .Append( "Target:" ).AppendLine()
                    .Append( "   Framework: " ).Append( c.Target.Framework ).AppendLine()
                    .Append( "   IsPortable: " ).Append( c.Target.IsPortable ).AppendLine()
                    .Append( "   Runtime: " ).Append( c.Target.Runtime ).AppendLine()
                    .Append( "   RuntimeSignature: " ).Append( c.Target.RuntimeSignature ).AppendLine()
                    .Append( "CompileLibraries:" ).AppendLine();
                foreach( var library in c.CompileLibraries.OrderBy( x => x.Name, StringComparer.InvariantCultureIgnoreCase ) )
                {
                    b.AppendLine( $"  {library.Name}: {library.Version} at {library.Path}" );
                    DumpDeps( b, library.Dependencies );
                }
                b.Append( "RuntimeLibraries:" ).AppendLine();
                foreach( var library in c.RuntimeLibraries.OrderBy( x => x.Name, StringComparer.InvariantCultureIgnoreCase ) )
                {
                    b.AppendLine( $"  {library.Name}: {library.Version} at {library.Path}" );
                }
                m.Log( LogLevel.Debug, b.ToString() );
            }
        }

        private static void DumpDeps( StringBuilder b, IReadOnlyList<Dependency> deps )
        {
            foreach( var dep in deps )
            {
                b.AppendLine( $"    Depends on: {dep.Name} {dep.Version}" );
            }
        }

        /// <summary>
        /// Removes libraries from CompileLibraries if they have a conflicting RuntimeLibrary.
        /// This also rewrites the libraries ot make sure dependencies are targeting correct versions.
        /// </summary>
        /// <returns></returns>
        private DependencyContext FixCompileRuntimeConflicts( IActivityMonitor m, DependencyContext c )
        {
            IEnumerable<string> compileNames = c.CompileLibraries.Select( x => x.Name );
            IEnumerable<string> runtimeNames = c.RuntimeLibraries.Select( x => x.Name );
            IEnumerable<string> commonLibraryNames = compileNames.Intersect( runtimeNames ).ToList();

            Dictionary<string, RuntimeLibrary> keptRuntimeLibs = new Dictionary<string, RuntimeLibrary>( StringComparer.OrdinalIgnoreCase );

            if( commonLibraryNames.Any() )
            {
                List<string> compileLibsToRemove = new List<string>();
                foreach( var libraryName in commonLibraryNames )
                {
                    var compileLib = c.CompileLibraries.First( x => x.Name == libraryName );
                    var runtimeLib = c.RuntimeLibraries.First( x => x.Name == libraryName );
                    if( compileLib.Version != runtimeLib.Version )
                    {
                        using( m.OpenLog( LogLevel.Warn, $"Library '{libraryName}' was merged in both Compile and Runtime libraries with different versions:" ) )
                        {
                            m.Log( LogLevel.Warn, $"(removed) Compile: {compileLib.Name} {compileLib.Version} at {compileLib.Path}" );
                            if( _candidatesByCompileLibrary.TryGetValue( compileLib.Name, out var compileCandidates ) )
                            {
                                foreach( var dc in compileCandidates )
                                {
                                    var cl = dc.DependencyContext.CompileLibraries.First( x => x.Name == compileLib.Name );
                                    m.Log( LogLevel.Warn, $"                   - From {dc.Name}: {cl.Name} {cl.Version}" );
                                }
                            }
                            else
                            {
                                m.Log( LogLevel.Warn, $"                   - (Not present in any DependencyContext CompileLibraries)" );
                            }

                            m.Log( LogLevel.Warn, $"   (kept) Runtime: {runtimeLib.Name} {runtimeLib.Version} at {runtimeLib.Path}" );
                            if( _candidatesByRuntimeLibrary.TryGetValue( runtimeLib.Name, out var runtimeCandidates ) )
                            {
                                foreach( var dc in runtimeCandidates )
                                {
                                    var rl = dc.DependencyContext.RuntimeLibraries.First( x => x.Name == runtimeLib.Name );
                                    m.Log( LogLevel.Warn, $"                   - From {dc.Name}: {rl.Name} {rl.Version}" );
                                }
                            }
                            else
                            {
                                m.Log( LogLevel.Warn, $"                   - (Not present in any DependencyContext RuntimeLibraries)" );
                            }
                        }
                        compileLibsToRemove.Add( libraryName );
                        keptRuntimeLibs.Add( runtimeLib.Name, runtimeLib );
                    }
                }

                if( compileLibsToRemove.Count > 0 )
                {
                    // Rewrite DependencyContext, excluding problematic libraries,
                    // and make sure libraries on both sides are targeting the kept version.
                    c = new DependencyContext(
                        c.Target,
                        c.CompilationOptions,
                        CleanAndRebuildCompileLibraries( c, compileLibsToRemove, keptRuntimeLibs ),
                        RebuildRuntimeLibraries( c, keptRuntimeLibs ),
                        c.RuntimeGraph );
                }
            }

            return c;
        }

        private static IEnumerable<CompilationLibrary> CleanAndRebuildCompileLibraries(
            DependencyContext c,
            IReadOnlyList<string> compileLibsToRemove,
            IReadOnlyDictionary<string, RuntimeLibrary> keptRuntimeLibs
            )
        {
            return c
                .CompileLibraries
                .Where( l => !compileLibsToRemove.Contains( l.Name ) )
                .Select( ( l ) => new CompilationLibrary(
                    l.Type, l.Name, l.Version, l.Hash, l.Assemblies,
                    RewriteLibraryDependencies( l.Dependencies, keptRuntimeLibs ),
                    l.Serviceable ) );
        }

        private static IEnumerable<RuntimeLibrary> RebuildRuntimeLibraries(
            DependencyContext c,
            IReadOnlyDictionary<string, RuntimeLibrary> keptRuntimeLibs
        )
        {
            return c
                .RuntimeLibraries
                .Select( ( l ) => new RuntimeLibrary( l.Type, l.Name, l.Version, l.Hash, l.RuntimeAssemblyGroups,
                    l.NativeLibraryGroups, l.ResourceAssemblies, RewriteLibraryDependencies( l.Dependencies, keptRuntimeLibs ), l.Serviceable ) );
        }

        private static IEnumerable<Dependency> RewriteLibraryDependencies( IReadOnlyList<Dependency> dependencies, IReadOnlyDictionary<string, RuntimeLibrary> keptRuntimeLibs )
        {
            return dependencies.Select( dep =>
            {
                if( keptRuntimeLibs.TryGetValue( dep.Name, out var rtLib ) )
                {
                    return new Dependency( rtLib.Name, rtLib.Version );
                }
                return dep;
            } );
        }

        class DependencyContextCandidate
        {
            public DependencyContextCandidate( string name, DependencyContext dependencyContext )
            {
                Name = name;
                DependencyContext = dependencyContext;
            }

            public string Name { get; }
            public DependencyContext DependencyContext { get; }
        }

        public void Dispose()
        {
            _dependencyContextJsonReader.Dispose();
        }
    }
}
#endif
