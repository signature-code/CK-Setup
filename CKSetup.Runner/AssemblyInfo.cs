[assembly: CK.Setup.IsSetupDependency()]

#if !NET6_0_OR_GREATER
namespace CK.Setup
{
    class IsSetupDependencyAttribute : System.Attribute
    {
    }
}
#endif
