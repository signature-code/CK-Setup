using CK.Core;
using System;

#if NET6_0_OR_GREATER

namespace CKSetup
{
    sealed class StaticGatesConfigurator
    {
        readonly string[] _names;
        readonly bool[] _states;

        public StaticGatesConfigurator( string configuration )
        {
            (_names, _states) = CreateConfig( configuration );
            StaticGate.OnNewStaticGate += DoConfigure;
            foreach( var g in StaticGate.GetStaticGates() )
            {
                DoConfigure( g );
            }
        }

        void DoConfigure( StaticGate g )
        {
            if( g.HasDisplayName )
            {
                int idx = Array.IndexOf( _names, g.DisplayName );
                if( idx >= 0 )
                {
                    g.IsOpen = _states[idx];
                }
            }
        }

        static (string[] Names, bool[] States) CreateConfig( string configuration )
        {
            var e = configuration.Split( ';', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries );
            var n = new string[e.Length];
            var s = new bool[e.Length];
            for( int i = 0; i < e.Length; ++i )
            {
                var name = e[i];
                if( s[i] = name[0] != '!' ) n[i] = name;
                else n[i] = name.Substring( 1 );
            }
            return (n, s);
        }
    }
}

#endif
