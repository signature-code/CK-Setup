using CK.Core;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace CKSetup.Runner
{
    static class ActivityMonitorExtensions
    {
        static public void Log( this IActivityMonitor @this, LogLevel level, string text, Exception ex = null, [CallerFilePath]string fileName = null, [CallerLineNumber]int lineNumber = 0 )
        {
#if NET6_0_OR_GREATER
            @this.UnfilteredLog( level, null, text, ex, fileName, lineNumber );
#else
            @this.UnfilteredLog( null, level, text, @this.NextLogTime(), ex, fileName, lineNumber );
#endif
        }
        static public IDisposable OpenLog( this IActivityMonitor @this, LogLevel level, string text, Exception ex = null, [CallerFilePath]string fileName = null, [CallerLineNumber]int lineNumber = 0 )
        {
#if NET6_0_OR_GREATER
            return @this.UnfilteredOpenGroup( level, null, text, ex, fileName, lineNumber );
#else
            return @this.UnfilteredOpenGroup( null, level, null, text, @this.NextLogTime(), ex, fileName, lineNumber );
#endif
        }
    }
}
