using CK.Core;
using System;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

namespace StupidEngine
{
    public class Engine
    {
        readonly IActivityMonitor _m;
        readonly XElement _config;

#if NET6_0_OR_GREATER

        // The open/closed status is reverted by the RunningStupidEngineTests.run_setup_on_CurrentFramework.

        // This works!
        static readonly StaticGate _demo1;
        static readonly StaticGate _demo2;
        static readonly StaticGate _demo3;

        static Engine()
        {
            _demo1 = new StaticGate( "Demo1", false );
            _demo2 = new StaticGate( "Demo2", true );
            _demo3 = new StaticGate( "Demo3", false );
        }


        // This doesn't work: the constructor sees StaticGate.GetStaticGates(),
        // even StaticGate.TotalCount (interlocked increment) is zero!
        //
        // ...unless one of the static field is touched in the constructor with something
        // like m.Info( $"Touch '{_demo1.DisplayName}'!" );
        //
        //static readonly StaticGate _demo1 = new StaticGate( "Demo1", false );
        //static readonly StaticGate _demo2 = new StaticGate( "Demo2", true );
        //static readonly StaticGate _demo3 = new StaticGate( "Demo3", false );
#endif

        public Engine( IActivityMonitor m, XElement config )
        {
            _m = m;
            _config = config;
            using( m.OpenInfo( $"Initialized new stupid Engine." ) )
            {
#if NET6_0_OR_GREATER
                // This triggers the instantiation of the inlined statics.
                // m.Info( $"Touch '{_demo1.DisplayName}'!" );

                using( m.OpenInfo( $"There is {StaticGate.OpenedCount} opened gates out of {StaticGate.TotalCount}:" ) )
                {
                    foreach( var g in StaticGate.GetStaticGates() )
                    {
                        _m.Info( $"Gate: {g.DisplayName} [{(g.IsOpen ? "Open" : "Clos")}ed]" );
                    }
                }
#endif
            }
        }

        public bool Run()
        {
            using( _m.OpenInfo( $"Running stupid Engine (LogLevel = {_m.MinimalFilter})." ) )
            {
                _m.Info( _config.ToString() );
                _m.Info( $"ResAccessSample.GetText => {StupidModel.ResAccessSample.GetTexts()}" );
            }
            return true;
        }

    }
}
