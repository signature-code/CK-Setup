using System;
using System.Globalization;

namespace StupidModel
{
    public class ResAccessSample
    {
        public static string GetTexts()
        {
            string Sample = SampleResources.Sample;
            var resDE = SampleResources.ResourceManager.GetResourceSet( new CultureInfo( "de" ), true, false );
            if( resDE == null ) throw new Exception( "Unable to load DE resources." );
            string SampleDE = resDE.GetString("Sample");
            if( SampleDE == null ) throw new Exception( "Unable find StupidModel.Sample in DE resources." );
            return $"Sample: {Sample}, DE: {SampleDE}";
        }

    }
}
