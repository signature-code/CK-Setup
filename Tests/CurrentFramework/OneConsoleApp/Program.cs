using System;

namespace OneConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine( $"Hello World! {OneClassLibrary.DemoLib.DemoLibVersion}" );
        }
    }
}
