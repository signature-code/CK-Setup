using CSemVer;
using System;

namespace OneClassLibrary
{
    public class DemoLib
    {
        public static SVersion DemoLibVersion => InformationalVersion.ReadFromAssembly( typeof( DemoLib ).Assembly ).Version!;

        /// <summary>
        /// This is required since without such an explicit usage, the assembly reference to StupidModel is optimized out.
        /// </summary>
        /// <returns></returns>
        public static string GetStupidModelTexts() => StupidModel.ResAccessSample.GetTexts();
    }
}
