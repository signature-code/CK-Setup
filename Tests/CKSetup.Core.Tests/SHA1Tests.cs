using CK.Core;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CK.Testing.CKSetupTestHelper;

namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class SHA1Tests
    {
        static string GetFilePath() => TestHelper.TestProjectFolder.AppendPart( "SHA1Tests.cs" );

        [Test]
        public async Task SHA1_from_file_Async()
        {
            var sha = await SHA1Value.ComputeFileHashAsync( GetFilePath() );
            var sha2 = await SHA1Value.ComputeFileHashAsync( GetFilePath() );
            sha2.Should().Be( sha );

            using( var compressedPath = new TemporaryFile() )
            {
                using( var input = new FileStream( GetFilePath(), FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.SequentialScan | FileOptions.Asynchronous ) )
                using( var compressed = new FileStream( compressedPath.Path, FileMode.Truncate, FileAccess.Write, FileShare.None, 4096, FileOptions.SequentialScan | FileOptions.Asynchronous ) )
                {
                    var writer = StreamStore.StreamStoreExtension.GetCompressShellAsync( w => input.CopyToAsync( w ) );
                    await writer( compressed );
                }
                var shaCompressed = await SHA1Value.ComputeFileHashAsync( compressedPath.Path );
                shaCompressed.Should().NotBe( sha );
                var localSha = await SHA1Value.ComputeFileHashAsync( compressedPath.Path, r => new GZipStream( r, CompressionMode.Decompress, true ) );
                localSha.Should().Be( sha );
            }
        }

        [Test]
        public void SHA1_from_file()
        {
            var sha = SHA1Value.ComputeFileHash( GetFilePath() );
            var sha2 = SHA1Value.ComputeFileHash( GetFilePath() );
            sha2.Should().Be( sha );

            using( var compressedPath = new TemporaryFile() )
            {
                using( var input = new FileStream( GetFilePath(), FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.SequentialScan | FileOptions.Asynchronous ) )
                using( var compressed = new FileStream( compressedPath.Path, FileMode.Truncate, FileAccess.Write, FileShare.None, 4096, FileOptions.SequentialScan | FileOptions.Asynchronous ) )
                {
                    var writer = StreamStore.StreamStoreExtension.GetCompressShell( w => input.CopyTo( w ) );
                    writer( compressed );
                }
                var shaCompressed = SHA1Value.ComputeFileHashAsync( compressedPath.Path );
                shaCompressed.Should().NotBe( sha );
                var localSha = SHA1Value.ComputeFileHash( compressedPath.Path, r => new GZipStream( r, CompressionMode.Decompress, true ) );
                localSha.Should().Be( sha );
            }
        }


    }
}
