using CK.Core;
using CSemVer;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using static CK.Testing.CKSetupTestHelper;

namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class SetupConfigurationTests
    {
        [TestCase( "", "" )]
        [TestCase( "[A|B]", "A;B" )]
        [TestCase( "[A|B]/Pouf/Poetry[Cloud|Dirt]Inside", "A/Pouf/PoetryCloudInside;B/Pouf/PoetryCloudInside;A/Pouf/PoetryDirtInside;B/Pouf/PoetryDirtInside" )]
        [TestCase( "this CK-Build[NetCore2] is unchanged", "this CK-Build[NetCore2] is unchanged" )]
        [TestCase( "this[Before/After] is not an alternative", "this[Before/After] is not an alternative" )]
        public void simple_AlternativePath_analysis( string path, string expectedPaths )
        {
            new AlternativePath( path, path ).Should().BeEquivalentTo( expectedPaths.Split( ';' ) );
        }

        [TestCase( "[Choice|X]", "None", 0, "None" )]
        [TestCase( "[C|X]", "P[C|X] and another time [X|C]", 0, "PC and another time C" )]
        [TestCase( "[C|X1]", "P[C|X1] and another time [X1|C]", 1, "PX1 and another time X1" )]
        [TestCase( "[Ambiguous|Here]-[Ambiguous|Here]", "[Ambiguous|Here] always be the same as [Ambiguous|Here]", 0, "Ambiguous always be the same as Ambiguous" )]
        [TestCase( "[Ambiguous|Here]-[Ambiguous|Here]", "[Ambiguous|Here] always be the same as [Ambiguous|Here]", 1, "Here always be the same as Here" )]
        [TestCase( "[Ambiguous|Here]-[Ambiguous|Here]", "[Ambiguous|Here] always be the same as [Ambiguous|Here]", 2, "Ambiguous always be the same as Ambiguous" )]
        [TestCase( "[Ambiguous|Here]-[Ambiguous|Here]", "[Ambiguous|Here] always be the same as [Ambiguous|Here]", 3, "Here always be the same as Here" )]
        public void AlternativePath_covering_has_ambiguities_but_we_dont_care( string path, string coveredPath, int choice, string coveredResult )
        {
            var a = new AlternativePath( path, path );
            var c = new AlternativePath( coveredPath, coveredPath );
            a.CanCover( c ).Should().BeTrue();
            a.Cover( choice, c ).Should().Be( coveredResult );
        }

        [Test]
        public void choose_among_alternative_paths()
        {
            AlternativePath[] altPaths = new[]
            {
                new AlternativePath( "", "/TTGE.FirstDown.BackOffice.Portal/bin/[Debug|Release]/netcoreapp3.1"),
                new AlternativePath( "", "/TTGE.FirstDown.Viewer.TTGE.Portal/bin/[Release|Debug]/netcoreapp3.1"),
                new AlternativePath( "", "/Moulinettes/TTGE.FirstDown.CLI/bin/Debug/netcoreapp3.1"),
            };
            var main = altPaths[0];
            {
                var alien = altPaths.Skip( 1 ).FirstOrDefault( p => !main.CanCover( in p ) );
                alien.IsValid.Should().BeFalse();
                main.Cover( 0, altPaths[1] ).Should().Be( "/TTGE.FirstDown.Viewer.TTGE.Portal/bin/Debug/netcoreapp3.1" );
                main.Cover( 0, altPaths[2] ).Should().Be( "/Moulinettes/TTGE.FirstDown.CLI/bin/Debug/netcoreapp3.1" );
                main.Cover( 1, altPaths[1] ).Should().Be( "/TTGE.FirstDown.Viewer.TTGE.Portal/bin/Release/netcoreapp3.1" );
                main.Cover( 1, altPaths[2] ).Should().Be( "/Moulinettes/TTGE.FirstDown.CLI/bin/Debug/netcoreapp3.1" );
            }
            altPaths[2] = new AlternativePath( "", "/Moulinettes/TTGE.FirstDown.CLI/bin/[Debug]/netcoreapp3.1" );
            {
                var alien = altPaths.Skip( 1 ).FirstOrDefault( p => !main.CanCover( in p ) );
                alien.IsValid.Should().BeFalse();
                main.Cover( 0, altPaths[1] ).Should().Be( "/TTGE.FirstDown.Viewer.TTGE.Portal/bin/Debug/netcoreapp3.1" );
                main.Cover( 0, altPaths[2] ).Should().Be( "/Moulinettes/TTGE.FirstDown.CLI/bin/[Debug]/netcoreapp3.1", "This is NOT an [Alternative]!" );
                main.Cover( 1, altPaths[1] ).Should().Be( "/TTGE.FirstDown.Viewer.TTGE.Portal/bin/Release/netcoreapp3.1" );
                main.Cover( 1, altPaths[2] ).Should().Be( "/Moulinettes/TTGE.FirstDown.CLI/bin/[Debug]/netcoreapp3.1", "This is NOT an [Alternative]!" );
            }
            altPaths[2] = new AlternativePath( "", "/Moulinettes/TTGE.FirstDown.CLI/bin/[Debug|Other]/netcoreapp3.1" );
            {
                var alien = altPaths.Skip( 1 ).FirstOrDefault( p => !main.CanCover( in p ) );
                alien.IsValid.Should().BeTrue();
            }

        }


        [Test]
        public void SharedConfigurationMode_is_true_by_default()
        {
            var c = new SetupConfiguration();
            CheckEmpty( c, true );
            ConfigureSampleConfiguration( c );
            CheckSampleSharedModeXml( c );
        }

        [Test]
        public void IndependentConfigurationMode_works_the_same()
        {
            var c = new SetupConfiguration( sharedMode: false );
            CheckEmpty( c, false );
            ConfigureSampleConfiguration( c );
            CheckSampleIndependentModeXml( c );
        }

        static void CheckSampleSharedModeXml( SetupConfiguration c )
        {
            Assert.That( c.ForceSetup == ForceSetupLevel.None, "The default for this ForceSetupLevel is to be None." );
            c.ForceSetup = ForceSetupLevel.Engine;

            Assert.That( c.SharedConfigurationMode );
            XElement expected = XElement.Parse( @"
<CKSetup Engine=""Engine"">
    <Dependencies>
        <Dependency Name=""Dep0"" MinVersion=""0.0.0-0"" />
        <Dependency Source=""Test"" Name=""Dep1"" MinVersion=""1.0.0"" />
    </Dependencies>
    <BinPaths>
        <BinPath Path=""P1"" />
        <BinPath Path=""P2"" />
    </BinPaths>
    <BasePath>/Base/Path</BasePath>
    <ExternalSignature>ext-signature</ExternalSignature>
    <PreferredTargetRuntimes>Net462;NetCoreApp11</PreferredTargetRuntimes>
    <WorkingDirectory>/Temp/WorkDir</WorkingDirectory>
    <ForceSetup>Engine</ForceSetup>
</CKSetup>
" );
            var e = c.ToXml();
            e.Root.Should().BeEquivalentTo( expected );

            var eForSign = c.ToXml( forSignature: true );
            expected.Element( "ForceSetup" ).Remove();
            eForSign.Root.Should().BeEquivalentTo( expected );
        }

        static void CheckSampleIndependentModeXml( SetupConfiguration c )
        {
            Assert.That( c.ForceSetup == ForceSetupLevel.None, "The default for this ForceSetup is to be 'None'." );
            c.ForceSetup = ForceSetupLevel.Full;

            Assert.That( !c.SharedConfigurationMode );
            XElement expected = XElement.Parse( @"
<Root>
  <CKSetup>
    <Dependencies>
      <Dependency Name=""Dep0"" MinVersion=""0.0.0-0"" />
      <Dependency Source=""Test"" Name=""Dep1"" MinVersion=""1.0.0"" />
    </Dependencies>
    <BinPaths>
      <BinPath Path=""P1"" />
      <BinPath Path=""P2"" />
    </BinPaths>
    <BasePath>/Base/Path</BasePath>
    <EngineAssemblyQualifiedName>Engine</EngineAssemblyQualifiedName>
    <ExternalSignature>ext-signature</ExternalSignature>
    <PreferredTargetRuntimes>Net462;NetCoreApp11</PreferredTargetRuntimes>
    <WorkingDirectory>/Temp/WorkDir</WorkingDirectory>
    <ForceSetup>Full</ForceSetup>
  </CKSetup>
  <Configuration />
</Root>
" );
            var e = c.ToXml();
            e.Root.Should().BeEquivalentTo( expected );

            var eForSign = c.ToXml( forSignature: true );
            expected.Element( "CKSetup" ).Element( "ForceSetup" ).Remove();
            eForSign.Root.Should().BeEquivalentTo( expected );
        }

        static void ConfigureSampleConfiguration( SetupConfiguration c )
        {
            c.BasePath = "/Base/Path";
            c.BinPaths.Clear();
            c.BinPaths.AddRangeArray( "P1", "P2" );
            c.Dependencies.Clear();
            c.Dependencies.AddRangeArray( new SetupDependency( "Dep0", useMinVersion: SVersion.ZeroVersion ),
                                          new SetupDependency( "Dep1", useMinVersion: SVersion.Parse( "1.0.0" ), sourceName: "Test" ) );
            c.EngineAssemblyQualifiedName = "Engine";
            c.ExternalSignature = "ext-signature";
            c.PreferredTargetRuntimes.Clear();
            c.PreferredTargetRuntimes.AddRangeArray( TargetRuntime.Net462, TargetRuntime.NetCoreApp11 );
            c.ForceSetup = ForceSetupLevel.None;
            c.WorkingDirectory = "/Temp/WorkDir";
        }

        static void CheckEmpty( SetupConfiguration c, bool expectedSharedMode )
        {
            c.SharedConfigurationMode.Should().Be( expectedSharedMode );
            c.BasePath.Should().Be( new NormalizedPath() );
            c.BinPaths.Should().BeEmpty();
            c.Dependencies.Should().BeEmpty();
            c.EngineAssemblyQualifiedName.Should().BeEmpty();
            c.ExternalSignature.Should().BeEmpty();
            c.PreferredTargetRuntimes.Should().BeEmpty();
            c.ForceSetup.Should().Be( ForceSetupLevel.None );
            c.WorkingDirectory.Should().Be( new NormalizedPath() );

            if( !expectedSharedMode )
            {
                c.Configuration.Name.Should().Be( SetupConfiguration.xConfigurationDefaultName );
                c.Configuration.Elements().Should().BeEmpty();
                c.Configuration.Attributes().Should().BeEmpty();
            }
            else
            {
                c.Configuration.Should().BeEquivalentTo( new XElement( "CKSetup", new XElement( "Dependencies" ), new XElement( "BinPaths" ) ) );
            }
        }
    }
}
