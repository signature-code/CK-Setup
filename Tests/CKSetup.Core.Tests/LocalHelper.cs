using CK.Core;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CK.Testing.CKSetupTestHelper;

namespace CKSetup.Core.Tests
{
    static class LocalHelper
    {
        static public NormalizedPath StupidModelNetStandard20 = TestHelper.SolutionFolder.Combine(
                                        $"Tests/StupidModel/bin/{TestHelper.BuildConfiguration}/netstandard2.0" );

        static public NormalizedPath StupidEngineNetCoreApp31 = TestHelper.SolutionFolder.Combine(
                                        $"Tests/StupidEngine/bin/{TestHelper.BuildConfiguration}/netcoreapp3.1" );

        static public NormalizedPath StupidEngineNet60 = TestHelper.SolutionFolder.Combine(
                                        $"Tests/StupidEngine/bin/{TestHelper.BuildConfiguration}/net6.0" );

        static public NormalizedPath RunnerNet60 = TestHelper.SolutionFolder.Combine(
                                        $"CKSetup.Runner/bin/{TestHelper.BuildConfiguration}/net6.0" );

        static public NormalizedPath RunnerNetCoreApp31 = TestHelper.SolutionFolder.Combine(
                                        $"CKSetup.Runner/bin/{TestHelper.BuildConfiguration}/netcoreapp3.1" );


        static public NormalizedPath[] AllComponents = new[]
        {
            StupidModelNetStandard20,
            StupidEngineNet60,
            StupidEngineNetCoreApp31,
            RunnerNet60,
            RunnerNetCoreApp31,
      };

        static public void RegisterAllComponents( string storePath, bool forcePublish = false )
        {
            TestHelper.CKSetup.PublishAndAddComponentFoldersToStore( AllComponents.Select( p => p.ToString() ), forcePublish, storePath )
                .Should().BeTrue();
        }

    }
}
