using CKSetup.Runner;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using static CK.Testing.CKSetupTestHelper;

namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class RunnerTests
    {
        [TestCase( "Basic" )]
        [TestCase( "WithOptions" )]
        [TestCase( "MultipleFrameworks" )]
        public void runtime_config_write( string f )
        {
            var folder = TestHelper.TestProjectFolder.AppendPart( "RuntimeConfigFiles" ).AppendPart( f );
            RuntimeFrameworkSelector.MergeRuntimeConfig( folder, TestHelper.Monitor );
            var r = folder.AppendPart( "CKSetup.Runner.runtimeconfig.json.merged" );
            var e = folder.AppendPart( "Expected.json" );
            var rT = Regex.Replace( File.ReadAllText( r ), "\\s+", String.Empty );
            var rE = Regex.Replace( File.ReadAllText( e ), "\\s+", String.Empty );
            rT.Should().Be( rE );
        }

    }
}
