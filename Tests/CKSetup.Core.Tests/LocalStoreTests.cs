using CK.Core;
using CK.Testing.CKSetup;
using CKSetup.StreamStore;
using CSemVer;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Xml.Linq;
using static CK.Testing.CKSetupTestHelper;

namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class LocalStoreTests
    {
        static string RegisterInStoreWithAllComponents()
        {
            string sourcePath = TestHelper.CKSetup.GetTestStorePath( null, "StoreWithAllComponents" );
            TestHelper.OnlyOnce( () =>
            {
                TestHelper.CKSetup.RemoveComponentsFromStore( c => true, true, sourcePath );
                LocalHelper.RegisterAllComponents( sourcePath );

            }, $"push_to_StoreWithAllComponents_local_remote" );
            return sourcePath;
        }

        public void push_to_a_local_remote()
        {
            string sourcePath = RegisterInStoreWithAllComponents();

            string targetPath = TestHelper.CKSetup.GetCleanTestStorePath();
            // Ensures that the target store is created.
            LocalStore.OpenOrCreate( TestHelper.Monitor, targetPath ).Dispose();

            using( var source = LocalStore.Open( TestHelper.Monitor, sourcePath ) )
            {
                var targetUri = new Uri( targetPath );
                source.PushComponents( c => true, targetUri, null ).Should().BeTrue();
            }
            var sourceContent = new StoreContent( sourcePath );
            var targetContent = new StoreContent( targetPath );
            sourceContent.ShouldBeEquivalentTo( targetContent );
        }

        [Explicit("This cannot work if there is an ambient store!")]
        [Test]
        public void resolving_default_store_path()
        {
            var def = Facade.LocateStorePath( monitor: TestHelper.Monitor );
            def.Should().Be( Facade.DefaultStorePath );

            var tmp = Path.GetTempPath();

            var existing = Environment.GetEnvironmentVariable( "CKSETUP_STORE" );
            Environment.SetEnvironmentVariable( "CKSETUP_STORE", tmp );
            def = Facade.LocateStorePath( monitor: TestHelper.Monitor );
            def.Should().Be( tmp );
            Environment.SetEnvironmentVariable( "CKSETUP_STORE", existing );

            var specified = Path.Combine( tmp, "Spec" );
            def = Facade.LocateStorePath( specified, monitor: TestHelper.Monitor );
            def.Should().Be( specified );

            tmp = Path.Combine( tmp, "Redirected" );
            var redirFile = TestHelper.TestProjectFolder.RemoveLastPart().AppendPart( "CKSetupStore.txt" );
            File.WriteAllText( redirFile, tmp );
            def = Facade.LocateStorePath( monitor: TestHelper.Monitor );
            def.Should().Be( tmp );
            File.Delete( redirFile );
        }

        static bool StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( string path )
        {
            var localContent = new StoreContent( path );
            var components = localContent.ComponentDB.Components;
            if( components.Count == 0 ) return false;
            if( components.Count == 3 )
            {
                // We have 2 CKSetup.Runner (the current build version and the
                // 8.0.1--0026-develop that is the minDependencyVersion of CKSetup.Runner
                // by the required setup dependency of the StupidEngine) plus
                // the StupidEngine itself.
                var v = new InformationalVersion( typeof( Facade ).Assembly
                                                    .GetCustomAttributes( false )
                                                    .OfType<System.Reflection.AssemblyInformationalVersionAttribute>()
                                                    .Single()
                                                    .InformationalVersion );
                components.Any( c => c.Name == "CKSetup.Runner" && c.Version == v.Version )
                    .Should().BeTrue( $"The CKSetup.Runner/{v.Version} must exist but available versions are: {components.Where( c => c.Name == "CKSetup.Runner" ).Select( c => c.Version.ToString() ).Concatenate()}. Did you miss a rebuild of CKSetup.Runner?" );
            }
            else if( components.Count != 2 )
            {
                Assert.Fail( "There should be only 2 or 3 components." );
            }
            return true;
        }

        [Test]
        public void chaining_stores_with_prototype()
        {
            string remotePath = RegisterInStoreWithAllComponents();
            string localStore1 = TestHelper.CKSetup.GetCleanTestStorePath( "Level1" );
            string localStore2 = TestHelper.CKSetup.GetCleanTestStorePath( "Level2" );
            string localStore3 = TestHelper.CKSetup.GetCleanTestStorePath( "Level3" );
            using( var s = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore1 ) )
            {
                s.PrototypeStoreUrl = new Uri( remotePath, UriKind.Absolute );
            }
            using( var s = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore2 ) )
            {
                s.PrototypeStoreUrl = new Uri( localStore1, UriKind.Absolute );
            }
            using( var s = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore3 ) )
            {
                s.PrototypeStoreUrl = new Uri( localStore2, UriKind.Absolute );
            }
            StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( localStore1 ).Should().BeFalse();
            StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( localStore2 ).Should().BeFalse();
            StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( localStore3 ).Should().BeFalse();

            var conf = new SetupConfiguration( sharedMode: false );
            conf.BinPaths.Add( LocalHelper.StupidModelNetStandard20 );
            conf.Configuration = new XElement( "StupidConf" );
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";

            TestHelper.CKSetup.Run( conf, localStore3, "none", forceSetup: ForceSetupLevel.Full )
                .Should().Be( CKSetupRunResult.Succeed );

            StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( localStore1 ).Should().BeTrue();
            StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( localStore2 ).Should().BeTrue();
            StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( localStore3 ).Should().BeTrue();
        }

        [Test]
        public void importing_absolutely_no_components_does_not_throw()
        {
            NormalizedPath remote = TestHelper.CKSetup.GetTestStorePath( "Remote" );
            NormalizedPath local = TestHelper.CKSetup.GetTestStorePath( "Local" );

            using( var rem = LocalStore.OpenOrCreate( TestHelper.Monitor, remote ) )
            using( var loc = LocalStore.OpenOrCreate( TestHelper.Monitor, local ) )
            {
                var unexisting = new[] { new ComponentRef( "Does.Not.Exist", TargetFramework.Net461, SVersion.ZeroVersion ) };
                ComponentMissingDescription missing = new ComponentMissingDescription( unexisting );
                using( var mem = Util.RecyclableStreamManager.GetStream() )
                {
                    rem.ExportComponents( missing, mem, TestHelper.Monitor );
                    mem.Position = 0;
                    loc.ImportComponents( mem, rem.Remote ).Should().BeTrue();
                }
            }
        }

        class FakeDownloader : IComponentFileDownloader
        {
            public Uri Url => new Uri( "https://fake.fake" );

            public StoredStream GetDownloadStream( IActivityMonitor monitor, SHA1Value file, CompressionKind kind )
            {
                throw new NotImplementedException();
            }
        }

        [Test]
        public void importing_from_an_empty_or_invalid_stream_does_not_throw_but_is_an_error()
        {
            NormalizedPath storePath = TestHelper.CKSetup.GetTestStorePath();

            using( var store = LocalStore.OpenOrCreate( TestHelper.Monitor, storePath ) )
            {
                using( var mem = Util.RecyclableStreamManager.GetStream() )
                {
                    store.ImportComponents( mem, new FakeDownloader() ).Should().BeFalse();
                }
            }
        }

        [TestCase( "A", "5.0.0", false )]
        [TestCase( "B", "5.0.0", false )]
        [TestCase( "A", "4.0.0", true )]
        [TestCase( "B", "4.0.0", true )]
        [TestCase( "A", "3.0.0", true )]
        [TestCase( "B", "3.0.0", true )]
        [TestCase( "A", "2.0.0", true )]
        [TestCase( "B", "2.0.0", true )]
        [TestCase( "A", "1.0.0", true )]
        [TestCase( "B", "1.0.0", true )]
        public void resolving_components_accross_prototype_chain( string name, string version, bool success )
        {
            Component CreateComponent( string name, string version )
            {
                var s = $@"<Component Kind=""SetupDependency"" TargetFramework=""NetCoreApp20"" Name=""{name}"" Version=""{version}"">
                        <Dependencies />
                        <EmbeddedComponents />
                        <Files></Files>
                    </Component>";
                return new Component( XElement.Parse( s ) );
            }

            string localStore0 = TestHelper.CKSetup.GetCleanTestStorePath( "Level0" );
            string localStore1 = TestHelper.CKSetup.GetCleanTestStorePath( "Level1" );
            string localStore2 = TestHelper.CKSetup.GetCleanTestStorePath( "Level2" );
            string localStore3 = TestHelper.CKSetup.GetCleanTestStorePath( "Level3" );

            using var s0 = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore0 );
            s0.PrototypeStoreUrl.LocalPath.Should().Be( Facade.DefaultStorePath );
            s0.Invoking( sut => sut.PrototypeStoreUrl = null ).Should().Throw<ArgumentNullException>();
            s0.PrototypeStoreUrl = ComponentDB.NoRemote;

            s0.ApplyDirectMutation( db => db.AddComponent( TestHelper.Monitor, CreateComponent( "A", "4.0.0" ) ) ).Should().BeTrue();
            s0.ApplyDirectMutation( db => db.AddComponent( TestHelper.Monitor, CreateComponent( "B", "1.0.0" ) ) ).Should().BeTrue();

            using var s1 = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore1 );
            s1.PrototypeStoreUrl = new Uri( localStore0, UriKind.Absolute );
            s1.ApplyDirectMutation( db => db.AddComponent( TestHelper.Monitor, CreateComponent( "A", "3.0.0" ) ) ).Should().BeTrue();
            s1.ApplyDirectMutation( db => db.AddComponent( TestHelper.Monitor, CreateComponent( "B", "2.0.0" ) ) ).Should().BeTrue();

            using var s2 = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore2 );
            s2.PrototypeStoreUrl = new Uri( localStore1, UriKind.Absolute );
            s2.ApplyDirectMutation( db => db.AddComponent( TestHelper.Monitor, CreateComponent( "A", "2.0.0" ) ) ).Should().BeTrue();
            s2.ApplyDirectMutation( db => db.AddComponent( TestHelper.Monitor, CreateComponent( "B", "3.0.0" ) ) ).Should().BeTrue();

            using var s3 = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore3 );
            s3.PrototypeStoreUrl = new Uri( localStore2, UriKind.Absolute );
            s3.ApplyDirectMutation( db => db.AddComponent( TestHelper.Monitor, CreateComponent( "A", "1.0.0" ) ) ).Should().BeTrue();
            s3.ApplyDirectMutation( db => db.AddComponent( TestHelper.Monitor, CreateComponent( "B", "4.0.0" ) ) ).Should().BeTrue();

            var toFind = new ComponentDependency( name, SVersion.Parse( version ) );
            var r = new DependencyResolver( s3.ComponentDB, TargetRuntime.NetCoreApp20, toFind )
                        .Run( TestHelper.Monitor, "store identifier", s3.CreateComponentDownloader() );
            r.Success.Should().Be( success );
            if( success )
            {
                r.FinalDB.Find( new ComponentRef( toFind.UseName, TargetFramework.NetCoreApp20, toFind.UseMinVersion ) )
                         .Should().NotBeNull();
            }
        }

        [Test]
        public void running_with_prototype_store_updates_the_local_store()
        {
            string remotePath = RegisterInStoreWithAllComponents();
            string localStore = TestHelper.CKSetup.GetCleanTestStorePath();
            RunStupidEngine( remotePath, localStore );
        }

        [Test]
        public void running_with_None_remote_stored_files_instead_of_GZiped()
        {
            string remotePath = RegisterInStoreWithAllComponents();
            string localStore = TestHelper.CKSetup.GetCleanTestStorePath();

            // Cleanup previous run because files are locked for a long time.
            var tempPathNone = Path.Combine( Path.GetTempPath(), "Test-CKSetup" );
            TestHelper.CleanupFolder( tempPathNone );

            var remotePathNone = Path.Combine( tempPathNone, Guid.NewGuid().ToString() );
            var remoteNone = Directory.CreateDirectory( remotePathNone );
            FileUtil.CopyDirectory( new DirectoryInfo( remotePath ), remoteNone, withHiddenFiles: false, withHiddenFolders: false );
            var gzipedFolder = Path.Combine( remotePathNone, "GZiped" );
            foreach( var gziped in Directory.GetFiles( gzipedFolder ) )
            {
                using( var f = File.OpenRead( gziped ) )
                using( var g = new GZipStream( f, CompressionMode.Decompress ) )
                using( var dest = File.Create( Path.Combine( remotePathNone, "None", Path.GetFileName( gziped ) ) ) )
                {
                    g.CopyTo( dest );
                }
            }
            // Remove all GZiped files, leaving only None files.
            // All files will be gziped in NoneGziped cache folder.
            TestHelper.CleanupFolder( gzipedFolder );
            RunStupidEngine( remotePathNone, localStore );
        }

        static void RunStupidEngine( string remotePath, string localStore )
        {
            using( var s = LocalStore.OpenOrCreate( TestHelper.Monitor, localStore ) )
            {
                s.PrototypeStoreUrl = new Uri( remotePath, UriKind.Absolute );
            }

            var conf = new SetupConfiguration();
            conf.BinPaths.Add( LocalHelper.StupidModelNetStandard20 );
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";

            TestHelper.CKSetup.Run( conf, localStore, remoteStoreUrl: "none", forceSetup: ForceSetupLevel.Full )
                .Should().Be( CKSetupRunResult.Succeed );

            StoreHasEngineAndOneOrTwoCKSetupRunnerComponents( localStore );

            TestHelper.CKSetup.Run( conf, localStore, remoteStoreUrl: "none", forceSetup: ForceSetupLevel.Full )
                .Should().Be( CKSetupRunResult.Succeed );
        }


    }
}
