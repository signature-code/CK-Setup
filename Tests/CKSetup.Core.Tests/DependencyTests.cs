using CK.Core;
using CSemVer;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CK.Testing.CKSetupTestHelper;

namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class DependencyTests
    {
        static readonly SVersion Version1 = SVersion.Parse( "1.0.0-alpha" );
        static readonly SVersion Version2 = SVersion.Parse( "2.0.0" );
        static readonly SVersion Version3 = SVersion.Parse( "3.0.0" );
        static readonly SVersion Version4 = SVersion.Parse( "4.0.0" );
        static readonly ComponentRef RefLibModelV1 = new ComponentRef( "LibModel", TargetFramework.NetStandard20, Version1 );
        static readonly ComponentRef RefLibEngineV1 = new ComponentRef( "LibEngine", TargetFramework.NetCoreApp20, Version1 );

        static readonly ComponentRef RefLibModelV2 = new ComponentRef( "LibModel", TargetFramework.NetStandard20, Version2 );
        static readonly ComponentRef RefLibEngineV2 = new ComponentRef( "LibEngine", TargetFramework.NetCoreApp20, Version2 );

        static readonly ComponentRef RefAppModelV1 = new ComponentRef( "AppModel", TargetFramework.NetStandard20, Version1 );
        static readonly ComponentRef RefAppEngineV1 = new ComponentRef( "AppEngine", TargetFramework.NetCoreApp20, Version1 );
        // AppRuntime disappeared in V2 (there is only the AppEngine).
        static readonly ComponentRef RefAppRuntimeV1 = new ComponentRef( "AppRuntime", TargetFramework.NetCoreApp20, Version1 );
        static readonly ComponentRef RefAppModelV2 = new ComponentRef( "AppModel", TargetFramework.NetStandard20, Version2 );
        static readonly ComponentRef RefAppEngineV2 = new ComponentRef( "AppEngine", TargetFramework.NetCoreApp20, Version2 );

        static readonly ComponentRef RefSuperModelV1 = new ComponentRef( "SuperModel", TargetFramework.NetStandard20, Version1 );

        static readonly ComponentRef RefRunnerV3 = new ComponentRef( "Runner", TargetFramework.NetCoreApp20, Version3 );
        static readonly ComponentRef RefRunnerV4 = new ComponentRef( "Runner", TargetFramework.NetCoreApp20, Version4 );

        static readonly Component LibModelV1 = new Component( ComponentKind.Model, RefLibModelV1, new[] { new ComponentDependency( "LibEngine", null ) } );
        static readonly Component LibModelV2 = new Component( ComponentKind.Model, RefLibModelV2, new[] { new ComponentDependency( "LibEngine", Version2 ) } );
        static readonly Component LibEngineV1 = new Component( ComponentKind.SetupDependency, RefLibEngineV1, new[] { new ComponentDependency( "Runner", null ) } );
        static readonly Component LibEngineV2 = new Component( ComponentKind.SetupDependency, RefLibEngineV2, new[] { new ComponentDependency( "Runner", null ) } );

        static readonly Component AppModelV1 = new Component( ComponentKind.Model, RefAppModelV1,
                                                                new[] { new ComponentDependency( "AppRuntime", null ) } );

        static readonly Component AppModelV2 = new Component( ComponentKind.Model, RefAppModelV2,
                                                                new[] { new ComponentDependency( "AppEngine", Version2 ) } );

        static readonly Component AppRuntimeV1 = new Component( ComponentKind.SetupDependency, RefAppRuntimeV1, new[] {
                                                                new ComponentDependency( "AppEngine", null ) } );

        static readonly Component AppEngineV1 = new Component( ComponentKind.SetupDependency, RefAppEngineV1, new[] {
                                                                new ComponentDependency( "LibEngine", null ) } );

        static readonly Component AppEngineV2 = new Component( ComponentKind.SetupDependency, RefAppEngineV2, new[] {
                                                                new ComponentDependency( "LibEngine", Version2 ),
                                                                new ComponentDependency( "Runner", Version4 )} );

        static readonly Component SuperModelV1 = new Component( ComponentKind.Model, RefSuperModelV1,
                                                        new[] { new ComponentDependency( "AppModel", Version2 ) } );


        static readonly Component RunnerV3 = new Component( ComponentKind.SetupDependency, RefRunnerV3 );
        static readonly Component RunnerV4 = new Component( ComponentKind.SetupDependency, RefRunnerV4 );

        static readonly ComponentDB FullDB = ComponentDB.Empty
                                               .AddComponent( TestHelper.Monitor, LibModelV1 )
                                               .AddComponent( TestHelper.Monitor, LibEngineV1 )
                                               .AddComponent( TestHelper.Monitor, LibModelV2 )
                                               .AddComponent( TestHelper.Monitor, LibEngineV2 )
                                               .AddComponent( TestHelper.Monitor, AppModelV1 )
                                               .AddComponent( TestHelper.Monitor, AppRuntimeV1 )
                                               .AddComponent( TestHelper.Monitor, AppEngineV1 )
                                               .AddComponent( TestHelper.Monitor, AppModelV2 )
                                               .AddComponent( TestHelper.Monitor, AppEngineV2 )
                                               .AddComponent( TestHelper.Monitor, SuperModelV1 )
                                               .AddComponent( TestHelper.Monitor, RunnerV3 )
                                               .AddComponent( TestHelper.Monitor, RunnerV4 );


        class UnusedComponentLoader : IComponentDownloader
        {
            readonly ComponentDB _db;

            public UnusedComponentLoader( ComponentDB db = null )
            {
                _db = db;
            }

            public ComponentDB Download( IActivityMonitor monitor, ComponentMissingDescription missing ) => _db ?? throw new NotSupportedException( "Should not be called." );
        }

        [Test]
        public void dependencies_with_prerelease_must_be_found_exactly()
        {
            var resolver = new DependencyResolver( FullDB,
                                                   TargetRuntime.NetCoreApp20,
                                                   new ComponentDependency( "LibModel", SVersion.Parse( "1.0.0-a00-00-0004-develop" ) ) );
            var result = resolver.Run( TestHelper.Monitor, "store-identifier", new UnusedComponentLoader( FullDB ) );
            result.Success.Should().BeFalse();
        }

        [Test]
        public void basic_dependencies_resolution()
        {
            var db = ComponentDB.Empty;
            db = db.AddComponent( TestHelper.Monitor, LibModelV1 )
                   .AddComponent( TestHelper.Monitor, LibEngineV1 )
                   .AddComponent( TestHelper.Monitor, RunnerV3 );
            var resolver = new DependencyResolver( db, TargetRuntime.NetCoreApp20, new ComponentDependency( "LibModel", null ) );
            var result = resolver.Run( TestHelper.Monitor, "store-identifier",new UnusedComponentLoader() );
            result.Success.Should().BeTrue();
            result.Components.Should().BeEquivalentTo( new[] { LibModelV1, LibEngineV1, RunnerV3 } );
        }

        [Test]
        public void basic_dependencies_resolution_with_versions()
        {
            var db = ComponentDB.Empty;
            db = db.AddComponent( TestHelper.Monitor, LibModelV1 )
                   .AddComponent( TestHelper.Monitor, LibEngineV1 )
                   .AddComponent( TestHelper.Monitor, LibModelV2 )
                   .AddComponent( TestHelper.Monitor, LibEngineV2 )
                   .AddComponent( TestHelper.Monitor, RunnerV3 );
            var resolver = new DependencyResolver( db, TargetRuntime.NetCoreApp20,
                                        new ComponentDependency( "LibModel", null ),
                                        new ComponentDependency( "LibEngine", Version2 ) );
            var result = resolver.Run( TestHelper.Monitor, "store-identifier", new UnusedComponentLoader() );
            result.Success.Should().BeTrue();
            result.Components.Should().BeEquivalentTo( new[] { LibModelV1, LibEngineV2, RunnerV3 } );
        }

        [Test]
        public void dependencies_resolution_from_two_models()
        {
            {
                var resolver = new DependencyResolver( FullDB, TargetRuntime.NetCoreApp20,
                                            new ComponentDependency( "LibModel", null ),
                                            new ComponentDependency( "AppModel", null ) );
                var result = resolver.Run( TestHelper.Monitor, "store-identifier", new UnusedComponentLoader() );
                result.Success.Should().BeTrue();
                result.Components.Should().BeEquivalentTo( new[] { LibModelV1, LibEngineV1, AppModelV1, AppRuntimeV1, AppEngineV1, RunnerV3 } );
            }
        }

        [Test]
        public void dependencies_resolution_with_two_models_and_greater_version()
        {
            {
                var resolver = new DependencyResolver( FullDB, TargetRuntime.NetCoreApp20,
                                            new ComponentDependency( "LibModel", null ),
                                            new ComponentDependency( "AppModel", Version2 ) );
                var result = resolver.Run( TestHelper.Monitor, "store-identifier", new UnusedComponentLoader() );
                result.Success.Should().BeTrue();
                result.Components.Should().BeEquivalentTo( new[] { LibModelV1, LibEngineV2, AppModelV2, AppEngineV2, RunnerV4 } );
            }
        }

        [Test]
        public void dependencies_resolution_with_upgrades()
        {
            var resolver = new DependencyResolver( FullDB, TargetRuntime.NetCoreApp20,
                                            new ComponentDependency( "LibModel", null ),
                                            new ComponentDependency( "AppModel", null ),
                                            new ComponentDependency( "SuperModel", null ) );
            var result = resolver.Run( TestHelper.Monitor, "store-identifier", new UnusedComponentLoader() );
            result.Success.Should().BeTrue();
            result.Components.Should().BeEquivalentTo( new[] { LibModelV1, LibEngineV2, SuperModelV1, AppModelV2, AppEngineV2, RunnerV4 } );
        }
    }
}
