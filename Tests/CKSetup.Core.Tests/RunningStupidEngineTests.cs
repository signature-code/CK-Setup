using CK.Core;
using CK.Testing;
using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Xml.Linq;
using static CK.Testing.CKSetupTestHelper;

namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class RunningStupidEngineTests
    {
        [Test]
        public void checkConfig()
        {
            TestHelper.CKSetup.DefaultStorePath.Should().Be( TestHelper.TestProjectFolder.Combine( "../TempStore" ).ResolveDots() );
        }

        [TearDown]
        public void ResetCKSetupWorkingDirectory()
        {
            TestHelper.CKSetup.WorkingDirectory = null;
        }

        [TestCase( "WinFormApp", "net6.0-windows" )]
        [TestCase( "OneConsoleApp", "net6.0" )]
        public void run_setup_on_CurrentFramework( string appName, string framework )
        {
            NormalizedPath storePath = TestHelper.CKSetup.GetCleanTestStorePath();

            // Ensuring that Net6.0 StupidEngine is available in the store as well as the StupidModel.
            TestHelper.CKSetup.PublishAndAddComponentFoldersToStore( new string[] { LocalHelper.StupidEngineNet60, LocalHelper.StupidModelNetStandard20 }, storePath: storePath )
                .Should().BeTrue();

            // Ensuring that the Net6.0 CKSetup.Runner is available and reading its exact version.
            TestHelper.CKSetup.PublishAndAddComponentFoldersToStore( new string[] { LocalHelper.RunnerNet60 }, storePath: storePath )
                .Should().BeTrue();
            var runningVersion = CSemVer.InformationalVersion.ReadFromFile( LocalHelper.RunnerNet60.AppendPart( "CKSetup.Runner.exe" ) ).Version;

            // Creating the CKSetup configuration:
            //   - The engine is the stupid one.
            //   - The BinPath is the one of the application to setup.
            //   - The Runner is explicitly set to the actual available version.
            //   - The log level of the setup is Verbose.
            //   - There's 3 StaticGates in StupidEngine. This inverts them.
            var conf = new SetupConfiguration( true );
            conf.EngineAssemblyQualifiedName = "StupidEngine.Engine, StupidEngine";
            var p = TestHelper.SolutionFolder.Combine( "Tests/CurrentFramework/" ).AppendPart( appName ).AppendPart( "bin" ).AppendPart( TestHelper.BuildConfiguration ).AppendPart( framework );
            conf.BinPaths.Add( p );
            conf.Dependencies.Add( new SetupDependency( "CKSetup.Runner", runningVersion ) );
            conf.LogLevel = LogFilter.Verbose;
            conf.StaticGates = "Demo1; !Demo2; Demo3;";

            using( TestHelper.Monitor.CollectEntries( out var entries, LogLevelFilter.Debug ) )
            {
                TestHelper.CKSetup.Run( conf, storePath, remoteStoreUrl: "none", launchDebug: false, forceSetup: ForceSetupLevel.Full )
                    .Should().Be( CKSetupRunResult.Succeed );
                entries.Should().Contain( e => e.Text == "Initialized new stupid Engine." );
                entries.Should().Contain( e => e.Text == "Running stupid Engine (LogLevel = Verbose)." );

                entries.Should().Contain( e => e.Text == "Gate: Demo1 [Opened]" );
                entries.Should().Contain( e => e.Text == "Gate: Demo2 [Closed]" );
                entries.Should().Contain( e => e.Text == "Gate: Demo3 [Opened]" );
            }
        }

    }
}
