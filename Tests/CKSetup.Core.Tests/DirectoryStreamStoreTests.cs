using CK.Core;
using CKSetup.StreamStore;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using static CK.Testing.CKSetupTestHelper;

namespace CKSetup.Core.Tests
{
    [TestFixture]
    public class DirectoryStreamStoreTests
    {
        [Test]
        public void tampering_a_compressed_file_raises_a_SecurityException()
        {
            var filePath = TestHelper.TestProjectFolder.AppendPart( "DirectoryStreamStoreTests.cs" );

            string localStore = TestHelper.CKSetup.GetCleanTestStorePath();
            using( IStreamStore d = new DirectoryStreamStore( localStore ) )
            {
                SHA1Value goodSHA1 = SHA1Value.ComputeFileHash( filePath );
                string fileKey = goodSHA1.ToString();
                string storedFilePath = Path.Combine( localStore, "GZiped", fileKey );
                using( var read = File.OpenRead( filePath ) )
                {
                    d.Create( fileKey, read, CompressionKind.None, CompressionKind.GZiped );
                }
                string extractedPath = storedFilePath + ".extracted";
                d.ExtractToFile( fileKey, extractedPath, goodSHA1 );
                File.ReadAllText( extractedPath ).Should().Be( File.ReadAllText( filePath ) );

                d.Invoking( sut => sut.ExtractToFile( fileKey, storedFilePath + ".NEVER", SHA1Value.Empty ) ).Should().Throw<SecurityException>();

            }
        }
    }
}
